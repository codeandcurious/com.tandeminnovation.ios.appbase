**EXTENSION**

# `Collection`
```swift
public extension Collection
```

## Methods
### `count(where:)`

```swift
func count(where test: (Element) throws -> Bool) rethrows -> Int
```
