**EXTENSION**

# `LAContext`
```swift
public extension LAContext
```

## Properties
### `biometricType`

```swift
var biometricType: BiometricTypeEnum
```
