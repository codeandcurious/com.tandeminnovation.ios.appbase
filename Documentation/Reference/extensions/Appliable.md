**EXTENSION**

# `Appliable`
```swift
public extension Appliable
```

## Methods
### `apply(closure:)`

```swift
func apply(closure: (Self) -> Void) -> Self
```
