**EXTENSION**

# `Dictionary`
```swift
public extension Dictionary
```

## Methods
### `toNSMutableDictionary()`

```swift
func toNSMutableDictionary() -> NSMutableDictionary
```

### `toNSDictionary()`

```swift
func toNSDictionary() -> NSDictionary
```
