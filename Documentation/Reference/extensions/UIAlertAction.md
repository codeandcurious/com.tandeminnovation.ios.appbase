**EXTENSION**

# `UIAlertAction`
```swift
public extension UIAlertAction
```

## Methods
### `init(title:handler:)`

```swift
convenience init(title: String?, handler: ((UIAlertAction) -> Void)? = nil)
```
