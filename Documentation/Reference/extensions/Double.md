**EXTENSION**

# `Double`
```swift
public extension Double
```

## Methods
### `roundDecimalPlaces(decimalPlaces:)`

```swift
func roundDecimalPlaces(decimalPlaces: Int) -> Double
```

Rounds the double to decimal places value

### `toString(decimalPlaces:)`

```swift
func toString(decimalPlaces: Int = 9) -> String
```

### `toInt()`

```swift
func toInt() -> Int
```
