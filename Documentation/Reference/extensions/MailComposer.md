**EXTENSION**

# `MailComposer`
```swift
extension MailComposer: MFMailComposeViewControllerDelegate
```

## Methods
### `mailComposeController(_:didFinishWith:error:)`

```swift
public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
```
