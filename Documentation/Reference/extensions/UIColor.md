**EXTENSION**

# `UIColor`
```swift
public extension UIColor
```

## Properties
### `hexString`

```swift
var hexString: String
```

### `rgba`

```swift
var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
```

### `redComponent`

```swift
var redComponent: CGFloat
```

### `greenComponent`

```swift
var greenComponent: CGFloat
```

### `blueComponent`

```swift
var blueComponent: CGFloat
```

### `alphaComponent`

```swift
var alphaComponent: CGFloat
```

## Methods
### `init(hex:)`

```swift
convenience init(hex: String)
```

Convenience Init for UIColor

- Parameter hex: Number relating to a Color

#### Parameters

| Name | Description |
| ---- | ----------- |
| hex | Number relating to a Color |