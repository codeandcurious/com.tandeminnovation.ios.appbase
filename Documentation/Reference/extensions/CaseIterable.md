**EXTENSION**

# `CaseIterable`
```swift
public extension CaseIterable where Self: Equatable
```

## Properties
### `index`

```swift
var index: Self.AllCases.Index?
```

Returns the index of CaseIterable ex: Index of the Enum: CaseIterable
