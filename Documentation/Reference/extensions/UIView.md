**EXTENSION**

# `UIView`
```swift
public extension UIView
```

## Properties
### `parentViewController`

```swift
var parentViewController: UIViewController?
```

Return the earliest related ParentViewController of any given view

### `borderWidth`

```swift
@IBInspectable var borderWidth: CGFloat
```

### `borderColor`

```swift
@IBInspectable var borderColor: UIColor?
```

### `cornerRadius`

```swift
@IBInspectable var cornerRadius: CGFloat
```

### `visibility`

```swift
var visibility: Visibility
```

### `visibilityState`

```swift
var visibilityState: String
```

## Methods
### `createUIImage()`

```swift
func createUIImage() -> UIImage
```

Creates a UIImage from the UIView
- Returns: UIImage of the UIView

### `setBackgroundGradient(colorArray:)`

```swift
func setBackgroundGradient(colorArray: [CGColor])
```

### `setRoundedCorners(corners:radius:)`

```swift
func setRoundedCorners(corners: UIRectCorner, radius: CGFloat)
```

Set each corner rounded by given corners and radius
- Parameters:
  - corners: UIRectCorner array ex: [.topRight, .topLeft]
  - radius: CGFloat

#### Parameters

| Name | Description |
| ---- | ----------- |
| corners | UIRectCorner array ex: [.topRight, .topLeft] |
| radius | CGFloat |

### `setRoundedCorners()`

```swift
func setRoundedCorners()
```

Sets the view with rounded corners, the corner radius is height / 2

### `setRound()`

```swift
func setRound()
```

Sets the view fully rounded

### `addSubview(view:viewFrame:)`

```swift
func addSubview(view: UIView, viewFrame: CGRect?)
```
