**EXTENSION**

# `UICollectionViewCell`
```swift
public extension UICollectionViewCell
```

## Properties
### `currentCellIndexPath`

```swift
var currentCellIndexPath: IndexPath?
```
