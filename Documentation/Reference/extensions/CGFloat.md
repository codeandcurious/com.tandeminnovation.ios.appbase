**EXTENSION**

# `CGFloat`
```swift
public extension CGFloat
```

## Methods
### `roundDecimalPlaces(decimalPlaces:)`

```swift
func roundDecimalPlaces(decimalPlaces: Int) -> CGFloat
```

Rounds the double to decimal places value
