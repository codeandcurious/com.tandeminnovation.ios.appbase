**EXTENSION**

# `DispatchSemaphore`
```swift
public extension DispatchSemaphore
```

## Methods
### `with(_:)`

```swift
func with<T>(_ block: () throws -> T) rethrows -> T
```
