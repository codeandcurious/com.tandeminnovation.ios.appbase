**EXTENSION**

# `NSMutableAttributedString`
```swift
public extension NSMutableAttributedString
```

## Methods
### `setFontFace(font:color:)`

```swift
func setFontFace(font: UIFont, color: UIColor? = nil)
```
