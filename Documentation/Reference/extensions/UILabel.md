**EXTENSION**

# `UILabel`
```swift
public extension UILabel
```

## Properties
### `actualFontSize`

```swift
var actualFontSize: CGFloat
```

This var it's usefull to know the fontSize after the text is setted, useCase: want to use the same fontSize for muiltiple uiLabels

## Methods
### `text(value:defaultValue:)`

```swift
func text(value: String?, defaultValue: String)
```
