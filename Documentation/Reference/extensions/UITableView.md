**EXTENSION**

# `UITableView`
```swift
public extension UITableView
```

## Properties
### `centerPoint`

```swift
var centerPoint: CGPoint
```

### `centerCellIndexPath`

```swift
var centerCellIndexPath: IndexPath?
```

## Methods
### `registerForCell(_:bundle:)`

```swift
func registerForCell(_ cellName: String, bundle: Bundle? = nil)
```

Register UITableViewCell

- Parameter cellName: cellName should be the same for Identifier and XIB Name

#### Parameters

| Name | Description |
| ---- | ----------- |
| cellName | cellName should be the same for Identifier and XIB Name |

### `reuseCell(_:_:)`

```swift
func reuseCell(_ cellWrapperBase: CellWrapperBase, _ indexPath: IndexPath) -> UITableViewCell
```

### `reloadDataAndKeepOffset()`

```swift
func reloadDataAndKeepOffset()
```
