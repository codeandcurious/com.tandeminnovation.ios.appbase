**EXTENSION**

# `Bundle`
```swift
public extension Bundle
```

## Methods
### `findBundle(bundleName:)`

```swift
static func findBundle(bundleName: String) -> Bundle
```

Search for the Bundle with the provided name in the possible candidate locations (ex: App, Framework or DebugSimulator
- Parameter bundleName: The bundle name (ex: MySpmModule)
- Returns: returns the bundle or throw fatalError("unable to find bundle named")

#### Parameters

| Name | Description |
| ---- | ----------- |
| bundleName | The bundle name (ex: MySpmModule) |