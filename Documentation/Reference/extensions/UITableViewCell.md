**EXTENSION**

# `UITableViewCell`
```swift
public extension UITableViewCell
```

## Properties
### `currentCellIndexPath`

```swift
var currentCellIndexPath: IndexPath?
```
