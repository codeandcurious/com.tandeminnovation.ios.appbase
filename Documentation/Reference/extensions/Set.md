**EXTENSION**

# `Set`
```swift
public extension Set
```

## Methods
### `toArray()`

```swift
func toArray() -> [Element]
```
