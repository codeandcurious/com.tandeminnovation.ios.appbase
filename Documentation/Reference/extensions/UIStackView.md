**EXTENSION**

# `UIStackView`
```swift
public extension UIStackView
```

## Methods
### `removeAllArrangedSubviews()`

```swift
func removeAllArrangedSubviews()
```

Removes all arranged subviews from the stack and superview.

### `removeAndDiscardArrangedSubview(_:)`

```swift
func removeAndDiscardArrangedSubview(_ view: UIView)
```

Removes the arranged subview and superview.
