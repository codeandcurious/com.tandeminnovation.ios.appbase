**EXTENSION**

# `UIImageView`
```swift
public extension UIImageView
```

## Methods
### `setRoundedImage(_:)`

```swift
func setRoundedImage(_ uiImage: UIImage)
```
