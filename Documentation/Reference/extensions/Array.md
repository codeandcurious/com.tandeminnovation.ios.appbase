**EXTENSION**

# `Array`
```swift
public extension Array where Element: NSCopying
```

## Properties
### `isNotEmpty`

```swift
var isNotEmpty: Bool
```

## Methods
### `copy()`

```swift
func copy() -> [Element]
```

### `containsIgnoringCase(_:)`

```swift
func containsIgnoringCase(_ element: Element) -> Bool
```

### `toSet()`

```swift
func toSet() -> Set<Element>
```

### `removeDuplicates()`

```swift
func removeDuplicates() -> [Element]
```

### `uniques(by:)`

```swift
func uniques<T: Hashable>(by keyPath: KeyPath<Element, T>) -> [Element]
```

Return unique values by keypath, usage yourArray,uniques(by: \.property)
- Parameter keyPath: keyPath description
- Returns: description

#### Parameters

| Name | Description |
| ---- | ----------- |
| keyPath | keyPath description |

### `getSafely(index:)`

```swift
func getSafely(index: Int) -> Element?
```

Safely lookup an index that might be out of bounds, returning nil if it does not exist
- Parameter index: desired index
- Returns: return or the element or nil

#### Parameters

| Name | Description |
| ---- | ----------- |
| index | desired index |

### `insertOrAppend(_:at:)`

```swift
mutating func insertOrAppend(_ newElement: Self.Element, at i: Self.Index) -> Int
```
