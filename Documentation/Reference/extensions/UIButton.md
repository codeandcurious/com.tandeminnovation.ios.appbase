**EXTENSION**

# `UIButton`
```swift
public extension UIButton
```

## Properties
### `localize`

```swift
@IBInspectable var localize: String
```

### `localizedForHighlighted`

```swift
@IBInspectable var localizedForHighlighted: String
```

## Methods
### `enable()`

```swift
func enable()
```

### `disable()`

```swift
func disable()
```

### `setTitle(_:)`

```swift
func setTitle(_ title: String?)
```

### `moveImageToRight()`

```swift
func moveImageToRight()
```
