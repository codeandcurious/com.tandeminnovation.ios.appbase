**EXTENSION**

# `UIViewController`
```swift
public extension UIViewController
```

## Methods
### `add(_:)`

```swift
func add(_ child: UIViewController)
```

### `add(_:at:)`

```swift
func add(_ child: UIViewController, at level: Int)
```

### `remove()`

```swift
func remove()
```
