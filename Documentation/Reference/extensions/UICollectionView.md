**EXTENSION**

# `UICollectionView`
```swift
public extension UICollectionView
```

## Properties
### `centerPoint`

```swift
var centerPoint: CGPoint
```

### `centerCellIndexPath`

```swift
var centerCellIndexPath: IndexPath?
```

## Methods
### `registerForCell(_:bundle:)`

```swift
func registerForCell(_ cellName: String, bundle: Bundle? = nil)
```

Register UICollectionViewCell

- Parameter cellName: cellName should be the same for Identifier and XIB Name

#### Parameters

| Name | Description |
| ---- | ----------- |
| cellName | cellName should be the same for Identifier and XIB Name |

### `reuseCell(_:_:)`

```swift
func reuseCell(_ cellWrapperBase: CellWrapperBase, _ indexPath: IndexPath) -> UICollectionViewCell
```

### `reloadDataAndKeepOffset()`

```swift
func reloadDataAndKeepOffset()
```

### `scrollToLastItem(_:_:)`

```swift
func scrollToLastItem(_ scrollDirection: UICollectionView.ScrollDirection = .vertical, _ animated: Bool = false)
```

### `scrollToNearestVisibleItem()`

```swift
func scrollToNearestVisibleItem()
```
