**EXTENSION**

# `UIImage`
```swift
public extension UIImage
```

## Properties
### `rounded`

```swift
var rounded: UIImage?
```

Transform any image to rounded

### `squared`

```swift
var squared: UIImage?
```

Crops the image with equal width and height
- Returns: UIImage

## Methods
### `cropToBounds(width:height:)`

```swift
func cropToBounds(width: Double, height: Double) -> UIImage
```

### `crop(width:height:)`

```swift
func crop(width: Double, height: Double) -> UIImage
```

### `tintColor(with:)`

```swift
func tintColor(with color: UIColor) -> UIImage?
```

### `scale(size:)`

```swift
func scale(size: CGSize) -> UIImage
```

### `resized(maxSize:)`

```swift
func resized(maxSize: CGFloat) -> UIImage?
```

Resize the image for the maxSize
- Parameter maxSize: Can be the width or height of the image
- Returns: UIImage?

#### Parameters

| Name | Description |
| ---- | ----------- |
| maxSize | Can be the width or height of the image |

### `makeImageWithColorAndSize(_:size:)`

```swift
func makeImageWithColorAndSize(_ color: UIColor, size: CGSize) -> UIImage
```

### `fixOrientation()`

```swift
func fixOrientation() -> UIImage?
```

Fix the image orientation
- Returns: UIImage?
