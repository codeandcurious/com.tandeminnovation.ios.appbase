**EXTENSION**

# `BinaryInteger`
```swift
public extension BinaryInteger
```

## Properties
### `isEven`

```swift
var isEven: Bool
```

Returns true whenever the integer is even, otherwise it will return false

### `isOdd`

```swift
var isOdd: Bool
```

Returns true whenever the integer is odd, otherwise it will return false
