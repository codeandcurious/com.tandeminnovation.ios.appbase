**EXTENSION**

# `UINavigationController`
```swift
public extension UINavigationController
```

## Methods
### `popToViewController(type:animated:)`

```swift
@discardableResult func popToViewController<T:UIViewController>(type: T.Type? = nil, animated: Bool = true) -> [UIViewController]?
```

This method is used as a fail safe method as an alternative to popViewController(_:animated:), the declaration of the viewController is not needed. The name of the controller can be used directly in the parameters of the function
- Example: popToViewController(type: TheViewController.self, animated: true)
- Returns: The poped viewControllers

### `findParentViewController(of:)`

```swift
func findParentViewController<T:UIViewController>(of type: T.Type? = nil) -> T?
```

Find the parent viewController of type
- Example: findParentViewController(type: TheViewController.self)
- Returns: The viewController

### `findViewController(of:)`

```swift
func findViewController<T>(of type: T.Type) -> T?
```

Find the viewController of type
- Example: findParentViewController(type: TheViewController.self)
- Returns: The viewController

### `findViewControllers(of:)`

```swift
func findViewControllers<T>(of type: T.Type) -> [T]?
```

Find the viewControllers of type
- Example: findViewControllers(type: TheViewController.self)
- Returns: The viewControllers
