**EXTENSION**

# `Int`
```swift
public extension Int
```

## Methods
### `toString()`

```swift
func toString() -> String
```

### `toDouble()`

```swift
func toDouble() -> Double
```
