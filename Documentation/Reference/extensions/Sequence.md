**EXTENSION**

# `Sequence`
```swift
public extension Sequence
```

## Methods
### `group(by:)`

```swift
func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U: [Iterator.Element]]
```

### `sorted(by:)`

```swift
func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element]
```

Simple method to sort sequence by parameter avoiding closure
- Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
- Returns: The sequence sorted by a > b

#### Parameters

| Name | Description |
| ---- | ----------- |
| keyPath | The argument must be passed as a keypath, eg: .argName |

### `sortedDescendent(by:)`

```swift
func sortedDescendent<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element]
```

Sort descendent by the keypath
- Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
- Returns: The sequence sorted descendent

#### Parameters

| Name | Description |
| ---- | ----------- |
| keyPath | The argument must be passed as a keypath, eg: .argName |

### `sortedAscendent(by:)`

```swift
func sortedAscendent<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element]
```

Sort ascendent by the keypath
- Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
- Returns: The sequence sorted ascendent

#### Parameters

| Name | Description |
| ---- | ----------- |
| keyPath | The argument must be passed as a keypath, eg: .argName |