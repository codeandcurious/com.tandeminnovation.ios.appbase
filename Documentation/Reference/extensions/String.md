**EXTENSION**

# `String`
```swift
public extension String
```

## Properties
### `cString`

```swift
var cString: UnsafePointer<CChar>
```

### `trimmed`

```swift
var trimmed: String
```

### `asURL`

```swift
var asURL: URL?
```

### `encodedUrl`

```swift
var encodedUrl: String?
```

### `localized`

```swift
var localized: String
```

### `asAttributedString`

```swift
var asAttributedString: NSAttributedString?
```

### `unescaped`

```swift
var unescaped: String
```

return a copy of the string with unscapped chars for :"\0", "\t", "\n", "\r", "\"", "\'", "\\"

### `numbers`

```swift
var numbers: String
```

### `clearedString`

```swift
var clearedString: String
```

### `normalize`

```swift
var normalize: String
```

Returns a string without special characters .

### `containsSpecialCharacters`

```swift
@inlinable var containsSpecialCharacters: Bool
```

### `isBlank`

```swift
var isBlank: Bool
```

### `isEmptyOrBlank`

```swift
var isEmptyOrBlank: Bool
```

## Methods
### `removeWhitespaces()`

```swift
func removeWhitespaces() -> String
```

### `trim()`

```swift
mutating func trim()
```

### `fromBase64()`

```swift
func fromBase64() -> String?
```

### `toBase64()`

```swift
func toBase64() -> String
```

### `fromBase64()`

```swift
func fromBase64() -> Data?
```

### `toBase64()`

```swift
func toBase64() -> Data
```

### `localized(withComment:)`

```swift
func localized(withComment comment: String) -> String
```

### `localized(_:)`

```swift
func localized(_ values: [String]) -> String
```

### `attributedStringFromHtml(_:)`

```swift
func attributedStringFromHtml(_ font: UIFont? = nil) -> NSMutableAttributedString
```

### `format(_:oldString:)`

```swift
func format(_ format: String, oldString: String) -> String
```

### `unformat(_:oldString:)`

```swift
func unformat(_ format: String, oldString: String) -> String
```

### `isChar()`

```swift
func isChar() -> Bool
```

### `isNumber()`

```swift
func isNumber() -> Bool
```

### `substring(fromIndex:)`

```swift
func substring(fromIndex: Int) -> String
```

### `substring(toIndex:)`

```swift
func substring(toIndex: Int) -> String
```

### `containsCharacters(validCharacterSet:)`

```swift
func containsCharacters(validCharacterSet: CharacterSet) -> Bool
```

### `attributedStringFromHtmlWithLineHeight(_:)`

```swift
func attributedStringFromHtmlWithLineHeight(_ font: UIFont? = nil) -> NSMutableAttributedString
```

### `split(by:)`

```swift
func split(by length: Int) -> [String]
```

Split the string in an array
- Parameter length: split at index
- Returns: [String]

#### Parameters

| Name | Description |
| ---- | ----------- |
| length | split at index |

### `height(withConstrainedWidth:font:)`

```swift
func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
```

Allows us to calculate the String height using the provided UIFont and the desired width
let text = "Hello, world!"
let textHeight = text.height(withConstrainedWidth: 100, font: UIFont.systemFont(ofSize: 16))
- Parameters:
  - width: desired with
  - font: font
- Returns: height: CGFloat

#### Parameters

| Name | Description |
| ---- | ----------- |
| width | desired with |
| font | font |

### `width(withConstrainedHeight:font:)`

```swift
func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat
```

Allows us to calculate the String width using the provided UIFont and the desired height
let text = "Hello, world!"
let textWidth = text.width(withConstrainedHeight: 100, font: UIFont.systemFont(ofSize: 16))
- Parameters:
  - height: desired height
  - font: font
- Returns: width: CGFloat

#### Parameters

| Name | Description |
| ---- | ----------- |
| height | desired height |
| font | font |