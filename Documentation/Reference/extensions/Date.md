**EXTENSION**

# `Date`
```swift
public extension Date
```

## Properties
### `startOfMonth`

```swift
var startOfMonth: Date
```

### `endOfMonth`

```swift
var endOfMonth: Date
```

### `startOfYear`

```swift
var startOfYear: Date
```

### `calendar`

```swift
var calendar: Calendar
```

SwifterSwift: User’s current calendar.

### `era`

```swift
var era: Int
```

SwifterSwift: Era.

       Date().era -> 1

### `quarter`

```swift
var quarter: Int
```

SwifterSwift: Quarter.

       Date().quarter -> 3 // date in third quarter of the year.

### `weekOfYear`

```swift
var weekOfYear: Int
```

SwifterSwift: Week of year.

       Date().weekOfYear -> 2 // second week in the year.

### `weekOfMonth`

```swift
var weekOfMonth: Int
```

SwifterSwift: Week of month.

       Date().weekOfMonth -> 3 // date is in third week of the month.

### `year`

```swift
var year: Int
```

SwifterSwift: Year.

       Date().year -> 2017

       var someDate = Date()
       someDate.year = 2000 // sets someDate's year to 2000

### `month`

```swift
var month: Int
```

SwifterSwift: Month.

    Date().month -> 1

    var someDate = Date()
    someDate.month = 10 // sets someDate's month to 10.

### `day`

```swift
var day: Int
```

SwifterSwift: Day.

    Date().day -> 12

    var someDate = Date()
    someDate.day = 1 // sets someDate's day of month to 1.

### `weekday`

```swift
var weekday: Int
```

SwifterSwift: Weekday.

    Date().weekday -> 5 // fifth day in the current week.

### `hour`

```swift
var hour: Int
```

SwifterSwift: Hour.

    Date().hour -> 17 // 5 pm

    var someDate = Date()
    someDate.hour = 13 // sets someDate's hour to 1 pm.

### `minute`

```swift
var minute: Int
```

SwifterSwift: Minutes.

    Date().minute -> 39

    var someDate = Date()
    someDate.minute = 10 // sets someDate's minutes to 10.

### `second`

```swift
var second: Int
```

SwifterSwift: Seconds.

    Date().second -> 55

    var someDate = Date()
    someDate.second = 15 // sets someDate's seconds to 15.

### `nanosecond`

```swift
var nanosecond: Int
```

SwifterSwift: Nanoseconds.

    Date().nanosecond -> 981379985

    var someDate = Date()
    someDate.nanosecond = 981379985 // sets someDate's seconds to 981379985.

### `millisecond`

```swift
var millisecond: Int
```

SwifterSwift: Milliseconds.

    Date().millisecond -> 68

    var someDate = Date()
    someDate.millisecond = 68 // sets someDate's nanosecond to 68000000.

### `isInFuture`

```swift
var isInFuture: Bool
```

SwifterSwift: Check if date is in future.

    Date(timeInterval: 100, since: Date()).isInFuture -> true

### `isInPast`

```swift
var isInPast: Bool
```

SwifterSwift: Check if date is in past.

    Date(timeInterval: -100, since: Date()).isInPast -> true

### `isInToday`

```swift
var isInToday: Bool
```

SwifterSwift: Check if date is within today.

    Date().isInToday -> true

### `isInYesterday`

```swift
var isInYesterday: Bool
```

SwifterSwift: Check if date is within yesterday.

    Date().isInYesterday -> false

### `isInTomorrow`

```swift
var isInTomorrow: Bool
```

SwifterSwift: Check if date is within tomorrow.

    Date().isInTomorrow -> false

### `isInWeekend`

```swift
var isInWeekend: Bool
```

SwifterSwift: Check if date is within a weekend period.

### `isWorkday`

```swift
var isWorkday: Bool
```

SwifterSwift: Check if date is within a weekday period.

### `isInCurrentWeek`

```swift
var isInCurrentWeek: Bool
```

SwifterSwift: Check if date is within the current week.

### `isInCurrentMonth`

```swift
var isInCurrentMonth: Bool
```

SwifterSwift: Check if date is within the current month.

### `isInCurrentYear`

```swift
var isInCurrentYear: Bool
```

SwifterSwift: Check if date is within the current year.

### `iso8601String`

```swift
var iso8601String: String
```

SwifterSwift: ISO8601 string of format (yyyy-MM-dd'T'HH:mm:ss.SSS) from date.

    Date().iso8601String -> "2017-01-12T14:51:29.574Z"

### `nearestFiveMinutes`

```swift
var nearestFiveMinutes: Date
```

SwifterSwift: Nearest five minutes to date.

    var date = Date() // "5:54 PM"
    date.minute = 32 // "5:32 PM"
    date.nearestFiveMinutes // "5:30 PM"

    date.minute = 44 // "5:44 PM"
    date.nearestFiveMinutes // "5:45 PM"

### `nearestTenMinutes`

```swift
var nearestTenMinutes: Date
```

SwifterSwift: Nearest ten minutes to date.

    var date = Date() // "5:57 PM"
    date.minute = 34 // "5:34 PM"
    date.nearestTenMinutes // "5:30 PM"

    date.minute = 48 // "5:48 PM"
    date.nearestTenMinutes // "5:50 PM"

### `nearestQuarterHour`

```swift
var nearestQuarterHour: Date
```

SwifterSwift: Nearest quarter hour to date.

    var date = Date() // "5:57 PM"
    date.minute = 34 // "5:34 PM"
    date.nearestQuarterHour // "5:30 PM"

    date.minute = 40 // "5:40 PM"
    date.nearestQuarterHour // "5:45 PM"

### `nearestHalfHour`

```swift
var nearestHalfHour: Date
```

SwifterSwift: Nearest half hour to date.

    var date = Date() // "6:07 PM"
    date.minute = 41 // "6:41 PM"
    date.nearestHalfHour // "6:30 PM"

    date.minute = 51 // "6:51 PM"
    date.nearestHalfHour // "7:00 PM"

### `nearestHour`

```swift
var nearestHour: Date
```

SwifterSwift: Nearest hour to date.

    var date = Date() // "6:17 PM"
    date.nearestHour // "6:00 PM"

    date.minute = 36 // "6:36 PM"
    date.nearestHour // "7:00 PM"

### `yesterday`

```swift
var yesterday: Date
```

SwifterSwift: Yesterday date.

    let date = Date() // "Oct 3, 2018, 10:57:11"
    let yesterday = date.yesterday // "Oct 2, 2018, 10:57:11"

### `tomorrow`

```swift
var tomorrow: Date
```

SwifterSwift: Tomorrow's date.

    let date = Date() // "Oct 3, 2018, 10:57:11"
    let tomorrow = date.tomorrow // "Oct 4, 2018, 10:57:11"

### `unixTimestamp`

```swift
var unixTimestamp: Double
```

SwifterSwift: UNIX timestamp from date.

       Date().unixTimestamp -> 1484233862.826291

## Methods
### `isBetween(beginDate:endDate:)`

```swift
func isBetween(beginDate: Date, endDate: Date) -> Bool
```

### `isSameDay(as:)`

```swift
func isSameDay(as date2: Date) -> Bool
```

### `isSameMonth(as:)`

```swift
func isSameMonth(as date2: Date) -> Bool
```

### `isSameYear(as:)`

```swift
func isSameYear(as date2: Date) -> Bool
```

### `isInSame(timeUnit:as:)`

```swift
func isInSame(timeUnit: Calendar.Component, as date2: Date) -> Bool
```

### `adding(_:value:)`

```swift
func adding(_ component: Calendar.Component, value: Int) -> Date
```

SwifterSwift: Date by adding multiples of calendar component.

    let date = Date() // "Jan 12, 2017, 7:07 PM"
    let date2 = date.adding(.minute, value: -10) // "Jan 12, 2017, 6:57 PM"
    let date3 = date.adding(.day, value: 4) // "Jan 16, 2017, 7:07 PM"
    let date4 = date.adding(.month, value: 2) // "Mar 12, 2017, 7:07 PM"
    let date5 = date.adding(.year, value: 13) // "Jan 12, 2030, 7:07 PM"

- Parameters:
  - component: component type.
  - value: multiples of components to add.
- Returns: original date + multiples of component added.

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | component type. |
| value | multiples of components to add. |

### `add(_:value:)`

```swift
mutating func add(_ component: Calendar.Component, value: Int)
```

SwifterSwift: Add calendar component to date.

    var date = Date() // "Jan 12, 2017, 7:07 PM"
    date.add(.minute, value: -10) // "Jan 12, 2017, 6:57 PM"
    date.add(.day, value: 4) // "Jan 16, 2017, 7:07 PM"
    date.add(.month, value: 2) // "Mar 12, 2017, 7:07 PM"
    date.add(.year, value: 13) // "Jan 12, 2030, 7:07 PM"

- Parameters:
  - component: component type.
  - value: multiples of compnenet to add.

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | component type. |
| value | multiples of compnenet to add. |

### `changing(_:value:)`

```swift
func changing(_ component: Calendar.Component, value: Int) -> Date?
```

SwifterSwift: Date by changing value of calendar component.

    let date = Date() // "Jan 12, 2017, 7:07 PM"
    let date2 = date.changing(.minute, value: 10) // "Jan 12, 2017, 6:10 PM"
    let date3 = date.changing(.day, value: 4) // "Jan 4, 2017, 7:07 PM"
    let date4 = date.changing(.month, value: 2) // "Feb 12, 2017, 7:07 PM"
    let date5 = date.changing(.year, value: 2000) // "Jan 12, 2000, 7:07 PM"

- Parameters:
  - component: component type.
  - value: new value of compnenet to change.
- Returns: original date after changing given component to given value.

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | component type. |
| value | new value of compnenet to change. |

### `beginning(of:)`

```swift
func beginning(of component: Calendar.Component) -> Date?
```

SwifterSwift: Data at the beginning of calendar component.

    let date = Date() // "Jan 12, 2017, 7:14 PM"
    let date2 = date.beginning(of: .hour) // "Jan 12, 2017, 7:00 PM"
    let date3 = date.beginning(of: .month) // "Jan 1, 2017, 12:00 AM"
    let date4 = date.beginning(of: .year) // "Jan 1, 2017, 12:00 AM"

- Parameter component: calendar component to get date at the beginning of.
- Returns: date at the beginning of calendar component (if applicable).

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | calendar component to get date at the beginning of. |

### `end(of:)`

```swift
func end(of component: Calendar.Component) -> Date?
```

SwifterSwift: Date at the end of calendar component.

    let date = Date() // "Jan 12, 2017, 7:27 PM"
    let date2 = date.end(of: .day) // "Jan 12, 2017, 11:59 PM"
    let date3 = date.end(of: .month) // "Jan 31, 2017, 11:59 PM"
    let date4 = date.end(of: .year) // "Dec 31, 2017, 11:59 PM"

- Parameter component: calendar component to get date at the end of.
- Returns: date at the end of calendar component (if applicable).

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | calendar component to get date at the end of. |

### `isInCurrent(_:)`

```swift
func isInCurrent(_ component: Calendar.Component) -> Bool
```

SwifterSwift: Check if date is in current given calendar component.

    Date().isInCurrent(.day) -> true
    Date().isInCurrent(.year) -> true

- Parameter component: calendar component to check.
- Returns: true if date is in current given calendar component.

#### Parameters

| Name | Description |
| ---- | ----------- |
| component | calendar component to check. |

### `string(withFormat:)`

```swift
func string(withFormat format: String = "dd/MM/yyyy HH:mm") -> String
```

SwifterSwift: Date string from date.

    Date().string(withFormat: "dd/MM/yyyy") -> "1/12/17"
    Date().string(withFormat: "HH:mm") -> "23:50"
    Date().string(withFormat: "dd/MM/yyyy HH:mm") -> "1/12/17 23:50"

- Parameter format: Date format (default is "dd/MM/yyyy").
- Returns: date string.

#### Parameters

| Name | Description |
| ---- | ----------- |
| format | Date format (default is “dd/MM/yyyy”). |

### `dateString(ofStyle:)`

```swift
func dateString(ofStyle style: DateFormatter.Style = .medium) -> String
```

SwifterSwift: Date string from date.

    Date().dateString(ofStyle: .short) -> "1/12/17"
    Date().dateString(ofStyle: .medium) -> "Jan 12, 2017"
    Date().dateString(ofStyle: .long) -> "January 12, 2017"
    Date().dateString(ofStyle: .full) -> "Thursday, January 12, 2017"

- Parameter style: DateFormatter style (default is .medium).
- Returns: date string.

#### Parameters

| Name | Description |
| ---- | ----------- |
| style | DateFormatter style (default is .medium). |

### `dateTimeString(ofStyle:)`

```swift
func dateTimeString(ofStyle style: DateFormatter.Style = .medium) -> String
```

SwifterSwift: Date and time string from date.

    Date().dateTimeString(ofStyle: .short) -> "1/12/17, 7:32 PM"
    Date().dateTimeString(ofStyle: .medium) -> "Jan 12, 2017, 7:32:00 PM"
    Date().dateTimeString(ofStyle: .long) -> "January 12, 2017 at 7:32:00 PM GMT+3"
    Date().dateTimeString(ofStyle: .full) -> "Thursday, January 12, 2017 at 7:32:00 PM GMT+03:00"

- Parameter style: DateFormatter style (default is .medium).
- Returns: date and time string.

#### Parameters

| Name | Description |
| ---- | ----------- |
| style | DateFormatter style (default is .medium). |

### `timeString(ofStyle:)`

```swift
func timeString(ofStyle style: DateFormatter.Style = .medium) -> String
```

SwifterSwift: Time string from date

    Date().timeString(ofStyle: .short) -> "7:37 PM"
    Date().timeString(ofStyle: .medium) -> "7:37:02 PM"
    Date().timeString(ofStyle: .long) -> "7:37:02 PM GMT+3"
    Date().timeString(ofStyle: .full) -> "7:37:02 PM GMT+03:00"

- Parameter style: DateFormatter style (default is .medium).
- Returns: time string.

#### Parameters

| Name | Description |
| ---- | ----------- |
| style | DateFormatter style (default is .medium). |

### `dayName(ofStyle:)`

```swift
func dayName(ofStyle style: DayNameStyle = .full) -> String
```

SwifterSwift: Day name from date.

    Date().dayName(ofStyle: .oneLetter) -> "T"
    Date().dayName(ofStyle: .threeLetters) -> "Thu"
    Date().dayName(ofStyle: .full) -> "Thursday"

- Parameter Style: style of day name (default is DayNameStyle.full).
- Returns: day name string (example: W, Wed, Wednesday).

#### Parameters

| Name | Description |
| ---- | ----------- |
| Style | style of day name (default is DayNameStyle.full). |

### `monthName(ofStyle:)`

```swift
func monthName(ofStyle style: MonthNameStyle = .full) -> String
```

SwifterSwift: Month name from date.

    Date().monthName(ofStyle: .oneLetter) -> "J"
    Date().monthName(ofStyle: .threeLetters) -> "Jan"
    Date().monthName(ofStyle: .full) -> "January"

- Parameter Style: style of month name (default is MonthNameStyle.full).
- Returns: month name string (example: D, Dec, December).

#### Parameters

| Name | Description |
| ---- | ----------- |
| Style | style of month name (default is MonthNameStyle.full). |

### `secondsSince(_:)`

```swift
func secondsSince(_ date: Date) -> Double
```

SwifterSwift: get number of seconds between two date

- Parameter date: date to compate self to.
- Returns: number of seconds between self and given date.

#### Parameters

| Name | Description |
| ---- | ----------- |
| date | date to compate self to. |

### `minutesSince(_:)`

```swift
func minutesSince(_ date: Date) -> Double
```

SwifterSwift: get number of minutes between two date

- Parameter date: date to compate self to.
- Returns: number of minutes between self and given date.

#### Parameters

| Name | Description |
| ---- | ----------- |
| date | date to compate self to. |

### `hoursSince(_:)`

```swift
func hoursSince(_ date: Date) -> Double
```

SwifterSwift: get number of hours between two date

- Parameter date: date to compate self to.
- Returns: number of hours between self and given date.

#### Parameters

| Name | Description |
| ---- | ----------- |
| date | date to compate self to. |

### `daysSince(_:)`

```swift
func daysSince(_ date: Date) -> Double
```

SwifterSwift: get number of days between two date

- Parameter date: date to compate self to.
- Returns: number of days between self and given date.

#### Parameters

| Name | Description |
| ---- | ----------- |
| date | date to compate self to. |

### `isBetween(_:_:includeBounds:)`

```swift
func isBetween(_ startDate: Date, _ endDate: Date, includeBounds: Bool = false) -> Bool
```

SwifterSwift: check if a date is between two other dates

- Parameters:
  - startDate: start date to compare self to.
  - endDate: endDate date to compare self to.
  - includeBounds: true if the start and end date should be included (default is false)
- Returns: true if the date is between the two given dates.

#### Parameters

| Name | Description |
| ---- | ----------- |
| startDate | start date to compare self to. |
| endDate | endDate date to compare self to. |
| includeBounds | true if the start and end date should be included (default is false) |

### `isWithin(_:_:of:)`

```swift
func isWithin(_ value: UInt, _ component: Calendar.Component, of date: Date) -> Bool
```

SwifterSwift: check if a date is a number of date components of another date

- Parameters:
  - value: number of times component is used in creating range
  - component: Calendar.Component to use.
  - date: Date to compare self to.
- Returns: true if the date is within a number of components of another date

#### Parameters

| Name | Description |
| ---- | ----------- |
| value | number of times component is used in creating range |
| component | Calendar.Component to use. |
| date | Date to compare self to. |

### `random(in:)`

```swift
static func random(in range: Range<Date>) -> Date
```

SwifterSwift: Returns a random date within the specified range.

- Parameter range: The range in which to create a random date. `range` must not be empty.
- Returns: A random date within the bounds of `range`.

#### Parameters

| Name | Description |
| ---- | ----------- |
| range | The range in which to create a random date. `range` must not be empty. |

### `random(in:)`

```swift
static func random(in range: ClosedRange<Date>) -> Date
```

SwifterSwift: Returns a random date within the specified range.

- Parameter range: The range in which to create a random date.
- Returns: A random date within the bounds of `range`.

#### Parameters

| Name | Description |
| ---- | ----------- |
| range | The range in which to create a random date. |

### `random(in:using:)`

```swift
static func random<T>(in range: Range<Date>, using generator: inout T) -> Date where T: RandomNumberGenerator
```

SwifterSwift: Returns a random date within the specified range, using the given generator as a source for randomness.

- Parameters:
  - range: The range in which to create a random date. `range` must not be empty.
  - generator: The random number generator to use when creating the new random date.
- Returns: A random date within the bounds of `range`.

#### Parameters

| Name | Description |
| ---- | ----------- |
| range | The range in which to create a random date. `range` must not be empty. |
| generator | The random number generator to use when creating the new random date. |

### `random(in:using:)`

```swift
static func random<T>(in range: ClosedRange<Date>, using generator: inout T) -> Date where T: RandomNumberGenerator
```

SwifterSwift: Returns a random date within the specified range, using the given generator as a source for randomness.

- Parameters:
  - range: The range in which to create a random date.
  - generator: The random number generator to use when creating the new random date.
- Returns: A random date within the bounds of `range`.

#### Parameters

| Name | Description |
| ---- | ----------- |
| range | The range in which to create a random date. |
| generator | The random number generator to use when creating the new random date. |

### `init(calendar:timeZone:era:year:month:day:hour:minute:second:nanosecond:)`

```swift
init?(
    calendar: Calendar? = Calendar.current,
    timeZone: TimeZone? = NSTimeZone.default,
    era: Int? = Date().era,
    year: Int? = Date().year,
    month: Int? = Date().month,
    day: Int? = Date().day,
    hour: Int? = Date().hour,
    minute: Int? = Date().minute,
    second: Int? = Date().second,
    nanosecond: Int? = Date().nanosecond)
```

SwifterSwift: Create a new date form calendar components.

    let date = Date(year: 2010, month: 1, day: 12) // "Jan 12, 2010, 7:45 PM"

- Parameters:
  - calendar: Calendar (default is current).
  - timeZone: TimeZone (default is current).
  - era: Era (default is current era).
  - year: Year (default is current year).
  - month: Month (default is current month).
  - day: Day (default is today).
  - hour: Hour (default is current hour).
  - minute: Minute (default is current minute).
  - second: Second (default is current second).
  - nanosecond: Nanosecond (default is current nanosecond).

#### Parameters

| Name | Description |
| ---- | ----------- |
| calendar | Calendar (default is current). |
| timeZone | TimeZone (default is current). |
| era | Era (default is current era). |
| year | Year (default is current year). |
| month | Month (default is current month). |
| day | Day (default is today). |
| hour | Hour (default is current hour). |
| minute | Minute (default is current minute). |
| second | Second (default is current second). |
| nanosecond | Nanosecond (default is current nanosecond). |

### `init(iso8601String:)`

```swift
init?(iso8601String: String)
```

SwifterSwift: Create date object from ISO8601 string.

    let date = Date(iso8601String: "2017-01-12T16:48:00.959Z") // "Jan 12, 2017, 7:48 PM"

- Parameter iso8601String: ISO8601 string of format (yyyy-MM-dd'T'HH:mm:ss.SSSZ).

#### Parameters

| Name | Description |
| ---- | ----------- |
| iso8601String | ISO8601 string of format (yyyy-MM-dd’T’HH:mm:ss.SSSZ). |

### `init(unixTimestamp:)`

```swift
init(unixTimestamp: Double)
```

SwifterSwift: Create new date object from UNIX timestamp.

    let date = Date(unixTimestamp: 1484239783.922743) // "Jan 12, 2017, 7:49 PM"

- Parameter unixTimestamp: UNIX timestamp.

#### Parameters

| Name | Description |
| ---- | ----------- |
| unixTimestamp | UNIX timestamp. |

### `init(integerLiteral:)`

```swift
init?(integerLiteral value: Int)
```

SwifterSwift: Create date object from Int literal

    let date = Date(integerLiteral: 2017_12_25) // "2017-12-25 00:00:00 +0000"
- Parameter value: Int value, e.g. 20171225, or 2017_12_25 etc.

#### Parameters

| Name | Description |
| ---- | ----------- |
| value | Int value, e.g. 20171225, or 2017_12_25 etc. |