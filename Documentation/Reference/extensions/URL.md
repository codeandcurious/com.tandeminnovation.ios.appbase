**EXTENSION**

# `URL`
```swift
public extension URL
```

## Methods
### `encode(string:)`

```swift
static func encode(string: String) -> String
```
