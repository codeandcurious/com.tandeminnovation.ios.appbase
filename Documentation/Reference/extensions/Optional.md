**EXTENSION**

# `Optional`
```swift
public extension Optional
```

## Properties
### `isNilOrEmpty`

```swift
var isNilOrEmpty: Bool
```

## Methods
### `whenNotNil(_:)`

```swift
func whenNotNil(_ closure: (Wrapped) -> Void)
```

Receives a closure to be executed if the wrapped value isn't nil

### `whenNilOrNot(_:_:)`

```swift
func whenNilOrNot(_ whenNil: () -> Void, _ whenNotNil: (Wrapped) -> Void)
```

### `whenNil(_:)`

```swift
func whenNil(_ completion: () -> Void)
```

### `unwrapped(_:)`

```swift
func unwrapped(_ default: Wrapped) -> Wrapped
```

Return the value of the Optional or the `default` parameter
- param: The value to return if the optional is empty

### `unwrapped(else:)`

```swift
func unwrapped(else: @autoclosure () -> Wrapped) -> Wrapped
```

Returns the unwrapped value of the optional *or*
the result of an expression `else`
I.e. optional.or(else: print("Arrr"))

### `unwrapped(throw:)`

```swift
func unwrapped(throw exception: Error) throws -> Wrapped
```

Returns the unwrapped contents of the optional if it is not empty
If it is empty, throws exception `throw`

### `whenNotNilOrEmpty(_:)`

```swift
func whenNotNilOrEmpty(_ closure: (Wrapped) -> Void)
```

### `unwrapped()`

```swift
func unwrapped() -> Wrapped
```

Return the value of the Optional or a new ModelBase object

### `unwrapped()`

```swift
func unwrapped() -> Wrapped
```

Return the value of the Optional or a new Date object
