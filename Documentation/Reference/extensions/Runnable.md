**EXTENSION**

# `Runnable`
```swift
public extension Runnable
```

## Methods
### `run(closure:)`

```swift
func run<T>(closure: (Self) -> T) -> T
```
