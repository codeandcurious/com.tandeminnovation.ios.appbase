**EXTENSION**

# `UIScrollView`
```swift
public extension UIScrollView
```

## Properties
### `isBouncingAtTop`

```swift
var isBouncingAtTop: Bool
```

Returns true if the scrollView is bouncing at the top

### `isBouncingAtBottom`

```swift
var isBouncingAtBottom: Bool
```

Returns true if the scrollView is bouncing at the bottom

### `isBouncing`

```swift
var isBouncing: Bool
```

Returns true if the scrollView is bouncing (at the top or bottom)
