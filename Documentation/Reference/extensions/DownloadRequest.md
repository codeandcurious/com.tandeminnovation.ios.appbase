**EXTENSION**

# `DownloadRequest`

## Methods
### `response()`

```swift
func response() -> DefaultDownloadResponse
```

Wait for the request to finish then return the response value.

- returns: The response.

### `response(responseSerializer:)`

```swift
func response<T: DownloadResponseSerializerProtocol>(responseSerializer: T) -> DownloadResponse<T.SerializedObject>
```

Wait for the request to finish then return the response value.

- parameter responseSerializer: The response serializer responsible for serializing the request, response,
and data.
- returns: The response.

#### Parameters

| Name | Description |
| ---- | ----------- |
| responseSerializer | The response serializer responsible for serializing the request, response, and data. |

### `responseData()`

```swift
func responseData() -> DownloadResponse<Data>
```

Wait for the request to finish then return the response value.

- returns: The response.

### `responseJSON(options:)`

```swift
func responseJSON(options: JSONSerialization.ReadingOptions = .allowFragments) -> DownloadResponse<Any>
```

Wait for the request to finish then return the response value.

- parameter options: The JSON serialization reading options. `.AllowFragments` by default.

- returns: The response.

#### Parameters

| Name | Description |
| ---- | ----------- |
| options | The JSON serialization reading options. `.AllowFragments` by default. |

### `responseString(encoding:)`

```swift
func responseString(encoding: String.Encoding? = nil) -> DownloadResponse<String>
```

Wait for the request to finish then return the response value.

- parameter encoding: The string encoding. If `nil`, the string encoding will be determined from the
server response, falling back to the default HTTP default character set,
ISO-8859-1.

- returns: The response.

#### Parameters

| Name | Description |
| ---- | ----------- |
| encoding | The string encoding. If `nil`, the string encoding will be determined from the server response, falling back to the default HTTP default character set, ISO-8859-1. |

### `responsePropertyList(options:)`

```swift
func responsePropertyList(options: PropertyListSerialization.ReadOptions = PropertyListSerialization.ReadOptions()) -> DownloadResponse<Any>
```

Wait for the request to finish then return the response value.

- parameter options: The property list reading options. Defaults to `[]`.

- returns: The response.

#### Parameters

| Name | Description |
| ---- | ----------- |
| options | The property list reading options. Defaults to `[]`. |