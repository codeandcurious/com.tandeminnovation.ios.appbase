**PROTOCOL**

# `CollectionCellBase`

```swift
public protocol CollectionCellBase: UICollectionViewCell
```

## Methods
### `setup(cellWrapperBase:)`

```swift
func setup(cellWrapperBase: CellWrapperBase)
```
