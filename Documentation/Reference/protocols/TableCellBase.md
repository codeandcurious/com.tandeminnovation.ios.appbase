**PROTOCOL**

# `TableCellBase`

```swift
public protocol TableCellBase: UITableViewCell
```

## Methods
### `setup(cellWrapperBase:)`

```swift
func setup(cellWrapperBase: CellWrapperBase)
```
