**PROTOCOL**

# `CellWrapperBaseLegacy`

```swift
public protocol CellWrapperBaseLegacy
```

## Properties
### `identifier`

```swift
var identifier: String
```

### `data`

```swift
var data: Any?
```
