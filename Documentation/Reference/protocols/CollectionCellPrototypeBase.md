**PROTOCOL**

# `CollectionCellPrototypeBase`

```swift
public protocol CollectionCellPrototypeBase: UICollectionViewCell
```

## Methods
### `setup(with:)`

```swift
func setup(with any: Any)
```
