**PROTOCOL**

# `CellWrapperBase`

```swift
public protocol CellWrapperBase
```

## Properties
### `cellIdentifier`

```swift
var cellIdentifier: String
```
