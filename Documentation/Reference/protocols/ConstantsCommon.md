**PROTOCOL**

# `ConstantsCommon`

```swift
public protocol ConstantsCommon
```

Contains the encryptionPassword method that defines the app's encryption key.

## Methods
### `encryptionPassword()`

```swift
static func encryptionPassword() -> String
```
