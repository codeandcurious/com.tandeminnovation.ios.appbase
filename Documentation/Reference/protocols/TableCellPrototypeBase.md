**PROTOCOL**

# `TableCellPrototypeBase`

```swift
public protocol TableCellPrototypeBase: UITableViewCell
```

## Methods
### `setup(with:)`

```swift
func setup(with any: Any)
```
