**TYPEALIAS**

# `AlertHelper.Completion`

```swift
public typealias Completion = (() -> Void)?
```
