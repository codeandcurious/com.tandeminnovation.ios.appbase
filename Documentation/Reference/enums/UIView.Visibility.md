**ENUM**

# `UIView.Visibility`

```swift
enum Visibility: String
```

## Cases
### `visible`

```swift
case visible = "visible"
```

### `invisible`

```swift
case invisible = "invisible"
```

### `gone`

```swift
case gone = "gone"
```
