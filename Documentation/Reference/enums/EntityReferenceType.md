**ENUM**

# `EntityReferenceType`

```swift
public enum EntityReferenceType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `poi`

```swift
case poi = 10
```

### `poiGroup`

```swift
case poiGroup = 11
```

### `poiCluster`

```swift
case poiCluster = 12
```

### `event`

```swift
case event = 21
```

### `eventGroup`

```swift
case eventGroup = 22
```

### `eventCluster`

```swift
case eventCluster = 23
```

### `item`

```swift
case item = 31
```

### `itemGroup`

```swift
case itemGroup = 32
```

### `itemCluster`

```swift
case itemCluster = 33
```

### `ar`

```swift
case ar = 41
```

### `arGroup`

```swift
case arGroup = 42
```

### `arCluster`

```swift
case arCluster = 43
```

### `newsItem`

```swift
case newsItem = 51
```

### `newsGroup`

```swift
case newsGroup = 52
```

### `newsCluster`

```swift
case newsCluster = 53
```

### `beacon`

```swift
case beacon = 61
```

### `beaconGroup`

```swift
case beaconGroup = 62
```

### `beaconCluster`

```swift
case beaconCluster = 63
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> EntityReferenceType?
```
