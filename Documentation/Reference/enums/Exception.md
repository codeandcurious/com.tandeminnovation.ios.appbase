**ENUM**

# `Exception`

```swift
public enum Exception: Error
```

## Cases
### `INVALID_BODY_EXCEPTION`

```swift
case INVALID_BODY_EXCEPTION
```

### `TOKEN_NOT_FOUND`

```swift
case TOKEN_NOT_FOUND
```
