**ENUM**

# `Date.MonthNameStyle`

```swift
enum MonthNameStyle
```

SwifterSwift: Month name format.

- threeLetters: 3 letter month abbreviation of month name.
- oneLetter: 1 letter month abbreviation of month name.
- full: Full month name.

## Cases
### `threeLetters`

```swift
case threeLetters
```

SwifterSwift: 3 letter month abbreviation of month name.

### `oneLetter`

```swift
case oneLetter
```

SwifterSwift: 1 letter month abbreviation of month name.

### `full`

```swift
case full
```

SwifterSwift: Full month name.
