**ENUM**

# `NavigationType`

```swift
public enum NavigationType: CaseIterable
```

## Cases
### `show`

```swift
case show
```

### `present`

```swift
case present
```
