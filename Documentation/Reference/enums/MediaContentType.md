**ENUM**

# `MediaContentType`

```swift
public enum MediaContentType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `image`

```swift
case image = 1
```

### `video`

```swift
case video = 2
```

### `streaming`

```swift
case streaming = 3
```

### `document`

```swift
case document = 4
```

### `bundle`

```swift
case bundle = 5
```

### `rtBundle`

```swift
case rtBundle = 6
```

### `uiBundle`

```swift
case uiBundle = 7
```

### `webLocal`

```swift
case webLocal = 8
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> MediaContentType?
```
