**ENUM**

# `FieldType`

```swift
public enum FieldType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `text`

```swift
case text = 1
```

### `numeric`

```swift
case numeric = 2
```

### `trueOrFalse`

```swift
case trueOrFalse = 3
```

### `date`

```swift
case date = 4
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> FieldType?
```
