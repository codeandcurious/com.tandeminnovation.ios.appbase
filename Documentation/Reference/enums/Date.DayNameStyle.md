**ENUM**

# `Date.DayNameStyle`

```swift
enum DayNameStyle
```

SwifterSwift: Day name format.

- threeLetters: 3 letter day abbreviation of day name.
- oneLetter: 1 letter day abbreviation of day name.
- full: Full day name.

## Cases
### `threeLetters`

```swift
case threeLetters
```

SwifterSwift: 3 letter day abbreviation of day name.

### `oneLetter`

```swift
case oneLetter
```

SwifterSwift: 1 letter day abbreviation of day name.

### `full`

```swift
case full
```

SwifterSwift: Full day name.
