**ENUM**

# `PerformanceActionType`

```swift
public enum PerformanceActionType
```

Performance Log options

- start: Marks the Beginning of the log
- end: Marks the End of the log

## Cases
### `start`

```swift
case start
```

It declares the start of a log

### `stop`

```swift
case stop
```

It declares the end of a log
