**ENUM**

# `PlatformType`

```swift
public enum PlatformType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `android`

```swift
case android = 1
```

### `ios`

```swift
case ios = 2
```

### `windowsPhone`

```swift
case windowsPhone = 3
```

### `windows`

```swift
case windows = 4
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> PlatformType?
```
