**ENUM**

# `ActiveStatus`

```swift
public enum ActiveStatus: Int, CustomStringConvertible, Codable, CaseIterable
```

## Cases
### `inactive`

```swift
case inactive = 0
```

### `test`

```swift
case test = 1
```

### `active`

```swift
case active = 2
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `valueOfKey(_:)`

```swift
public static func valueOfKey(_ value: Int?) -> ActiveStatus
```

### `fromJson(_:_:)`

```swift
public static func fromJson(_ key: String, _ json: JSON) -> ActiveStatus?
```

### `fromJsonArray(_:_:)`

```swift
public static func fromJsonArray(_ key: String, _ json: JSON) -> [ActiveStatus]?
```

### `toJson(_:_:)`

```swift
public func toJson(_ key: String, _ json: inout JSON)
```

### `toJsonArray(_:_:_:)`

```swift
public static func toJsonArray(_ array: [ActiveStatus], _ key: String, _ json: inout JSON)
```

### `parseFromName(_:)`

```swift
public static func parseFromName(_ value: String?) -> ActiveStatus?
```

### `parseFromNameList(_:)`

```swift
public static func parseFromNameList(_ list: [String]) -> [ActiveStatus]?
```
