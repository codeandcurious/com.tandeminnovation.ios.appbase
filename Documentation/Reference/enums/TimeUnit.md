**ENUM**

# `TimeUnit`

```swift
public enum TimeUnit
```

Time Unit options.

````
case milliseconds
case seconds
case minutes
case hours
case days
````

- **NOTE:**
Each case also has a description **String**

## Cases
### `milliseconds`

```swift
case milliseconds
```

### `seconds`

```swift
case seconds
```

### `minutes`

```swift
case minutes
```

### `hours`

```swift
case hours
```

### `days`

```swift
case days
```

## Properties
### `description`

```swift
public var description: String
```
