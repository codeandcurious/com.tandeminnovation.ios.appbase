**ENUM**

# `EntityContentType`

```swift
public enum EntityContentType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `field`

```swift
case field = 1
```

### `entityReference`

```swift
case entityReference = 2
```

### `mediaContentReference`

```swift
case mediaContentReference = 3
```

### `appReference`

```swift
case appReference = 4
```

### `inAppPurchase`

```swift
case inAppPurchase = 5
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> EntityContentType?
```
