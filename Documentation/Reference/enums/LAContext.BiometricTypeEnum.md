**ENUM**

# `LAContext.BiometricTypeEnum`

```swift
enum BiometricTypeEnum: String
```

## Cases
### `none`

```swift
case none
```

### `touchID`

```swift
case touchID
```

### `faceID`

```swift
case faceID
```
