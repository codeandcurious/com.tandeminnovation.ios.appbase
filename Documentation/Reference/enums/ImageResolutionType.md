**ENUM**

# `ImageResolutionType`

```swift
public enum ImageResolutionType: Int, CustomStringConvertible, CaseIterable
```

## Cases
### `res1080p`

```swift
case res1080p = 1
```

### `res720p`

```swift
case res720p = 2
```

### `res480p`

```swift
case res480p = 3
```

### `res240p`

```swift
case res240p = 4
```

### `res100p`

```swift
case res100p = 5
```

### `thumbnail`

```swift
case thumbnail = 6
```

### `thumbnail200`

```swift
case thumbnail200 = 7
```

### `thumbnail300`

```swift
case thumbnail300 = 8
```

## Properties
### `description`

```swift
public var description: String
```

## Methods
### `parseFromString(_:)`

```swift
public static func parseFromString(_ value: String?) -> ImageResolutionType?
```
