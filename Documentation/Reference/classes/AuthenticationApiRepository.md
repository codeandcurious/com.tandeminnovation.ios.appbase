**CLASS**

# `AuthenticationApiRepository`

```swift
open class AuthenticationApiRepository: ApiRepositoryBase
```

## Methods
### `getAccessToken(url:clientID:clientSecret:scope:)`

```swift
open func getAccessToken(url: String, clientID: String, clientSecret: String, scope: String) throws -> ApiResponseWrapper<String>
```

### `refreshAccessToken(url:clientID:clientSecret:scope:refreshToken:)`

```swift
open func refreshAccessToken(url: String, clientID: String, clientSecret: String, scope: String, refreshToken: String) throws -> ApiResponseWrapper<OAuth2TokenModel>
```
