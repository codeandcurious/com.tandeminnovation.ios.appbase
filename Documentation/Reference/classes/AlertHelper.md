**CLASS**

# `AlertHelper`

```swift
open class AlertHelper
```

## Methods
### `buildAlert(title:message:actions:style:color:)`

```swift
public static func buildAlert(title: String? = String(), message: String? = String(), actions: [UIAlertAction]? = nil, style: UIAlertController.Style? = .alert, color: UIColor? = UIColor.black) -> UIAlertController
```

Build the UIAlertController so you can present as you wish
- Parameters:
  - title: title to display
  - message: message to display
  - actions: actions to execute
  - style: style .alert or .actionSheet
  - color: color action
- Returns: UIAlertController

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| actions | actions to execute |
| style | style .alert or .actionSheet |
| color | color action |

### `showAlert(presenter:completion:)`

```swift
public static func showAlert(presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with default "Ok" title and action
- Parameters:
  - presenter: presenter description
  - completion: completion description

#### Parameters

| Name | Description |
| ---- | ----------- |
| presenter | presenter description |
| completion | completion description |

### `showAlert(title:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title
- Parameters:
  - title: title to display
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |

### `showAlert(title:message:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), message: String? = String(), presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title, message
- Parameters:
  - title: title to display
  - message: message to display
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |

### `showAlert(title:message:actions:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), message: String? = String(), actions: [UIAlertAction]? = nil, presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title, message and actions
- Parameters:
  - title: title to display
  - message: message to display
  - actions: actions to execute
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| actions | actions to execute |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |

### `showAlert(title:message:actions:style:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), message: String? = String(), actions: [UIAlertAction]? = nil, style: UIAlertController.Style? = .alert, presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title, message, actions and style
- Parameters:
  - title: title to display
  - message: message to display
  - actions: actions to execute
  - style: style .alert or .actionSheet
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| actions | actions to execute |
| style | style .alert or .actionSheet |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |

### `showAlert(title:message:actions:style:color:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), message: String? = String(), actions: [UIAlertAction]? = nil, style: UIAlertController.Style? = .alert, color: UIColor? = UIColor.black, presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title, message, actions, style and color
- Parameters:
  - title: title to display
  - message: message to display
  - actions: actions to execute
  - style: style .alert or .actionSheet
  - color: color action
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| actions | actions to execute |
| style | style .alert or .actionSheet |
| color | color action |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |

### `showAlert(title:message:actions:style:color:animated:presenter:completion:)`

```swift
public static func showAlert(title: String? = String(), message: String? = String(), actions: [UIAlertAction]? = nil, style: UIAlertController.Style? = .alert, color: UIColor? = UIColor.black, animated: Bool? = true, presenter: UIViewController, completion: Completion? = nil)
```

Show Alert with custom title, message, actions, style, color and is animated or not
- Parameters:
  - title: title to display
  - message: message to display
  - actions: actions to execute
  - style: style .alert or .actionSheet
  - color: color action
  - animated: present with animation (true or false)
  - presenter: presenter is the viewController responsible to present this alert
  - completion: completion after display the

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | title to display |
| message | message to display |
| actions | actions to execute |
| style | style .alert or .actionSheet |
| color | color action |
| animated | present with animation (true or false) |
| presenter | presenter is the viewController responsible to present this alert |
| completion | completion after display the |