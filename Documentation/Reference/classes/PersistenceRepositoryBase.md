**CLASS**

# `PersistenceRepositoryBase`

```swift
open class PersistenceRepositoryBase: NSObject
```

## Methods
### `setup()`

```swift
open func setup()
```
