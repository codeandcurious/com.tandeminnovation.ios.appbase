**CLASS**

# `EventBusHelper`

```swift
open class EventBusHelper: NSObject
```

## Methods
### `init()`

```swift
public override init()
```

### `setup()`

```swift
public func setup()
```

### `RegisterOnMainThread(_:controller:handler:)`

```swift
open func RegisterOnMainThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void)
```

### `RegisterOnPostThread(_:controller:handler:)`

```swift
open func RegisterOnPostThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void)
```

### `RegisterOnAsyncThread(_:controller:handler:)`

```swift
open func RegisterOnAsyncThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void)
```

### `postEvent(_:)`

```swift
open func postEvent(_ event: EventBase)
```

### `postAction(_:)`

```swift
open func postAction(_ action: ActionBase)
```

### `postInitializeAction(sentBy:)`

```swift
open func postInitializeAction(sentBy: NSObject)
```

### `postStickyEvent(_:)`

```swift
open func postStickyEvent(_ event: EventBase)
```

### `cleanStickyEvent(_:)`

```swift
open func cleanStickyEvent(_ event: EventBase)
```

### `getStickyEvent(_:)`

```swift
open func getStickyEvent(_ eventName: String) -> EventBase?
```

### `postStickyAction(_:)`

```swift
open func postStickyAction(_ action: ActionBase)
```

### `cleanStickyAction(_:)`

```swift
open func cleanStickyAction(_ action: ActionBase)
```

### `getStickyAction(_:)`

```swift
open func getStickyAction(_ actionName: String) -> ActionBase?
```

### `startInitialization()`

```swift
open func startInitialization()
```

### `finishInitialization(_:)`

```swift
open func finishInitialization(_ event: EventBase)
```

### `removeListeners(_:)`

```swift
open func removeListeners(_ object: AnyObject)
```

### `hasAPICall()`

```swift
open func hasAPICall() -> Bool
```

### `onApiCall(_:)`

```swift
open func onApiCall(_ action: ActionBase?)
```

### `onApiCallFinished(_:)`

```swift
open func onApiCallFinished(_ action: ActionBase?)
```

### `isSentByMe(_:event:)`

```swift
open func isSentByMe(_ sender: NSObject, event: EventBase) -> Bool
```
