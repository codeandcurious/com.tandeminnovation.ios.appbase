**CLASS**

# `CompilerHelper`

```swift
public class CompilerHelper
```

## Properties
### `isDebugging`

```swift
public var isDebugging: Bool
```

## Methods
### `setup(_:)`

```swift
public func setup(_ debugCheck: () -> (Bool))
```

This function must be called in appDelegate and passed a closure with the pre-processor flags #if DEBUG return true #else return false #endif
- Parameter debugCheck: { #if DEBUG return true #else return false #endif }

#### Parameters

| Name | Description |
| ---- | ----------- |
| debugCheck | { #if DEBUG return true #else return false #endif } |