**CLASS**

# `PreferencesHelper`

```swift
open class PreferencesHelper: NSObject
```

## Methods
### `getPreferences(forKey:)`

```swift
open func getPreferences(forKey key: String) -> Any?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: String?) -> String?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: Date?) -> Date?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: Bool?) -> Bool?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: Int?) -> Int?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: Double?) -> Double?
```

### `getPreferences(_:defaultValue:)`

```swift
open func getPreferences(_ key: String, defaultValue: Float?) -> Float?
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: Int)
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: String)
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: Date)
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: Bool)
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: Double)
```

### `setPreferences(_:value:)`

```swift
open func setPreferences(_ key: String, value: Float)
```

### `set(value:forKey:password:)`

```swift
open func set(value: String, forKey key: String, password: String? = nil) -> Bool
```

Encrypts string into UserDefaults if a password is provided.

### `set(value:forKey:password:)`

```swift
open func set(value: Date, forKey key: String, password: String? = nil) -> Bool
```

Encrypts date into UserDefaults if a password is provided.

### `set(value:forKey:password:)`

```swift
open func set(value: Bool, forKey key: String, password: String? = nil) -> Bool
```

Encrypts bool into UserDefaults if a password is provided.

### `set(value:forKey:password:)`

```swift
open func set(value: Int, forKey key: String, password: String? = nil) -> Bool
```

Encrypts int into UserDefaults if a password is provided.

### `set(value:forKey:password:)`

```swift
open func set(value: Double, forKey key: String, password: String? = nil) -> Bool
```

Encrypts double into UserDefaults if a password is provided.

### `set(value:forKey:password:)`

```swift
open func set(value: Float, forKey key: String, password: String? = nil) -> Bool
```

Encrypts float into UserDefaults if a password is provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: String?, password: String? = nil) -> String?
```

Decrypts string from UserDefaults using the password provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: Date?, password: String? = nil) -> Date?
```

Decrypts date from UserDefaults using the password provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: Bool?, password: String? = nil) -> Bool?
```

Decrypts bool from UserDefaults using the password provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: Int?, password: String? = nil) -> Int?
```

Decrypts int from UserDefaults using the password provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: Double?, password: String? = nil) -> Double?
```

Decrypts double from UserDefaults using the password provided.

### `getValue(forKey:defaultValue:password:)`

```swift
open func getValue(forKey key: String, defaultValue: Float?, password: String? = nil) -> Float?
```

Decrypts float from UserDefaults using the password provided.

### `removeObject(forKey:)`

```swift
open func removeObject(forKey key: String)
```

Deletes an object from UserDefaults.
