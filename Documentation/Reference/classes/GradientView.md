**CLASS**

# `GradientView`

```swift
open class GradientView: UIView
```

Subclass of UIView with a gradient background

## Methods
### `init(coder:)`

```swift
required public init?(coder aDecoder: NSCoder)
```

### `draw(_:)`

```swift
override open func draw(_ rect: CGRect)
```
