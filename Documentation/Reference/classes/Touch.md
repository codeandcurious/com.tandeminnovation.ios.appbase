**CLASS**

# `Touch`

```swift
public class Touch
```

## Methods
### `on(view:action:)`

```swift
public static func on(view: UIView..., action: @escaping () -> ())
```
