**CLASS**

# `OrderedSet`

```swift
public class OrderedSet<T: Hashable>
```

## Methods
### `init()`

```swift
public init()
```

### `add(_:)`

```swift
public func add(_ object: T)
```

### `insert(_:at:)`

```swift
public func insert(_ object: T, at index: Int)
```

### `object(at:)`

```swift
public func object(at index: Int) -> T
```

### `set(_:at:)`

```swift
public func set(_ object: T, at index: Int)
```

### `indexOf(_:)`

```swift
public func indexOf(_ object: T) -> Int
```

### `remove(_:)`

```swift
public func remove(_ object: T)
```

### `all()`

```swift
public func all() -> [T]
```
