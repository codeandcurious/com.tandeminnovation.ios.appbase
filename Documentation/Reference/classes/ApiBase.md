**CLASS**

# `ApiBase`

```swift
open class ApiBase
```

## Properties
### `httpHelper`

```swift
open var httpHelper = HttpHelper.shared
```

## Methods
### `init(language:)`

```swift
public init(language: String)
```

### `init()`

```swift
public init()
```

### `getApiBaseUrl()`

```swift
open func getApiBaseUrl() -> String?
```

### `getDeviceId()`

```swift
open func getDeviceId() -> String?
```

### `getHttpHelper()`

```swift
public func getHttpHelper() -> HttpHelper
```

### `setHttpHelper(httpHelper:)`

```swift
public func setHttpHelper(httpHelper: HttpHelper)
```
