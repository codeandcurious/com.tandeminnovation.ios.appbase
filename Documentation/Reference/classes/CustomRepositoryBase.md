**CLASS**

# `CustomRepositoryBase`

```swift
open class CustomRepositoryBase: NSObject
```

## Methods
### `setup()`

```swift
open func setup()
```
