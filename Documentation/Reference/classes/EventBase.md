**CLASS**

# `EventBase`

```swift
open class EventBase: NSObject
```

## Properties
### `eventName`

```swift
public var eventName: String
```

### `action`

```swift
public var action: ActionBase?
```

### `uniqueIdentifier`

```swift
public var uniqueIdentifier: Int
```

To instances of the same object with the same values will return the same uniqueIdentifier

## Methods
### `init(eventName:action:)`

```swift
public init(eventName: String, action: ActionBase?)
```

### `propertiesUniqueIdentifier()`

```swift
public func propertiesUniqueIdentifier() -> String
```

Go to all properties and values and build a string based on his contents. To instances of the same object with the same values will return the same string
- Returns: returns a string describing all the properties and values
