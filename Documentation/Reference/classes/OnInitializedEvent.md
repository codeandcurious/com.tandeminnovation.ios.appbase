**CLASS**

# `OnInitializedEvent`

```swift
open class OnInitializedEvent: EventBase
```

## Methods
### `init(action:)`

```swift
public init(action: ActionBase?)
```
