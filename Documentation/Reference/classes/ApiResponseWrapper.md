**CLASS**

# `ApiResponseWrapper`

```swift
open class ApiResponseWrapper<T>: ApiResponseWrapperBase<T>
```

## Properties
### `TAG`

```swift
public let TAG: String = "ApiResponseWrapper"
```

### `error`

```swift
open var error: ApiResponseError?
```

### `localization`

```swift
open var localization: ApiResponseLocalization?
```

### `paging`

```swift
open var paging: ApiResponsePaging?
```

### `etag`

```swift
open var etag: String?
```

### `lastModified`

```swift
open var lastModified: String?
```

### `urlRequest`

```swift
open var urlRequest: URLRequest?
```

## Methods
### `build(dataResponse:apiResponseParser:)`

```swift
public static func build(dataResponse: DataResponse<Any>, apiResponseParser: ((_ json: JSON) -> T?) = { _ in return nil }) -> ApiResponseWrapper<T>
```

### `build(data:responseCode:apiResponseParser:)`

```swift
public static func build<T>(data: Data, responseCode: Int, apiResponseParser: ((_ json: JSON) -> T?)) -> ApiResponseWrapper<T>
```

### `build(data:responseCode:etag:)`

```swift
public static func build<T>(data: T?, responseCode: Int, etag: String) -> ApiResponseWrapper<T>
```

### `init(error:data:localization:paging:)`

```swift
public convenience init(error: ApiResponseError, data: T?, localization: ApiResponseLocalization?, paging: ApiResponsePaging?)
```

### `init(error:data:localization:paging:code:etag:)`

```swift
public init(error: ApiResponseError? = nil, data: T? = nil, localization: ApiResponseLocalization? = nil, paging: ApiResponsePaging? = nil, code: Int? = nil, etag: String? = nil)
```

### `init(json:apiResponseParser:)`

```swift
public convenience init(json: JSON, apiResponseParser: (_ json: JSON) -> T?)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```
