**CLASS**

# `Default`

```swift
open class Default
```

## Properties
### `defaultInt`

```swift
public let defaultInt: Int = 0
```

### `defaultString`

```swift
public let defaultString: String = ""
```

### `defaultBool`

```swift
public let defaultBool: Bool = false
```

### `defaultNSUUID`

```swift
public let defaultNSUUID: UUID = NSUUID(uuidBytes: "0000000000000000000000000000000") as UUID
```

### `defaultFloat`

```swift
public let defaultFloat: Float = Float(0)
```

### `defaultDouble`

```swift
public let defaultDouble: Double = Double(0)
```

### `defaultNSDate`

```swift
public let defaultNSDate: Foundation.Date = Foundation.Date()
```

### `defaultDate`

```swift
public let defaultDate: Foundation.Date = Date()
```

### `defaultNSData`

```swift
public let defaultNSData: Data = Data()
```

### `defaultNSNumber`

```swift
public let defaultNSNumber: NSNumber = NSNumber()
```

### `defaultCharacter`

```swift
public let defaultCharacter: Character = Character(" ")
```
