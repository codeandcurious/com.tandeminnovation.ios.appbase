**CLASS**

# `ApiResponseError`

```swift
open class ApiResponseError: NSObject
```

## Properties
### `userMessage`

```swift
public var userMessage: String?
```

### `errorMessage`

```swift
public var errorMessage: String?
```

### `errorCode`

```swift
public var errorCode: String?
```

## Methods
### `init()`

```swift
public override init()
```

### `init(userMessage:errorMessage:errorCode:)`

```swift
public init(userMessage: String?, errorMessage: String?, errorCode: String?)
```

### `init(json:)`

```swift
public init(json: JSON)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```
