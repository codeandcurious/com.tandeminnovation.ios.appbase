**CLASS**

# `ApiResponsePaging`

```swift
open class ApiResponsePaging: NSObject
```

## Properties
### `pageNumber`

```swift
public var pageNumber: Int?
```

### `pageSize`

```swift
public var pageSize: Int?
```

### `pageCount`

```swift
public var pageCount: Int?
```

### `totalRecordCount`

```swift
public var totalRecordCount: Int?
```

## Methods
### `init()`

```swift
public override init()
```

### `init(pageNumber:pageSize:pageCount:totalRecordCount:)`

```swift
public init(pageNumber: Int?, pageSize: Int?, pageCount: Int?, totalRecordCount: Int?)
```

### `init(json:)`

```swift
public init(json: JSON)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```
