**CLASS**

# `LocalizationHelper`

```swift
open class LocalizationHelper
```

## Properties
### `language`

```swift
public var language: String
```

Just gets the current setted up language.

@return "es","fr",...

## Methods
### `init()`

```swift
public init()
```

### `getLocalizedString(_:)`

```swift
open func getLocalizedString(_ key: String) -> String
```

### `StringFromPList(_:)`

```swift
open func StringFromPList(_ key: String) -> String
```

### `localizedFromTable(_:file:)`

```swift
open func localizedFromTable(_ key: String, file: String) -> String
```

### `NSLocalizedStringInternal(_:comment:)`

```swift
open func NSLocalizedStringInternal(_ key: String, comment: String) -> String
```

### `LocalizationSetLanguage(_:)`

```swift
open func LocalizationSetLanguage(_ language: String)
```

### `LocalizationGetLanguage()`

```swift
open func LocalizationGetLanguage() -> String
```

### `LocalizationReset()`

```swift
open func LocalizationReset()
```

### `localizedStringForKey(_:comment:)`

```swift
open func localizedStringForKey(_ key: String, comment: String) -> String
```

Gets the current localized string as in NSLocalizedString.

### `setLanguage(_:)`

```swift
open func setLanguage(_ language: String)
```

Sets the desired language of the ones you have.
If this function is not called it will use the default OS language.
If the language does not exists it returns the default OS language.

### `resetLocalization()`

```swift
open func resetLocalization()
```

Resets the localization system, so it uses the OS default language.
