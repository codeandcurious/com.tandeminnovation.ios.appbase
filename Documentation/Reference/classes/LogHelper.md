**CLASS**

# `LogHelper`

```swift
open class LogHelper
```

## Methods
### `init()`

```swift
public init()
```

### `verbose(_:_:_:line:)`

```swift
open func verbose(_ message: @autoclosure () -> Any, _
path: String = #file, _ function: String = #function, line: Int = #line)
```

### `debug(_:_:_:line:)`

```swift
open func debug(_ message: @autoclosure () -> Any, _
path: String = #file, _ function: String = #function, line: Int = #line)
```

### `info(_:_:_:line:)`

```swift
open func info(_ message: @autoclosure () -> Any, _
path: String = #file, _ function: String = #function, line: Int = #line)
```

### `warning(_:_:_:line:)`

```swift
open func warning(_ message: @autoclosure () -> Any, _
path: String = #file, _ function: String = #function, line: Int = #line)
```

### `error(_:_:_:line:)`

```swift
open func error(_ message: @autoclosure () -> Any, _
path: String = #file, _ function: String = #function, line: Int = #line)
```
