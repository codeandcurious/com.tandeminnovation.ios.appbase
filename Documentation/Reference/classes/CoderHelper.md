**CLASS**

# `CoderHelper`

```swift
open class CoderHelper
```

## Methods
### `hasKeyInCoder(_:coder:)`

```swift
open func hasKeyInCoder(_ key: String, coder: NSCoder) -> Bool
```

### `parseObject(_:coder:)`

```swift
open func parseObject(_ key: String, coder: NSCoder) -> Any
```

### `parseNullableObject(_:coder:)`

```swift
open func parseNullableObject(_ key: String, coder: NSCoder) -> Any?
```

### `parseString(_:coder:)`

```swift
open func parseString(_ key: String, coder: NSCoder) -> String
```

### `parseNullableString(_:coder:)`

```swift
open func parseNullableString(_ key: String, coder: NSCoder) -> String?
```

### `parseStringList(_:coder:)`

```swift
open func parseStringList(_ key: String, coder: NSCoder) -> Array<String>
```

### `parseNullableStringList(_:coder:)`

```swift
open func parseNullableStringList(_ key: String, coder: NSCoder) -> Array<String>?
```

### `parseDouble(_:coder:)`

```swift
open func parseDouble(_ key: String, coder: NSCoder) -> Double
```

### `parseNullableDouble(_:coder:)`

```swift
open func parseNullableDouble(_ key: String, coder: NSCoder) -> Double?
```

### `parseFloat(_:coder:)`

```swift
open func parseFloat(_ key: String, coder: NSCoder) -> Float
```

### `parseNullableFloat(_:coder:)`

```swift
open func parseNullableFloat(_ key: String, coder: NSCoder) -> Float?
```

### `parseBool(_:coder:)`

```swift
open func parseBool(_ key: String, coder: NSCoder) -> Bool
```

### `parseNullableBool(_:coder:)`

```swift
open func parseNullableBool(_ key: String, coder: NSCoder) -> Bool?
```

### `parseInt(_:coder:)`

```swift
open func parseInt(_ key: String, coder: NSCoder) -> Int
```

### `parseNullableInt(_:coder:)`

```swift
open func parseNullableInt(_ key: String, coder: NSCoder) -> Int?
```

### `parseIntList(_:coder:)`

```swift
open func parseIntList(_ key: String, coder: NSCoder) -> [Int]
```

### `parseNullableIntList(_:coder:)`

```swift
open func parseNullableIntList(_ key: String, coder: NSCoder) -> [Int]?
```

### `parseLong(_:coder:)`

```swift
open func parseLong(_ key: String, coder: NSCoder) -> Int
```

### `parseNullableLong(_:coder:)`

```swift
open func parseNullableLong(_ key: String, coder: NSCoder) -> Int?
```

### `parseDate(_:coder:)`

```swift
open func parseDate(_ key: String, coder: NSCoder) -> Foundation.Date
```

### `parseNullableDate(_:coder:)`

```swift
open func parseNullableDate(_ key: String, coder: NSCoder) -> Foundation.Date
```

### `parseBinary(_:coder:)`

```swift
open func parseBinary(_ key: String, coder: NSCoder) -> Data
```

### `parseChar(_:coder:)`

```swift
open func parseChar(_ key: String, coder: NSCoder) -> Character
```

### `parseUUID(_:coder:)`

```swift
open func parseUUID(_ key: String, coder: NSCoder) -> UUID
```

### `parseEnum(_:coder:)`

```swift
open func parseEnum<TEnum>(_ key: String, coder: NSCoder) -> TEnum
```
