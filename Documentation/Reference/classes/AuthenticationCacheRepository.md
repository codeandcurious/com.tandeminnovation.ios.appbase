**CLASS**

# `AuthenticationCacheRepository`

```swift
open class AuthenticationCacheRepository: CacheRepositoryBase
```

## Methods
### `save(oauth2Token:)`

```swift
open func save(oauth2Token: OAuth2TokenModel)
```

### `loadOAuth2Token()`

```swift
open func loadOAuth2Token() -> OAuth2TokenModel?
```

### `clearOAuth2Token()`

```swift
open func clearOAuth2Token()
```
