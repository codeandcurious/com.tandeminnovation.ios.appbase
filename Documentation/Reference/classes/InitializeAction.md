**CLASS**

# `InitializeAction`

```swift
open class InitializeAction: ActionBase
```

## Methods
### `init(sentBy:)`

```swift
public init(sentBy: NSObject)
```
