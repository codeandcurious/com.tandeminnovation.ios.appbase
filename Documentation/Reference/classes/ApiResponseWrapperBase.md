**CLASS**

# `ApiResponseWrapperBase`

```swift
open class ApiResponseWrapperBase<T>: NSObject
```

## Properties
### `jsonHelper`

```swift
open var jsonHelper = JsonHelper.shared
```

### `data`

```swift
open var data: T?
```

### `jsonData`

```swift
open var jsonData: JSON?
```

### `code`

```swift
open var code: Int!
```
