**CLASS**

# `JsonHelper`

```swift
open class JsonHelper
```

## Methods
### `hasKey(_:json:)`

```swift
open func hasKey(_ key: String, json: JSON) -> Bool
```

### `hasValueForKey(_:json:)`

```swift
open func hasValueForKey(_ key: String, json: JSON) -> Bool
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: String?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: Int?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: Float?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: Double?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: Bool?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: [Any]?, key: String, json: inout JSON)
```

### `setValue(_:key:json:)`

```swift
open func setValue(_ value: JSON?, key: String, json: inout JSON)
```

### `parseString(_:json:)`

```swift
open func parseString(_ key: String, json: JSON) -> String?
```

### `parseDouble(_:json:)`

```swift
open func parseDouble(_ key: String, json: JSON) -> Double?
```

### `parseFloat(_:json:)`

```swift
open func parseFloat(_ key: String, json: JSON) -> Float?
```

### `parseBool(_:json:)`

```swift
open func parseBool(_ key: String, json: JSON) -> Bool?
```

### `parseInt(_:json:)`

```swift
open func parseInt(_ key: String, json: JSON) -> Int?
```

### `parseLong(_:json:)`

```swift
open func parseLong(_ key: String, json: JSON) -> Int?
```

### `parseJSON(_:json:)`

```swift
open func parseJSON(_ key: String, json: JSON) -> JSON?
```

### `parseBinary(_:json:)`

```swift
open func parseBinary(_ key: String, json: JSON) -> Data?
```

### `parseChar(_:json:)`

```swift
open func parseChar(_ key: String, json: JSON) -> Character?
```

### `parseUUID(_:json:)`

```swift
open func parseUUID(_ key: String, json: JSON) -> UUID?
```

### `parseEnum(_:json:)`

```swift
open func parseEnum<TEnum>(_ key: String, json: JSON) -> TEnum?
```

### `parseDate(_:json:)`

```swift
open func parseDate(_ key: String, json: JSON) -> Date?
```

### `parseDate(_:json:dateFormat:)`

```swift
open func parseDate(_ key: String, json: JSON, dateFormat: String) -> Date?
```

### `parseArray(_:json:)`

```swift
open func parseArray(_ key: String, json: JSON) -> Array<JSON>?
```

### `parseAnyArray(_:json:)`

```swift
open func parseAnyArray(_ key: String, json: JSON) -> Array<Any>?
```

### `parseIntList(_:json:)`

```swift
open func parseIntList(_ key: String, json: JSON) -> Array<Int>?
```

### `parseBoolList(_:json:)`

```swift
open func parseBoolList(_ key: String, json: JSON) -> Array<Bool>?
```

### `parseDoubleList(_:json:)`

```swift
open func parseDoubleList(_ key: String, json: JSON) -> Array<Double>?
```

### `parseLongList(_:json:)`

```swift
open func parseLongList(_ key: String, json: JSON) -> [Int]?
```

### `parseStringList(_:json:)`

```swift
open func parseStringList(_ key: String, json: JSON) -> [String]?
```

### `parseJsonToString(_:)`

```swift
open func parseJsonToString(_ json: JSON) -> String?
```

### `parseDateList(_:json:)`

```swift
open func parseDateList(_ key: String, json: JSON) -> [Date]?
```

### `parseString(json:)`

```swift
open func parseString(json: JSON) -> String?
```

### `parseDouble(json:)`

```swift
open func parseDouble(json: JSON) -> Double?
```

### `parseFloat(json:)`

```swift
open func parseFloat(json: JSON) -> Float?
```

### `parseBool(json:)`

```swift
open func parseBool(json: JSON) -> Bool?
```

### `parseInt(json:)`

```swift
open func parseInt(json: JSON) -> Int?
```

### `parseLong(json:)`

```swift
open func parseLong(json: JSON) -> Int?
```

### `parseJSON(json:)`

```swift
open func parseJSON(json: JSON) -> JSON?
```

### `parseBinary(json:)`

```swift
open func parseBinary(json: JSON) -> Data?
```

### `parseChar(json:)`

```swift
open func parseChar(json: JSON) -> Character?
```

### `parseUUID(json:)`

```swift
open func parseUUID(json: JSON) -> UUID?
```

### `parseEnum(json:)`

```swift
open func parseEnum<TEnum>(json: JSON) -> TEnum?
```

### `parseDate(json:)`

```swift
open func parseDate(json: JSON) -> Date?
```

### `parseDate(json:dateFormat:)`

```swift
open func parseDate(json: JSON, dateFormat: String) -> Date?
```

### `parseArray(json:)`

```swift
open func parseArray(json: JSON) -> Array<JSON>?
```

### `parseAnyArray(json:)`

```swift
open func parseAnyArray(json: JSON) -> Array<Any>?
```

### `parseIntList(json:)`

```swift
open func parseIntList(json: JSON) -> Array<Int>?
```

### `parseBoolList(json:)`

```swift
open func parseBoolList(json: JSON) -> Array<Bool>?
```

### `parseDoubleList(json:)`

```swift
open func parseDoubleList(json: JSON) -> Array<Double>?
```

### `parseLongList(json:)`

```swift
open func parseLongList(json: JSON) -> [Int]?
```

### `parseStringList(json:)`

```swift
open func parseStringList(json: JSON) -> [String]?
```

### `parseDateList(json:)`

```swift
open func parseDateList(json: JSON) -> [Date]?
```

### `parseJsonToData(_:)`

```swift
open func parseJsonToData(_ json: JSON) -> Data?
```

### `parseJsonIntoDictionary(_:)`

```swift
open func parseJsonIntoDictionary(_ jsonString: String) -> NSDictionary?
```

### `parseJsonToArray(_:)`

```swift
open func parseJsonToArray(_ jsonString: String) -> Array<Any>?
```

### `format(_:)`

```swift
open func format(_ value: String) -> String
```

### `format(_:)`

```swift
open func format(_ value: Bool) -> Bool
```

### `format(_:)`

```swift
open func format(_ value: Data) -> String
```

### `format(_:)`

```swift
open func format(_ value: Character) -> String
```

### `formatEnum(_:)`

```swift
open func formatEnum(_ value: CustomStringConvertible) -> String
```

### `format(_:)`

```swift
open func format(_ value: Double) -> Double
```

### `format(_:)`

```swift
open func format(_ value: Float) -> Float
```

### `format(_:)`

```swift
open func format(_ value: Int) -> Int
```

### `format(_:)`

```swift
open func format(_ date: Date) -> String
```

### `format(_:dateFormat:)`

```swift
open func format(_ date: Date, dateFormat: String) -> String
```

Formats the given date with the specified format, using the default Locale

### `format(_:)`

```swift
open func format(_ value: UUID) -> String
```

### `format(_:)`

```swift
open func format(_ value: [Int]) -> [Int]
```

### `format(_:)`

```swift
open func format(_ value: JSON) -> String?
```

### `format(_:)`

```swift
open func format(_ value: [JSON]) -> String?
```

### `prettyPrint(_:)`

```swift
open func prettyPrint(_ value: JSON) -> String?
```

### `prettyPrint(_:)`

```swift
open func prettyPrint(_ dataResponse: DataResponse<Any>) -> String?
```

### `isValid(_:)`

```swift
open func isValid(_ value: JSON) -> Bool
```

Checks if the provided JSON is valid

### `isValid(_:)`

```swift
open func isValid(_ value: String) -> Bool
```

Checks if string is a valid JSON
