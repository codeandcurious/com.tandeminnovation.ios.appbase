**CLASS**

# `EventBus`

```swift
open class EventBus: NSObject
```

## Methods
### `init()`

```swift
public override init()
```

### `startInitialization()`

```swift
open func startInitialization()
```

### `finishInitialization()`

```swift
open func finishInitialization()
```

### `getInitializationCounter()`

```swift
open func getInitializationCounter() -> Int
```

### `isRegistered(_:controller:)`

```swift
open func isRegistered(_ name: String, controller: AnyObject) -> Bool
```

### `registerOnMainThread(_:controller:block:)`

```swift
open func registerOnMainThread(_ name: String, controller: AnyObject, block: @escaping (Notification) -> Void)
```

### `registerOnAsyncThread(_:controller:block:)`

```swift
open func registerOnAsyncThread(_ name: String, controller: AnyObject, block: @escaping (Notification) -> Void)
```

### `post(_:result:)`

```swift
open func post(_ name: String, result: [AnyHashable: Any]?)
```

### `postSticky(_:result:)`

```swift
open func postSticky(_ name: String, result: [AnyHashable: Any]?)
```

### `getStickyEvent(_:)`

```swift
open func getStickyEvent(_ name: String) -> [AnyHashable: Any]?
```

### `cleanStickyEvent(_:)`

```swift
open func cleanStickyEvent(_ name: String)
```

### `removeListeners(_:)`

```swift
open func removeListeners(_ object: AnyObject)
```
