**CLASS**

# `PerformanceHelper`

```swift
open class PerformanceHelper
```

Performance Helper allows the user to calculate performance times.

## Available Methods: ##
- logTime
- getElapsedTime
- printElapsedTime

## Methods
### `init()`

```swift
public init()
```

### `logTime(_:)`

```swift
public func logTime(_ type: PerformanceActionType)
```

Register a user determined key to start or end performance measuring.

### Usage Example: ###
````
PerformanceHelper.logTime(.start, description: "Entire Loop")
for _ in 0...300000 {
PerformanceHelper.logTime(.start, description: "Each Loop")
//...
PerformanceHelper.logTime(.end, description: "Each Loop")
}
PerformanceHelper.logTime(.end, description: "Entire Loop")
````

- Parameter type: .start or .end of type PerformanceLog

#### Parameters

| Name | Description |
| ---- | ----------- |
| type | .start or .end of type PerformanceLog |

### `printElapsedTime(forDescription:in:)`

```swift
public func printElapsedTime(forDescription description: String, in timeUnit: TimeUnit) -> String
```

Prints the elapsed time of a **finished** performance. The user can select the **TimeUnit** which will be reported

### Usage Example: ###
````
PerformanceHelper.printElapsedTime(forDescription: "Entire Loop" in: .minutes)
````

- Parameter type: .start or .end of type PerformanceLog
- Parameter description: the key used to resgister a performance

#### Parameters

| Name | Description |
| ---- | ----------- |
| type | .start or .end of type PerformanceLog |
| description | the key used to resgister a performance |

### `getElapsedTime(in:)`

```swift
public func getElapsedTime(in TimeUnit: TimeUnit) -> Int64
```

Return the elapsed time of a **finished** performance. The user can select the **TimeUnit** which will be reported

### Usage Example: ###
````
PerformanceHelper.getElapsedTime(forDescription: "Entire Loop" in: .minutes)
````

- Parameter type: .start or .end of type PerformanceLog
- Parameter description: the key used to resgister a performance
- Returns: **Double** describing the time elapsed in the selected **TimeUnit**

#### Parameters

| Name | Description |
| ---- | ----------- |
| type | .start or .end of type PerformanceLog |
| description | the key used to resgister a performance |

### `getStartTimeStamp()`

```swift
public func getStartTimeStamp() -> String
```

### `getStopTimeStamp()`

```swift
public func getStopTimeStamp() -> String
```

### `showLogger(_:_:_:_:)`

```swift
public static func showLogger(_ title: String? = "Log", _ tintColor: UIColor? = UIColor.black, _ presenter: UIViewController, _ completion: (() -> Void)? = nil)
```

Show alert with the last logged http request
- Parameters:
  - title: the title to present
  - tintColor: the color of the action
  - presenter: the UiViewController responsible to present
  - completion: optional completion

#### Parameters

| Name | Description |
| ---- | ----------- |
| title | the title to present |
| tintColor | the color of the action |
| presenter | the UiViewController responsible to present |
| completion | optional completion |