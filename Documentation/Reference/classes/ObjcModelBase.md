**CLASS**

# `ObjcModelBase`

```swift
open class ObjcModelBase: NSObject, NSCoding
```

## Methods
### `init()`

```swift
public override init()
```

### `init(json:)`

```swift
public init(json: JSON)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```

### `toJson()`

```swift
open func toJson() -> JSON
```

### `encode(with:)`

```swift
open func encode(with coder: NSCoder)
```

### `init(coder:)`

```swift
public required init?(coder aDecoder: NSCoder)
```

### `toJsonString()`

```swift
open func toJsonString() -> String?
```
