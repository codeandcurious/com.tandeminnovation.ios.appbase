**CLASS**

# `ManagerBase`

```swift
open class ManagerBase: NSObject
```

## Properties
### `isInitialized`

```swift
open var isInitialized: Bool = false
```

## Methods
### `setup()`

```swift
open func setup()
```

### `checkInitialization()`

```swift
open func checkInitialization()
```

### `sendOnErrorEventBase(action:data:)`

```swift
open func sendOnErrorEventBase(action: ActionBase?, data: Any?)
```

This method replaces the generated method sendOnErrorEven(action:code:message:)

- Parameters:
  - action: the requested action
  - data: Any data to be sent to the UI Layer

#### Parameters

| Name | Description |
| ---- | ----------- |
| action | the requested action |
| data | Any data to be sent to the UI Layer |