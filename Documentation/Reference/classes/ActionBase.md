**CLASS**

# `ActionBase`

```swift
open class ActionBase: NSObject
```

## Properties
### `actionName`

```swift
public var actionName: String
```

### `actionSentBy`

```swift
public var actionSentBy: String
```

### `uniqueIdentifier`

```swift
public var uniqueIdentifier: Int
```

To instances of the same object with the same values will return the same uniqueIdentifier

## Methods
### `init(actionName:sentBy:)`

```swift
public init(actionName: String, sentBy: Any)
```

### `propertiesUniqueIdentifier()`

```swift
public func propertiesUniqueIdentifier() -> String
```

Go to all properties and values and build a string based on his contents. To instances of the same object with the same values will return the same string
- Returns: returns a string describing all the properties and values
