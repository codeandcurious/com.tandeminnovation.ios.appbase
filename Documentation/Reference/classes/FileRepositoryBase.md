**CLASS**

# `FileRepositoryBase`

```swift
open class FileRepositoryBase: NSObject
```

## Methods
### `setup()`

```swift
open func setup()
```
