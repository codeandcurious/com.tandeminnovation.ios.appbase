**CLASS**

# `MailComposerBuilder`

```swift
public class MailComposerBuilder
```

## Methods
### `init(delegate:presenter:)`

```swift
public init(delegate: MFMailComposeViewControllerDelegate, presenter: UIViewController?)
```

### `withEmailDetails(to:subject:body:)`

```swift
public func withEmailDetails(to: [String], subject: String?, body: String?) -> MailComposerBuilder
```

### `withAlertDetails(tintColor:alertTitle:alertMessage:)`

```swift
public func withAlertDetails(tintColor: UIColor?, alertTitle: String?, alertMessage: String?) -> MailComposerBuilder
```

### `build()`

```swift
public func build() -> MailComposerBuilder
```
