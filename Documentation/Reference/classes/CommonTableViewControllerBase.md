**CLASS**

# `CommonTableViewControllerBase`

```swift
open class CommonTableViewControllerBase: UITableViewController
```

## Properties
### `logHelper`

```swift
public var logHelper = LogHelper.shared
```

## Methods
### `viewDidLoad()`

```swift
open override func viewDidLoad()
```

### `viewWillAppear(_:)`

```swift
override open func viewWillAppear(_ animated: Bool)
```

### `viewDidDisappear(_:)`

```swift
override open func viewDidDisappear(_ animated: Bool)
```

### `registerEvents()`

```swift
open func registerEvents()
```

### `unregisterEvents()`

```swift
open func unregisterEvents()
```

### `onApplicationDidEnterBackground()`

```swift
@objc open func onApplicationDidEnterBackground()
```

### `onApplicationWillEnterForeground()`

```swift
@objc open func onApplicationWillEnterForeground()
```

### `OnErrorEventReceiver(event:)`

```swift
open func OnErrorEventReceiver(event: OnErrorEvent)
```

### `setupApplicationLifeCycle()`

```swift
open func setupApplicationLifeCycle()
```

### `setup()`

```swift
open func setup()
```
