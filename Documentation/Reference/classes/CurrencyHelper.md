**CLASS**

# `CurrencyHelper`

```swift
open class CurrencyHelper
```

## Methods
### `formatValue(_:locale:fractionDigits:)`

```swift
public static func formatValue(_ value: String, locale: Locale, fractionDigits: Int = 2) -> String
```

### `formatValue(_:locale:fractionDigits:)`

```swift
public static func formatValue(_ value: Double, locale: Locale, fractionDigits: Int = 2) -> String
```
