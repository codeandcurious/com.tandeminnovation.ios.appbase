**CLASS**

# `ApiAuthenticationManager`

```swift
open class ApiAuthenticationManager: ManagerBase
```

## Methods
### `init()`

```swift
public override init()
```

### `setup(tokenUrl:clientSecret:clientId:scope:)`

```swift
open func setup(tokenUrl: String, clientSecret: String, clientId: String, scope: String)
```

### `getAccessToken()`

```swift
open func getAccessToken() throws -> ApiResponseWrapper<String>
```

### `refreshAccessToken(password:)`

```swift
open func refreshAccessToken(password: String = "") throws -> ApiResponseWrapper<OAuth2TokenModel>
```

### `saveOAuth2Token(tokenType:refreshToken:expiresIn:idToken:accessToken:password:)`

```swift
open func saveOAuth2Token(tokenType: String, refreshToken: String, expiresIn: Int, idToken: String, accessToken: String, password: String? = nil) -> Bool
```

### `clearOAuth2Token()`

```swift
open func clearOAuth2Token()
```

### `loadOAuth2Token(password:)`

```swift
open func loadOAuth2Token(password: String = "") -> OAuth2TokenModel?
```

Retrieves token from cache or persistence
