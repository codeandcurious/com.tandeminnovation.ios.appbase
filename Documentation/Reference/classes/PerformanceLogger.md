**CLASS**

# `PerformanceLogger`

```swift
public class PerformanceLogger
```

## Properties
### `httpLogger`

```swift
public var httpLogger: [String]
```

Getters

### `uiLogger`

```swift
public var uiLogger: [String]
```

### `businessLogger`

```swift
public var businessLogger: [String]
```

## Methods
### `clearHttpLogger()`

```swift
public func clearHttpLogger()
```

Setters

### `clearUiLogger()`

```swift
public func clearUiLogger()
```

### `clearBusinessLogger()`

```swift
public func clearBusinessLogger()
```

### `setHttpLogger(append:)`

```swift
public func setHttpLogger(append items: [String] = [])
```

### `setUiLogger(append:)`

```swift
public func setUiLogger(append items: [String] = [])
```

### `setBusinessLogger(append:)`

```swift
public func setBusinessLogger(append items: [String] = [])
```

### `appendHttpLoggerItem(_:)`

```swift
public func appendHttpLoggerItem(_ item: String)
```

### `appendUiLoggerItem(_:)`

```swift
public func appendUiLoggerItem(_ item: String)
```

### `appendBusinessLoggerItem(_:)`

```swift
public func appendBusinessLoggerItem(_ item: String)
```
