**CLASS**

# `OnAPICallFinishedEvent`

```swift
open class OnAPICallFinishedEvent: EventBase
```

## Methods
### `init(action:)`

```swift
public init(action: ActionBase?)
```
