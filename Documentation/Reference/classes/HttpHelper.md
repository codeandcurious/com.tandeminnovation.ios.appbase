**CLASS**

# `HttpHelper`

```swift
open class HttpHelper
```

## Properties
### `alamofireManager`

```swift
open var alamofireManager: SessionManager!
```

### `timeout`

```swift
open var timeout: TimeInterval = 60
```

## Methods
### `init()`

```swift
public init()
```

### `setTimeOut(timeout:)`

```swift
open func setTimeOut(timeout: Int)
```

### `setupAlamofireManager()`

```swift
open func setupAlamofireManager()
```

### `invalidateAndCancelAllTasks()`

```swift
open func invalidateAndCancelAllTasks()
```

### `makeRequest(url:method:headers:)`

```swift
open func makeRequest(url: String, method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any>
```

### `makeRequest(url:parameter:encoding:)`

```swift
open func makeRequest(url: String, parameter: [String: String], encoding: ParameterEncoding) throws -> DataResponse<Any>
```

### `makeRequest(url:parameter:method:headers:)`

```swift
open func makeRequest(url: String, parameter: [String: Any], method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any>
```

### `makeRequest(url:body:method:headers:)`

```swift
open func makeRequest(url: String, body: String, method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any>
```

### `getDataIfSuccessful(response:)`

```swift
open func getDataIfSuccessful(response: DataResponse<Any>) -> JSON?
```

### `unsecureSessionManager(urlSessionConfiguration:)`

```swift
open func unsecureSessionManager(urlSessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default) -> SessionManager
```

### `makeRequest(_:method:parameters:encoding:headers:)`

```swift
open func makeRequest(_ urlConvertible: URLConvertible, method: HTTPMethod = .get, parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest
```

### `makeRequest(_:)`

```swift
open func makeRequest(_ urlRequest: URLRequest) -> DataRequest
```
