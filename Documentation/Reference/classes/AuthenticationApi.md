**CLASS**

# `AuthenticationApi`

```swift
open class AuthenticationApi: ApiBase
```

## Methods
### `getAccessToken(url:clientID:clientSecret:scope:)`

```swift
open func getAccessToken(url: String, clientID: String, clientSecret: String, scope: String) throws -> ApiResponseWrapper<String>
```

### `refreshAccessToken(url:clientID:clientSecret:scope:refreshToken:)`

```swift
open func refreshAccessToken(url: String, clientID: String, clientSecret: String, scope: String, refreshToken: String) throws -> ApiResponseWrapper<OAuth2TokenModel>
```

### `getToken(url:success:fail:clientID:clientSecret:scope:)`

```swift
open func getToken(url: String, success: (_ result: String, _ statusCode: Int?) -> (), fail: (_ result: String, _ statusCode: Int?) -> ()?, clientID: String, clientSecret: String, scope: String)
```
