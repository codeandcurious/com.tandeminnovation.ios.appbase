**CLASS**

# `OAuth2TokenModelGenerated`

```swift
open class OAuth2TokenModelGenerated: ModelBase, NSCopying
```

## Properties
### `tokenType`

```swift
open var tokenType: String! = Default.shared.defaultString
```

### `refreshToken`

```swift
open var refreshToken: String! = Default.shared.defaultString
```

### `expiresIn`

```swift
open var expiresIn: Int! = Default.shared.defaultInt
```

### `idToken`

```swift
open var idToken: String! = Default.shared.defaultString
```

### `accessToken`

```swift
open var accessToken: String! = Default.shared.defaultString
```

## Methods
### `init()`

```swift
public override init()
```

### `init(json:)`

```swift
public override init(json: JSON)
```

### `init(coder:)`

```swift
required public init?(coder: NSCoder)
```

### `fromJson(json:)`

```swift
open override func fromJson(json: JSON)
```

### `toJson()`

```swift
open override func toJson() -> JSON
```

### `toJsonArray(array:)`

```swift
open class func toJsonArray(array: [OAuth2TokenModel]) -> [JSON]
```

### `toJsonArrayString(array:)`

```swift
open class func toJsonArrayString(array: [OAuth2TokenModel]) -> String?
```

### `fromJsonArray(jsonArray:)`

```swift
open class func fromJsonArray(jsonArray: [JSON]) -> [OAuth2TokenModel]
```

### `encode(with:)`

```swift
open override func encode(with coder: NSCoder)
```

### `decode(_:)`

```swift
open func decode(_ coder: NSCoder)
```

### `copy(with:)`

```swift
open func copy(with zone: NSZone?) -> Any
```
