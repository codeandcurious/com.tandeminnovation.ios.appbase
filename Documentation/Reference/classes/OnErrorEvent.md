**CLASS**

# `OnErrorEvent`

```swift
open class OnErrorEvent: EventBase
```

## Properties
### `data`

```swift
public let data: Any?
```

## Methods
### `init(action:code:message:)`

```swift
public convenience init(action: ActionBase?, code: Int, message: String?)
```

### `init(action:data:)`

```swift
public init(action: ActionBase?, data: Any?)
```
