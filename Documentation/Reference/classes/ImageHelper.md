**CLASS**

# `ImageHelper`

```swift
open class ImageHelper
```

## Methods
### `getOptimalImgeSize(_:isThumbnail:)`

```swift
open func getOptimalImgeSize(_ imagePath: String!, isThumbnail: Bool?) -> String?
```

### `resize(image:target:)`

```swift
public static func resize(image: UIImage, target targetSize: CGSize) -> UIImage
```
