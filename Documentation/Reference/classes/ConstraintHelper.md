**CLASS**

# `ConstraintHelper`

```swift
open class ConstraintHelper: NSLayoutConstraint
```

## Methods
### `init(item:attribute:relatedBy:constant:)`

```swift
public convenience init(item: Any, attribute: NSLayoutConstraint.Attribute, relatedBy: NSLayoutConstraint.Relation, constant: CGFloat)
```

### `init(item:attribute:relatedBy:toItem:attribute:constant:)`

```swift
public convenience init(item: Any, attribute: NSLayoutConstraint.Attribute, relatedBy: NSLayoutConstraint.Relation, toItem: Any?, attribute relatedAttribute: NSLayoutConstraint.Attribute, constant: CGFloat)
```

### `add(subview:toSuper:)`

```swift
public static func add(subview: UIView, toSuper superView: UIView)
```

### `constraint(subView:toSuper:)`

```swift
public static func constraint(subView: UIView, toSuper view: UIView)
```

Adds a view to another within the same rect/constraints

- Parameters:
  - subView: View to be added
  - view: View to receive the subview

#### Parameters

| Name | Description |
| ---- | ----------- |
| subView | View to be added |
| view | View to receive the subview |