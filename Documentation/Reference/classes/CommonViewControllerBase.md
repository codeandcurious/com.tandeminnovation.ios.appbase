**CLASS**

# `CommonViewControllerBase`

```swift
open class CommonViewControllerBase: UIViewController
```

## Properties
### `dataRequestActions`

```swift
public private (set) var dataRequestActions: Set<ActionBase> = Set<ActionBase>()
```

### `hasEndedFetchingData`

```swift
public var hasEndedFetchingData: Bool
```

### `tapToDismissKeyboard`

```swift
public var tapToDismissKeyboard: Bool = false
```

### `logHelper`

```swift
public var logHelper = LogHelper.shared
```

## Methods
### `viewDidLoad()`

```swift
open override func viewDidLoad()
```

### `viewWillAppear(_:)`

```swift
override open func viewWillAppear(_ animated: Bool)
```

### `viewDidDisappear(_:)`

```swift
override open func viewDidDisappear(_ animated: Bool)
```

### `registerEvents()`

```swift
open func registerEvents()
```

### `unregisterEvents()`

```swift
open func unregisterEvents()
```

### `addDataRequest(action:)`

```swift
open func addDataRequest(action: ActionBase?)
```

### `removeDataRequest(action:)`

```swift
open func removeDataRequest(action: ActionBase?)
```

### `clearDataRequest()`

```swift
open func clearDataRequest()
```

### `closeKeyboardOnTap()`

```swift
@objc open func closeKeyboardOnTap()
```

### `onApplicationWillEnterBackground()`

```swift
@objc open func onApplicationWillEnterBackground()
```

### `onApplicationDidEnterBackground()`

```swift
@objc open func onApplicationDidEnterBackground()
```

### `onApplicationWillEnterForeground()`

```swift
@objc open func onApplicationWillEnterForeground()
```

### `onApplicationDidEnterForeground()`

```swift
@objc open func onApplicationDidEnterForeground()
```

### `OnErrorEventReceiver(event:)`

```swift
open func OnErrorEventReceiver(event: OnErrorEvent)
```

### `setupApplicationLifeCycle()`

```swift
open func setupApplicationLifeCycle()
```

### `setup()`

```swift
open func setup()
```
