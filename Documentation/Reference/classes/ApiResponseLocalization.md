**CLASS**

# `ApiResponseLocalization`

```swift
open class ApiResponseLocalization: NSObject
```

## Properties
### `defaultLanguage`

```swift
public var defaultLanguage: String?
```

### `languages`

```swift
public var languages: Array<String>?
```

## Methods
### `init()`

```swift
public override init()
```

### `init(defaultLanguage:languages:)`

```swift
public init(defaultLanguage: String?, languages: Array<String>?)
```

### `init(json:)`

```swift
public init(json: JSON)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```
