**CLASS**

# `ApiRepositoryBase`

```swift
open class ApiRepositoryBase: NSObject
```

## Methods
### `setup()`

```swift
open func setup()
```
