**CLASS**

# `ModelBase`

```swift
open class ModelBase: NSCoding, Appliable, Runnable
```

## Methods
### `init()`

```swift
public init()
```

### `init(json:)`

```swift
public init(json: JSON)
```

### `fromJson(json:)`

```swift
open func fromJson(json: JSON)
```

### `toJson()`

```swift
open func toJson() -> JSON
```

### `encode(with:)`

```swift
open func encode(with coder: NSCoder)
```

### `init(coder:)`

```swift
public required init?(coder aDecoder: NSCoder)
```

### `toJsonString()`

```swift
open func toJsonString() -> String?
```

### `areEquals(_:)`

```swift
public static func areEquals(_ boolValues: Bool...) -> Bool
```

Simple function to avoid xcode infinite indexing and slow compile time https://stackoverflow.com/questions/65745161/xcode-slow-compile-time-on-equatable-protocol
- Parameter boolValues: example: lhs.title == rhs.title,lhs.subtitle == rhs.subtitle, ...
- Returns: true if it's equals, false if not

#### Parameters

| Name | Description |
| ---- | ----------- |
| boolValues | example: lhs.title == rhs.title,lhs.subtitle == rhs.subtitle, … |