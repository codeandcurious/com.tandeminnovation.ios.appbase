### `print(_:separator:terminator:)`

```swift
public func print(_ items: Any..., separator: String = " ", terminator: String = "\n")
```
