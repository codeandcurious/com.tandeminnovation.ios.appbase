### `logDebug(_:_:_:_:)`

```swift
public func logDebug(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line)
```
