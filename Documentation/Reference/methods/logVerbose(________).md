### `logVerbose(_:_:_:_:)`

```swift
public func logVerbose(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line)
```
