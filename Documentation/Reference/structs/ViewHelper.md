**STRUCT**

# `ViewHelper`

```swift
public struct ViewHelper
```

## Methods
### `loadNibView(withName:owner:)`

```swift
public static func loadNibView(withName: String, owner: Any?) -> UIView?
```

### `loadStoryboardViewController(withStoryboardName:withIdentifier:bundle:)`

```swift
public static func loadStoryboardViewController(withStoryboardName: String, withIdentifier: String, bundle: Bundle? = nil) -> UIViewController?
```

### `topViewController()`

```swift
public static func topViewController() -> UIViewController?
```

Returns the topViewController
WARNING: Should not be used for applications that support multiple scenes because it will only return the UIViewController on first keyWindow it finds
- Returns: UIViewController?

### `setRootViewController(_:)`

```swift
public static func setRootViewController(_ uiViewController: UIViewController)
```

Set the viewController passed as argument to rootViewController of the UIWindow
WARNING:  Should not be used for applications that support multiple scenes because it will only set the UIViewController on first keyWindow it finds
- Parameter uiViewController: The uiViewController which will be the rootViewController

#### Parameters

| Name | Description |
| ---- | ----------- |
| uiViewController | The uiViewController which will be the rootViewController |