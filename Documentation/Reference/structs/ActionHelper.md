**STRUCT**

# `ActionHelper`

```swift
public struct ActionHelper
```

## Methods
### `openInAppleMaps(locationName:phoneNumber:url:clLocationCoordinate2D:launchOptions:)`

```swift
public static func openInAppleMaps(locationName: String?, phoneNumber: String? = nil, url: URL? = nil, clLocationCoordinate2D: CLLocationCoordinate2D, launchOptions: [String: Any]?)
```

Opens Apple Maps on a give coordinate and shows a nome for said location

- Parameters:
  - locationName: Name to be shown
  - phoneNumber: Phone number to be called from Apple Maps
  - url: URL to be used by Apple Maps
  - clLocationCoordinate2D: The coordinates object
  - launchOptions: Apple Maps Options

#### Parameters

| Name | Description |
| ---- | ----------- |
| locationName | Name to be shown |
| phoneNumber | Phone number to be called from Apple Maps |
| url | URL to be used by Apple Maps |
| clLocationCoordinate2D | The coordinates object |
| launchOptions | Apple Maps Options |

### `shareUrl(url:uiViewController:excludedActivityTypes:)`

```swift
public static func shareUrl(url: URL, uiViewController: UIViewController, excludedActivityTypes: [UIActivity.ActivityType]?)
```

### `openUrl(url:options:completionHandler:)`

```swift
public static func openUrl(url: String, options: [UIApplication.OpenExternalURLOptionsKey: Any] = [:], completionHandler completion: ((Bool) -> Void)? = nil)
```

### `openUrl(url:options:completionHandler:)`

```swift
public static func openUrl(url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any] = [:], completionHandler completion: ((Bool) -> Void)? = nil)
```

### `openSafariViewControllerUrl(urlString:dismissButtonStyle:controlTintColor:barTintColor:modalTransitionStyle:modalPresentationStyle:_:sender:)`

```swift
public static func openSafariViewControllerUrl(urlString: String, dismissButtonStyle: SFSafariViewController.DismissButtonStyle = .done, controlTintColor: UIColor = UIColor.black, barTintColor: UIColor = UIColor.black, modalTransitionStyle: UIModalTransitionStyle = .coverVertical, modalPresentationStyle: UIModalPresentationStyle = .automatic, _ animated: Bool = true, sender: UIViewController)
```

### `openSafariViewControllerUrl(url:dismissButtonStyle:controlTintColor:barTintColor:modalTransitionStyle:modalPresentationStyle:_:sender:)`

```swift
public static func openSafariViewControllerUrl(url: URL, dismissButtonStyle: SFSafariViewController.DismissButtonStyle = .done, controlTintColor: UIColor = UIColor.black, barTintColor: UIColor = UIColor.black, modalTransitionStyle: UIModalTransitionStyle = .coverVertical, modalPresentationStyle: UIModalPresentationStyle = .automatic, _ animated: Bool = true, sender: UIViewController)
```

### `callPhoneNumber(_:)`

```swift
public static func callPhoneNumber(_ number: String?)
```

### `getAvailableMapsAppsAndGetDirections(alertTitle:alertMessage:latitude:longitude:tintColor:presenter:)`

```swift
public static func getAvailableMapsAppsAndGetDirections(alertTitle: String, alertMessage: String, latitude: Double, longitude: Double, tintColor: UIColor? = nil, presenter: UIViewController)
```

This method will check if there is GoogleMaps and/or Waze installed on the device, if it is an alert will be displayed with available options to get direction
- Parameters:
  - alertTitle: The title that should be displayed when multiple options are available
  - alertMessage: The message that should be displayed when multiple options are available
  - presenter: The view controller that will present the alert with options

Also put these in your info.plist:
   <key>LSApplicationQueriesSchemes</key>
   <array>
       <string>googlechromes</string>
       <string>comgooglemaps</string>
       <string>waze</string>
   </array>

#### Parameters

| Name | Description |
| ---- | ----------- |
| alertTitle | The title that should be displayed when multiple options are available |
| alertMessage | The message that should be displayed when multiple options are available |
| presenter | The view controller that will present the alert with options |

### `composeEmail(delegate:presenterViewController:)`

```swift
public static func composeEmail(delegate: MFMailComposeViewControllerDelegate, presenterViewController: UIViewController?) -> MailComposerBuilder
```

<key>LSApplicationQueriesSchemes</key>
<array>
    <string>readdle-spark</string>
    <string>ymail</string>
    <string>ms-outlook</string>
    <string>googlegmail</string>
</array>
