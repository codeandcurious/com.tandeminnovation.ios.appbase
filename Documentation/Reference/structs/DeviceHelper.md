**STRUCT**

# `DeviceHelper`

```swift
public struct DeviceHelper
```

## Methods
### `vibrate()`

```swift
public static func vibrate()
```

### `wasInstalledFromTestFlight()`

```swift
public static func wasInstalledFromTestFlight() -> Bool
```

### `wasInstalledFromAppStore()`

```swift
public static func wasInstalledFromAppStore() -> Bool
```

### `isRunningInSimulator()`

```swift
public static func isRunningInSimulator() -> Bool
```
