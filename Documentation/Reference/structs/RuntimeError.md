**STRUCT**

# `RuntimeError`

```swift
public struct RuntimeError: Error
```

## Properties
### `message`

```swift
public let message: String
```

### `localizedDescription`

```swift
public var localizedDescription: String
```

## Methods
### `init(_:)`

```swift
public init(_ message: String)
```
