**STRUCT**

# `DateHelper`

```swift
public struct DateHelper
```

## Methods
### `parse(date:format:locale:)`

```swift
public static func parse(date: String, format: String = DEFAULT_DATETIME_FORMAT, locale: Locale = DEFAULT_LOCALE) -> Date?
```

Parses the given `String` to `Date`.

- Parameter `date`: the `String` to be parsed to `Date`
`format`: desired format, default is `yyyy-MM-dd'T'HH:mm:ss.SSSZ`
`locale`: desired locale, default is `en_US_POSIX`

- Returns: `Date`, or nil if the conversion wasn't successful.

### `format(date:format:locale:)`

```swift
public static func format(date: Date, format: String = DEFAULT_DATETIME_FORMAT, locale: Locale = DEFAULT_LOCALE) -> String
```

Formats the given `Date` to `String`.

- Parameter `date`: the date to be parsed to `String`
`format`: desired format, default is `yyyy-MM-dd'T'HH:mm:ss.SSSZ`
`locale`: desired locale, default is `en_US_POSIX`

- Returns: `String`, or nil if the conversion wasn't successful.

### `timeElapsed(startDate:endDate:calendarComponent:)`

```swift
public static func timeElapsed(startDate: Date, endDate: Date, calendarComponent: Calendar.Component) -> Int?
```

### `isSameDay(date1:date2:)`

```swift
public static func isSameDay(date1: Date, date2: Date) -> Bool
```

### `isSameMonth(date1:date2:)`

```swift
public static func isSameMonth(date1: Date, date2: Date) -> Bool
```

### `isSameYear(date1:date2:)`

```swift
public static func isSameYear(date1: Date, date2: Date) -> Bool
```

### `isInSameTime(date1:date2:timeUnity:)`

```swift
public static func isInSameTime(date1: Date, date2: Date, timeUnity: Calendar.Component) -> Bool
```

### `combine(date:withTime:)`

```swift
public static func combine(date: Date, withTime time: Date) -> Date?
```

Creates a new date by combining the year, month and day from the first argument, and the hour, minute and second from the second
