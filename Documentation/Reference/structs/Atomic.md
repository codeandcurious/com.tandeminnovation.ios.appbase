**STRUCT**

# `Atomic`

```swift
public struct Atomic<Value>
```

Wrapper to assure a thread safe access to an element using a DispatchSemaphore.

## Properties
### `wrappedValue`

```swift
public var wrappedValue: Value
```

## Methods
### `init(_:)`

```swift
public init(_ value: Value)
```
