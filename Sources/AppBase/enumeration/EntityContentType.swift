/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

public enum EntityContentType: Int, CustomStringConvertible, CaseIterable {

	case field = 1
	case entityReference = 2
	case mediaContentReference = 3
	case appReference = 4
	case inAppPurchase = 5

	public var description: String {
		get {
			switch self {
			case .field:
				return "Field"

			case .entityReference:
				return "EntityReference"

			case .mediaContentReference:
				return "MediaContentReference"

			case .appReference:
				return "AppReference"

			case .inAppPurchase:
				return "InAppPurchase"
			}
		}
	}

	public static func parseFromString(_ value: String?) -> EntityContentType? {

		if let value = value {
			switch value {
			case "Field":
				return field

			case "EntityReference":
				return entityReference

			case "MediaContentReference":
				return mediaContentReference

			case "AppReference":
				return appReference

			case "InAppPurchase":
				return inAppPurchase

			default:
				return nil
			}
		}

		return nil
	}
}
