/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

public enum EntityReferenceType: Int, CustomStringConvertible, CaseIterable {

    case poi = 10
    case poiGroup = 11
    case poiCluster = 12
    case event = 21
    case eventGroup = 22
    case eventCluster = 23
    case item = 31
    case itemGroup = 32
    case itemCluster = 33
    case ar = 41
    case arGroup = 42
    case arCluster = 43
    case newsItem = 51
    case newsGroup = 52
    case newsCluster = 53
    case beacon = 61
    case beaconGroup = 62
    case beaconCluster = 63

    public var description: String {
        get {
            switch self {
            case .poi:
                return "Poi"

            case .poiGroup:
                return "PoiGroup"

            case .poiCluster:
                return "PoiCluster"

            case .event:
                return "Event"

            case .eventGroup:
                return "EventGroup"

            case .eventCluster:
                return "EventCluster"

            case .item:
                return "Item"

            case .itemGroup:
                return "ItemGroup"

            case .itemCluster:
                return "ItemCluster"

            case .ar:
                return "Ar"

            case .arGroup:
                return "ArGroup"

            case .arCluster:
                return "ArCluster"

            case .newsItem:
                return "NewsItem"

            case .newsGroup:
                return "NewsGroup"

            case .newsCluster:
                return "NewsCluster"

            case .beacon:
                return "Beacon"

            case .beaconGroup:
                return "BeaconGroup"

            case .beaconCluster:
                return "BeaconCluster"

            }
        }
    }

    public static func parseFromString(_ value: String?) -> EntityReferenceType? {

        if let value = value {
            switch value {
            case "Poi":
                return poi

            case "PoiGroup":
                return poiGroup

            case "PoiCluster":
                return poiCluster

            case "Event":
                return event

            case "EventGroup":
                return eventGroup

            case "EventCluster":
                return eventCluster

            case "Item":
                return item

            case "ItemGroup":
                return itemGroup

            case "ItemCluster":
                return itemCluster

            case "Ar":
                return ar

            case "ArGroup":
                return arGroup

            case "ArCluster":
                return arCluster

            case "NewsItem":
                return newsItem

            case "NewsGroup":
                return newsGroup

            case "NewsCluster":
                return newsCluster

            case "Beacon":
                return beacon

            case "BeaconGroup":
                return beaconGroup

            case "BeaconCluster":
                return beaconCluster

            default:
                return nil
            }
        }

        return nil
    }
}
