/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

public enum MediaContentType: Int, CustomStringConvertible, CaseIterable {

    case image = 1
    case video = 2
    case streaming = 3
    case document = 4
    case bundle = 5
    case rtBundle = 6
    case uiBundle = 7
    case webLocal = 8

    public var description: String {
        get {
            switch self {
            case .image:
                return "Image"

            case .video:
                return "Video"

            case .streaming:
                return "Streaming"

            case .document:
                return "Document"

            case .bundle:
                return "Bundle"

            case .rtBundle:
                return "RTBundle"

            case .uiBundle:
                return "UIBundle"

            case .webLocal:
                return "WebLocal"
            }
        }
    }

    public static func parseFromString(_ value: String?) -> MediaContentType? {

        if let value = value {
            switch value {
            case "Image":
                return image

            case "Video":
                return video

            case "Streaming":
                return streaming

            case "Document":
                return document

            case "Bundle":
                return bundle

            case "RTBundle":
                return rtBundle

            case "UIBundle":
                return uiBundle

            case "WebLocal":
                return webLocal

            default:
                return nil
            }
        }

        return nil
    }
}
