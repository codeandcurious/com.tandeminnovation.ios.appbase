/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

public enum PlatformType: Int, CustomStringConvertible, CaseIterable {

    case android = 1
    case ios = 2
    case windowsPhone = 3
    case windows = 4

    public var description: String {
        get {
            switch self {
            case .android:
                return "Android"

            case .ios:
                return "IOS"

            case .windowsPhone:
                return "WindowsPhone"

            case .windows:
                return "Windows"
            }
        }
    }

    public static func parseFromString(_ value: String?) -> PlatformType? {

        if let value = value {
            switch value {
            case "Android":
                return android

            case "IOS":
                return ios

            case "WindowsPhone":
                return windowsPhone

            case "Windows":
                return windows

            default:
                return nil
            }
        }

        return nil
    }
}
