//
//  NavigationType.swift
//  AppBase
//
//  Created by Fabio Barreiros on 14/01/2020.
//

import Foundation
public enum NavigationType: CaseIterable {

    case show
    case present
}
