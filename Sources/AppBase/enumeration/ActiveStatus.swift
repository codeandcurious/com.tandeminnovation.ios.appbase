/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

public enum ActiveStatus: Int, CustomStringConvertible, Codable, CaseIterable {

    case inactive = 0
    case test = 1
    case active = 2

    public var description: String {
        get {
            switch(self) {

            case .inactive:
                return "Inactive"
            case .test:
                return "Test"

            case .active:
                return "Active"
            }
        }
    }

    public static func valueOfKey(_ value: Int?) -> ActiveStatus {

        return ActiveStatus.allCases.first { $0.rawValue == value } ?? ActiveStatus.inactive
    }

    public static func fromJson(_ key: String, _ json: JSON) -> ActiveStatus? {

        var result: ActiveStatus? = nil
        let value = JsonHelper.shared.parseString(key, json: json)
        value.whenNotNil {

            result = parseFromName($0)
        }

        return result
    }

    public static func fromJsonArray(_ key: String, _ json: JSON) -> [ActiveStatus]? {

        let list = JsonHelper.shared.parseStringList(key, json: json)

        return list?.compactMap { parseFromName($0) }
    }

    public func toJson(_ key: String, _ json: inout JSON) {

        JsonHelper.shared.setValue(self.description, key: key, json: &json)
    }

    public static func toJsonArray(_ array: [ActiveStatus], _ key: String, _ json: inout JSON) {

        let list = array.compactMap { $0.description }
        JsonHelper.shared.setValue(list, key: key, json: &json)
    }

    public static func parseFromName(_ value: String?) -> ActiveStatus? {

        return ActiveStatus.allCases.first { $0.description == value }
    }

    public static func parseFromNameList(_ list: [String]) -> [ActiveStatus]? {

        return list.compactMap { ActiveStatus.parseFromName($0) }
    }
}
