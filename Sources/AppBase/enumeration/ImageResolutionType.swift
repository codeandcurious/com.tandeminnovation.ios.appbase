/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

public enum ImageResolutionType: Int, CustomStringConvertible, CaseIterable {

    case res1080p = 1
    case res720p = 2
    case res480p = 3
    case res240p = 4
    case res100p = 5
    case thumbnail = 6
    case thumbnail200 = 7
    case thumbnail300 = 8

    public var description: String {
        get {
            switch (self) {
            case .res1080p:
                return "Res1080p"
            case .res720p:
                return "Res720p"
            case .res480p:
                return "Res480p"
            case .res240p:
                return "Res240p"
            case .res100p:
                return "Res100p"
            case .thumbnail:
                return "Thumbnail"
            case .thumbnail200:
                return "Thumbnail200"
            case .thumbnail300:
                return "Thumbnail300"
            }
        }
    }

    public static func parseFromString(_ value: String?) -> ImageResolutionType? {
        if let value = value {
            switch value {
            case "Res1080p":
                return res1080p

            case "Res720p":
                return res720p

            case "Res480p":
                return res480p

            case "Res240p":
                return res240p

            case "Res100p":
                return res100p

            case "Thumbnail":
                return thumbnail

            case "Thumbnail200":
                return thumbnail200

            case "Thumbnail300":
                return thumbnail300

            default:
                return nil
            }
        }

        return nil
    }
}
