/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit
import os

private var logHelper = LogHelper.shared

public func setLogHelper(_ log: LogHelper) {
    logHelper = log
}

public func logVerbose(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {

    logHelper.verbose(message(), path, function, line: line)
}

public func logDebug(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {

    logHelper.debug(message(), path, function, line: line)
}

public func logInfo(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {

    logHelper.info(message(), path, function, line: line)
}

public func logWarning(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {

    logHelper.warning(message(), path, function, line: line)
}

public func logError(_ message: @autoclosure () -> Any, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {

    logHelper.error(message(), path, function, line: line)
}

open class LogHelper {

    public static let shared = LogHelper()

    public init() {
    }

    open func verbose(_ message: @autoclosure () -> Any, _
    path: String = #file, _ function: String = #function, line: Int = #line) {

        NSLog("\r🆚VERBOSE LOG🆚 \r\(message()) \r\rpath-> \(path) \rfunction-> \(function) \rline-> \(line) \r🆚END VERBOSE LOG🆚")
    }

    open func debug(_ message: @autoclosure () -> Any, _
    path: String = #file, _ function: String = #function, line: Int = #line) {

        if CompilerHelper.shared.isDebugging {

            NSLog("\r🪲DEBUG LOG🪲 \r\(message()) \r\rpath-> \(path) \rfunction-> \(function) \rline-> \(line) \r🪲END DEBUG LOG🪲")
        }
    }

    open func info(_ message: @autoclosure () -> Any, _
    path: String = #file, _ function: String = #function, line: Int = #line) {

        NSLog("\rℹ️INFO LOGℹ️ \r\(message()) \r\rpath-> \(path) \rfunction-> \(function) \rline-> \(line) \rℹ️END INFO LOGℹ️")
    }

    open func warning(_ message: @autoclosure () -> Any, _
    path: String = #file, _ function: String = #function, line: Int = #line) {

        NSLog("\r⚠️WARNING LOG⚠️ \r\(message()) \r\rpath-> \(path) \rfunction-> \(function) \rline-> \(line) \r⚠️END WARNING LOG⚠️")
    }

    open func error(_ message: @autoclosure () -> Any, _
    path: String = #file, _ function: String = #function, line: Int = #line) {

        NSLog("\r⛔️ERROR LOG⛔️ \r\(message()) \r\rpath-> \(path) \rfunction-> \(function) \rline-> \(line) \r⛔️END ERROR LOG⛔️")
    }
}
