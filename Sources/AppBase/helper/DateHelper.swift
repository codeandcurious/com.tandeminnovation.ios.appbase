/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

public struct DateHelper {

	public static let DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
	public static let DEFAULT_LOCALE = Locale(identifier: "pt_PT")
	public static let DEFAULT_TIMEZONE = TimeZone(secondsFromGMT: 0)
	private static func getDateFormatter(with locale: Locale = DEFAULT_LOCALE, dateFormat: String = DEFAULT_DATETIME_FORMAT) -> DateFormatter {

		let dateFormatter = DateFormatter()
		dateFormatter.locale = locale
		if !dateFormat.lowercased().contains("z") { /// If doesnt contains timezone then add the default

			dateFormatter.timeZone = DEFAULT_TIMEZONE
		}
		dateFormatter.dateFormat = dateFormat
		return dateFormatter
	}

	/**
     Parses the given `String` to `Date`.
     
     - Parameter `date`: the `String` to be parsed to `Date`
     `format`: desired format, default is `yyyy-MM-dd'T'HH:mm:ss.SSSZ`
     `locale`: desired locale, default is `en_US_POSIX`
     
     - Returns: `Date`, or nil if the conversion wasn't successful.
     */
	public static func parse(date: String, format: String = DEFAULT_DATETIME_FORMAT, locale: Locale = DEFAULT_LOCALE) -> Date? {

		return self.getDateFormatter(with: locale, dateFormat: format).date(from: date)
	}

	/**
     Formats the given `Date` to `String`.
     
     - Parameter `date`: the date to be parsed to `String`
     `format`: desired format, default is `yyyy-MM-dd'T'HH:mm:ss.SSSZ`
     `locale`: desired locale, default is `en_US_POSIX`
     
     - Returns: `String`, or nil if the conversion wasn't successful.
     */
	public static func format(date: Date, format: String = DEFAULT_DATETIME_FORMAT, locale: Locale = DEFAULT_LOCALE) -> String {

		return self.getDateFormatter(with: locale, dateFormat: format).string(from: date)
	}

	public static func timeElapsed(startDate: Date, endDate: Date, calendarComponent: Calendar.Component) -> Int? {
		let timeInterval = Calendar.current.dateComponents([calendarComponent], from: startDate, to: endDate)

		switch calendarComponent {

		case .era:
			return timeInterval.era

		case .year:
			return timeInterval.year

		case .month:
			return timeInterval.month

		case .day:
			return timeInterval.day

		case .hour:
			return timeInterval.hour

		case .minute:
			return timeInterval.minute

		case .second:
			return timeInterval.second

		case .nanosecond:
			return timeInterval.nanosecond

		case .weekday:
			return timeInterval.weekday

		case .weekdayOrdinal:
			return timeInterval.weekdayOrdinal

		case .quarter:
			return timeInterval.quarter

		case .weekOfMonth:
			return timeInterval.weekOfMonth

		case .weekOfYear:
			return timeInterval.weekOfYear

		case .yearForWeekOfYear:
			return timeInterval.yearForWeekOfYear

		default:
			return nil
		}
	}

	public static func isSameDay(date1: Date, date2: Date) -> Bool {

		return DateHelper.isInSameTime(date1: date1, date2: date2, timeUnity: Calendar.Component.day)
	}

	public static func isSameMonth(date1: Date, date2: Date) -> Bool {

		return DateHelper.isInSameTime(date1: date1, date2: date2, timeUnity: Calendar.Component.month)
	}

	public static func isSameYear(date1: Date, date2: Date) -> Bool {

		return DateHelper.isInSameTime(date1: date1, date2: date2, timeUnity: Calendar.Component.year)
	}

	public static func isInSameTime(date1: Date, date2: Date, timeUnity: Calendar.Component) -> Bool {

		return Calendar.current.isDate(date1, equalTo: date2, toGranularity: timeUnity)
	}

	/// Creates a new date by combining the year, month and day from the first argument, and the hour, minute and second from the second
	public static func combine(date: Date, withTime time: Date) -> Date? {
		let calendar = Calendar.current
		let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
		let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)

		var mergedComponments = DateComponents()
		mergedComponments.year = dateComponents.year!
		mergedComponments.month = dateComponents.month!
		mergedComponments.day = dateComponents.day!
		mergedComponments.hour = timeComponents.hour!
		mergedComponments.minute = timeComponents.minute!
		mergedComponments.second = timeComponents.second!

		return calendar.date(from: mergedComponments)
	}
}
