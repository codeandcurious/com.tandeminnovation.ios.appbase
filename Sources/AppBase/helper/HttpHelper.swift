/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SystemConfiguration
import Alamofire
import SwiftyJSON

open class HttpHelper {

    @available(*, deprecated, renamed: "HTTP_CODE")
    public struct Codes {

    }

    public struct HTTP_CODE {
        public static let NETWORK_NOT_FOUND = -1
        public static let OK = 200
        public static let CREATED = 201
        public static let ACCEPTED = 202
        public static let NOT_AUTHORITATIVE = 203
        public static let NO_CONTENT = 204
        public static let RESET = 205
        public static let PARTIAL = 206
        public static let MULT_CHOICE = 300
        public static let MOVED_PERM = 301
        public static let MOVED_TEMP = 302
        public static let SEE_OTHER = 303
        public static let NOT_MODIFIED = 304
        public static let USE_PROXY = 305
        public static let BAD_REQUEST = 400
        public static let UNAUTHORIZED = 401
        public static let PAYMENT_REQUIRED = 402
        public static let FORBIDDEN = 403
        public static let NOT_FOUND = 404
        public static let BAD_METHOD = 405
        public static let NOT_ACCEPTABLE = 406
        public static let PROXY_AUTH = 407
        public static let CLIENT_TIMEOUT = 408
        public static let CONFLICT = 409
        public static let GONE = 410
        public static let LENGTH_REQUIRED = 411
        public static let PRECON_FAILED = 412
        public static let ENTITY_TOO_LARGE = 413
        public static let REQ_TOO_LONG = 414
        public static let UNSUPPORTED_TYPE = 415
        public static let INTERNAL_ERROR = 500
        public static let NOT_IMPLEMENTED = 501
        public static let BAD_GATEWAY = 502
        public static let UNAVAILABLE = 503
        public static let GATEWAY_TIMEOUT = 504
        public static let VERSION = 505

    }

    open var alamofireManager: SessionManager!

    open var timeout: TimeInterval = 60 //seconds

    public static let shared = HttpHelper()

    public static var hasConnectivity: Bool {
        var zeroAddress: sockaddr_in = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability: SCNetworkReachability? = withUnsafePointer(to: &zeroAddress) {

            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in

                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags.connectionAutomatic

        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {

            return false
        }

        let isReachable: Bool = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection: Bool = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0

        return (isReachable && !needsConnection)
    }

    public init() {

        setupAlamofireManager()
    }

    open func setTimeOut(timeout: Int) {

        self.timeout = TimeInterval(timeout)
        invalidateAndCancelAllTasks()
        setupAlamofireManager()
    }

    open func setupAlamofireManager() {

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = self.timeout
        configuration.timeoutIntervalForResource = self.timeout
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }

    open func invalidateAndCancelAllTasks() {

        alamofireManager.session.invalidateAndCancel()
        setupAlamofireManager()
    }

    open func makeRequest(url: String, method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any> {

        guard let encodedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)?.replacingOccurrences(of: "+", with: "%2B") else {

            throw RuntimeError("Error encoding URL")
        }
        guard let requestUrl = URL(string: encodedString) else {

            throw RuntimeError("Error initializing URL")
        }
        if CompilerHelper.shared.isDebugging {

            let performanceHelper = PerformanceHelper()
            performanceHelper.logTime(.start)
            let response = self.makeRequest(requestUrl, method: method, headers: headers).responseJSON()
            performanceHelper.logTime(.stop)
            let timeStamp = performanceHelper.printElapsedTime(forDescription: url, in: .milliseconds)
            PerformanceLogger.shared.appendHttpLoggerItem(timeStamp)

            return response
        } else {

            return self.makeRequest(url, method: method, headers: headers).responseJSON()
        }
    }

    open func makeRequest(url: String, parameter: [String: String], encoding: ParameterEncoding) throws -> DataResponse<Any> {

        guard let encodedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)?.replacingOccurrences(of: "+", with: "%2B") else {

            throw RuntimeError("Error encoding URL")
        }
        guard let requestUrl = URL(string: encodedString) else {

            throw RuntimeError("Error initializing URL")
        }
        if CompilerHelper.shared.isDebugging {

            let performanceHelper = PerformanceHelper()
            performanceHelper.logTime(.start)
            let response = self.makeRequest(requestUrl, parameters: parameter, encoding: encoding).responseJSON()
            performanceHelper.logTime(.stop)
            let timeStamp = performanceHelper.printElapsedTime(forDescription: url, in: .milliseconds)
            PerformanceLogger.shared.appendHttpLoggerItem(timeStamp)

            return response
        } else {

            return self.makeRequest(url, parameters: parameter, encoding: encoding).responseJSON()
        }
    }

    open func makeRequest(url: String, parameter: [String: Any], method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any> {

        guard let encodedString = url.encodedUrl else {

            throw RuntimeError("Error encoding URL")
        }
        guard let requestUrl = URL(string: encodedString) else {

            throw RuntimeError("Error initializing URL")
        }
        if CompilerHelper.shared.isDebugging {

            let performanceHelper = PerformanceHelper()
            performanceHelper.logTime(.start)
            let response = self.makeRequest(requestUrl, method: method, parameters: parameter, headers: headers).responseJSON()
            performanceHelper.logTime(.stop)
            let timeStamp = performanceHelper.printElapsedTime(forDescription: url, in: .milliseconds)
            PerformanceLogger.shared.appendHttpLoggerItem(timeStamp)

            return response
        } else {

            return self.makeRequest(url, method: method, parameters: parameter, headers: headers).responseJSON()
        }
    }

    open func makeRequest(url: String, body: String, method: HTTPMethod, headers: HTTPHeaders) throws -> DataResponse<Any> {

        guard let encodedString = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)?.replacingOccurrences(of: "+", with: "%2B") else {

            throw RuntimeError("Error encoding URL")
        }
        guard let requestUrl = URL(string: encodedString) else {

            throw RuntimeError("Error initializing URL")
        }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = method.rawValue
        if !body.isEmpty {

            request.httpBody = body.data(using: .utf8, allowLossyConversion: false)
        }
        for (field, value) in headers {

            request.setValue(value, forHTTPHeaderField: field)
        }
        let performanceHelper = PerformanceHelper()

        if CompilerHelper.shared.isDebugging {

            performanceHelper.logTime(.start)
            let response = self.makeRequest(request).responseJSON()
            performanceHelper.logTime(.stop)
            let timeStamp = performanceHelper.printElapsedTime(forDescription: url, in: .milliseconds)
            PerformanceLogger.shared.appendHttpLoggerItem(timeStamp)

            return response
        } else {

            return self.makeRequest(request).responseJSON()
        }
    }

    open func getDataIfSuccessful(response: DataResponse<Any>) -> JSON? {

        var result: JSON? = nil
        if let json = response.result.value {

            result = JSON(json)
        }
        return result
    }

    open func unsecureSessionManager(urlSessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default) -> SessionManager {
        let trustPolicies = UnsecureTrustPolicyManager(policies: [:])

        return Alamofire.SessionManager(configuration: urlSessionConfiguration, delegate: SessionDelegate(), serverTrustPolicyManager: trustPolicies)
    }

    class UnsecureTrustPolicyManager: ServerTrustPolicyManager {

        // Override this function in order to trust any self-signed https
        open override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {

            return ServerTrustPolicy.disableEvaluation
        }
    }

    @discardableResult
    open func makeRequest(_ urlConvertible: URLConvertible, method: HTTPMethod = .get, parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {

        headers.whenNotNilOrEmpty { (httpHeaders) in

            if httpHeaders.keys.contains("If-None-Match") { // needed because Alamofire is caching request by default and when you use etags it will always give you 200 instead of 304
                do {

                    let url = try urlConvertible.asURL()
                    let urlRequest = URLRequest(url: url)
                    URLCache.shared.removeCachedResponse(for: urlRequest)
                } catch {

                    LogHelper.shared.error(error.localizedDescription)
                }
            }
        }

        return alamofireManager.request(urlConvertible, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }

    @discardableResult
    open func makeRequest(_ urlRequest: URLRequest) -> DataRequest {

        urlRequest.allHTTPHeaderFields.whenNotNilOrEmpty { (httpHeaders) in

            if httpHeaders.keys.contains("If-None-Match") { // needed because Alamofire is caching request by default and when you use etags it will always give you 200 instead of 304

                urlRequest.url.whenNotNil { (url) in

                    let urlRequest = URLRequest(url: url)
                    URLCache.shared.removeCachedResponse(for: urlRequest)
                }
            }
        }


        return alamofireManager.request(urlRequest)
    }
}
