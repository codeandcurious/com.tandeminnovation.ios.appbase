/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import CoreTelephony
import LocalAuthentication
import AudioToolbox

public struct DeviceHelper {

    public static func vibrate() {
        AudioServicesPlaySystemSound(1519)
    }

    public static var orientation: UIDeviceOrientation { UIDevice.current.orientation }

    public static var screenHeight: CGFloat {

        return UIScreen.main.bounds.height
    }

    public static var screenWidth: CGFloat {

        return UIScreen.main.bounds.width
    }

    public static func wasInstalledFromTestFlight() -> Bool {
        if isRunningInSimulator() {

            return false
        } else {
            if isAppStoreReceiptSandbox() && !hasEmbeddedMobileProvision() {

                return true
            } else {

                return false
            }
        }
    }

    public static func wasInstalledFromAppStore() -> Bool {
        if isRunningInSimulator() {

            return false
        } else {
            if isAppStoreReceiptSandbox() || hasEmbeddedMobileProvision() {

                return false
            } else {

                return true
            }
        }
    }

    private static func hasEmbeddedMobileProvision() -> Bool {
        if let _ = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") { return true }

        return false
    }

    private static func isAppStoreReceiptSandbox() -> Bool {
        if isRunningInSimulator() {

            return false
        } else {
            if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL {

                if appStoreReceiptURL.lastPathComponent == "sandboxReceipt" {

                    return true
                }
            }
            return false
        }
    }

    public static func isRunningInSimulator() -> Bool {
        #if arch(i386) || arch(x86_64)
            return true
        #else
            return false
        #endif
    }

    public static var osVersion: String {

        return UIDevice.current.systemVersion
    }

    public static var biometricType: LAContext.BiometricTypeEnum {

        return LAContext().biometricType
    }

    public static var deviceLanguage: String? {

        return Locale.current.languageCode
    }

    public static var deviceIdentifier: String {

        return UIDevice.current.identifierForVendor!.uuidString
    }

    /// Returns the 1st carrier name
    public static var carrierName: String? {

        var carrier: CTCarrier?
        if #available(iOS 12, *) {

            let carriers = CTTelephonyNetworkInfo().serviceSubscriberCellularProviders
            carrier = carriers?.values.first
        }
        else {

            carrier = CTTelephonyNetworkInfo().subscriberCellularProvider
        }
        let carrierName = carrier?.carrierName

        return carrierName
    }

    public static var modelVersion: String? {

        var result: String? = nil
        if let key = "hw.machine".cString(using: String.Encoding.utf8) {

            var size: Int = 0
            sysctlbyname(key, nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: Int(size))
            sysctlbyname(key, &machine, &size, nil, 0)

            result = String(cString: machine)
        }
        return result
    }

    @available(*, deprecated, renamed: "LocalizationHelper.locationCode")
    public static var locationCode: String? {

        return LocalizationHelper.locationCode
    }

    public static var deviceName: String = UIDevice.current.name

    public static var modelName: String = UIDevice.modelName

}

