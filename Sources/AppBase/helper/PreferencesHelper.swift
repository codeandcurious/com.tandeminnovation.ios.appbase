/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import RNCryptor

open class PreferencesHelper: NSObject {

    //MARK: Constructor
    public static let shared = PreferencesHelper()
    fileprivate let userDefaults: UserDefaults = UserDefaults.standard

    fileprivate override init() {

        super.init()
    }

    @available(*, deprecated, message: "use the appropriate getValue(forKey:defaultValue:password:)")
    open func getPreferences(forKey key: String) -> Any? {

        return userDefaults.object(forKey: key)
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    open func getPreferences(_ key: String, defaultValue: String?) -> String? {
        if let result = self.getPreferences(forKey: key) {

            return result as? String
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    @nonobjc
    open func getPreferences(_ key: String, defaultValue: Date?) -> Date? {
        if let result = self.getPreferences(forKey: key) {

            return result as? Date
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    open func getPreferences(_ key: String, defaultValue: Bool?) -> Bool? {
        if let result = self.getPreferences(forKey: key) {

            return result as? Bool
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    open func getPreferences(_ key: String, defaultValue: Int?) -> Int? {
        if let result = self.getPreferences(forKey: key) {

            return result as? Int
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    open func getPreferences(_ key: String, defaultValue: Double?) -> Double? {
        if let result = self.getPreferences(forKey: key) {

            return result as? Double
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "getValue(forKey:defaultValue:password:)")
    open func getPreferences(_ key: String, defaultValue: Float?) -> Float? {
        if let result = self.getPreferences(forKey: key) {

            return result as? Float
        } else {

            return defaultValue
        }
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    open func setPreferences(_ key: String, value: Int) {
        self.userDefaults.set(value, forKey: key)
        self.userDefaults.synchronize()
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    @nonobjc
    open func setPreferences(_ key: String, value: String) {
        self.userDefaults.setValue(value, forKey: key)
        self.userDefaults.synchronize()
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    @nonobjc
    open func setPreferences(_ key: String, value: Date) {
        self.userDefaults.setValue(value, forKey: key)
        self.userDefaults.synchronize()
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    @nonobjc
    open func setPreferences(_ key: String, value: Bool) {
        self.userDefaults.set(value, forKey: key)
        self.userDefaults.synchronize()
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    @nonobjc
    open func setPreferences(_ key: String, value: Double) {
        self.userDefaults.set(value, forKey: key)
        self.userDefaults.synchronize()
    }

    @available(*, deprecated, renamed: "set(value:forKey:password:)")
    @nonobjc
    open func setPreferences(_ key: String, value: Float) {
        self.userDefaults.set(value, forKey: key)
        self.userDefaults.synchronize()
    }

    /// Encrypts string into UserDefaults if a password is provided.
    open func set(value: String, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        }
        else {

            data = value.data
        }
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Encrypts date into UserDefaults if a password is provided.
    open func set(value: Date, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        }
        else {

            data = value.data
        }
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Encrypts bool into UserDefaults if a password is provided.
    open func set(value: Bool, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        }
        else {

            data = value.data
        }
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Encrypts int into UserDefaults if a password is provided.
    open func set(value: Int, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        }
        else {

            data = value.data
        }
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Encrypts double into UserDefaults if a password is provided.
    open func set(value: Double, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        }
        else {

            data = value.data
        }

        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Encrypts float into UserDefaults if a password is provided.
    open func set(value: Float, forKey key: String, password: String? = nil) -> Bool {

        var data: Data?
        if password != nil {

            data = RNCryptor.encrypt(data: value.data, withPassword: password!)
        } else {

            data = value.data
        }
        self.userDefaults.set(data, forKey: key)
        return self.userDefaults.synchronize()
    }

    /// Decrypts string from UserDefaults using the password provided.
    open func getValue(forKey key: String, defaultValue: String?, password: String? = nil) -> String? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedString = String(data: decryptedData) {

            return decryptedString
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return String(data: savedData)
        }
        else {

            return defaultValue
        }
    }

    /// Decrypts date from UserDefaults using the password provided.

    open func getValue(forKey key: String, defaultValue: Date?, password: String? = nil) -> Date? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedDate = Date(data: decryptedData) {

            return decryptedDate
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return Date(data: savedData)
        }
        else {

            return defaultValue
        }
    }

    /// Decrypts bool from UserDefaults using the password provided.

    open func getValue(forKey key: String, defaultValue: Bool?, password: String? = nil) -> Bool? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedBool = Bool(data: decryptedData) {

            return decryptedBool
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return Bool(data: savedData)
        } else {

            return defaultValue
        }
    }

    /// Decrypts int from UserDefaults using the password provided.

    open func getValue(forKey key: String, defaultValue: Int?, password: String? = nil) -> Int? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedInt = Int(data: decryptedData) {

            return decryptedInt
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return Int(data: savedData)
        } else {

            return defaultValue
        }
    }

    /// Decrypts double from UserDefaults using the password provided.

    open func getValue(forKey key: String, defaultValue: Double?, password: String? = nil) -> Double? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedDouble = Double(data: decryptedData) {

            return decryptedDouble
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return Double(data: savedData)
        } else {

            return defaultValue
        }
    }

    /// Decrypts float from UserDefaults using the password provided.

    open func getValue(forKey key: String, defaultValue: Float?, password: String? = nil) -> Float? {

        // fetches encrypted data and decrypts it with the provided password
        if password != nil,
            let encryptedData = self.userDefaults.value(forKey: key) as? Data,
            let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: password!),
            let decryptedFloat = Float(data: decryptedData) {

            return decryptedFloat
        }
        else if password == nil,
            let savedData = self.userDefaults.value(forKey: key) as? Data {

            return Float(data: savedData)
        }
        else {

            return defaultValue
        }
    }

    /// Deletes an object from UserDefaults.
    open func removeObject(forKey key: String) {

        self.userDefaults.removeObject(forKey: key)
        self.userDefaults.synchronize()
    }
}

