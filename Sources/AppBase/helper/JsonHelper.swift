/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON
import Alamofire

open class JsonHelper {

	public static let shared = JsonHelper()

	fileprivate init() {

	}

	open func hasKey(_ key: String, json: JSON) -> Bool {

		return json[key].exists()
	}

	open func hasValueForKey(_ key: String, json: JSON) -> Bool {

		return json[key].exists()
	}

	open func setValue(_ value: String?, key: String, json: inout JSON) {

		json[key].string = value
	}

	open func setValue(_ value: Int?, key: String, json: inout JSON) {

		json[key].int = value
	}

	open func setValue(_ value: Float?, key: String, json: inout JSON) {

		json[key].float = value
	}

	open func setValue(_ value: Double?, key: String, json: inout JSON) {

		json[key].double = value
	}

	open func setValue(_ value: Bool?, key: String, json: inout JSON) {

		json[key].bool = value
	}

	open func setValue(_ value: [Any]?, key: String, json: inout JSON) {

		json[key].arrayObject = value
	}

	open func setValue(_ value: JSON?, key: String, json: inout JSON) {

		json[key].object = value ?? JSON.null
	}

	open func parseString(_ key: String, json: JSON) -> String? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].string
		}
	}

	open func parseDouble(_ key: String, json: JSON) -> Double? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].double
		}
	}

	open func parseFloat(_ key: String, json: JSON) -> Float? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].float
		}
	}

	open func parseBool(_ key: String, json: JSON) -> Bool? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].bool
		}
	}

	open func parseInt(_ key: String, json: JSON) -> Int? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].int
		}
	}

	open func parseLong(_ key: String, json: JSON) -> Int? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].int
		}
	}

	open func parseJSON(_ key: String, json: JSON) -> JSON? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key]
		}
	}

	open func parseBinary(_ key: String, json: JSON) -> Data? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {
			var result: Data? = nil
			if let jsonValue = json[key].string {

				result = jsonValue.data(using: .utf8)
			}

			return result
		}
	}

	open func parseChar(_ key: String, json: JSON) -> Character? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {
			var result: Character? = nil
			if let jsonValue = json[key].string {

				result = Character(jsonValue)
			}

			return result
		}
	}

	open func parseUUID(_ key: String, json: JSON) -> UUID? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {
			var result: UUID? = nil
			if let jsonValue = json[key].string { result = UUID(uuidString: jsonValue) }

			return result
		}
	}

	open func parseEnum<TEnum>(_ key: String, json: JSON) -> TEnum? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			let value = json[key] as! TEnum

			return value
		}
	}

	open func parseDate(_ key: String, json: JSON) -> Date? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {
			var result: Date? = nil
			if let value = json[key].string {

				result = DateHelper.parse(date: value)
			} else {

				result = nil
			}

			return result
		}
	}

	open func parseDate(_ key: String, json: JSON, dateFormat: String) -> Date? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {
			var result: Date? = nil
			if let value = json[key].string {

				result = DateHelper.parse(date: value, format: dateFormat)
			} else {

				result = nil
			}

			return result
		}
	}

	open func parseArray(_ key: String, json: JSON) -> Array<JSON>? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].array
		}
	}

	open func parseAnyArray(_ key: String, json: JSON) -> Array<Any>? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayObject
		}
	}

	open func parseIntList(_ key: String, json: JSON) -> Array<Int>? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map { $0.intValue }
		}
	}

	open func parseBoolList(_ key: String, json: JSON) -> Array<Bool>? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map { $0.boolValue }
		}
	}

	open func parseDoubleList(_ key: String, json: JSON) -> Array<Double>? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map { $0.doubleValue }
		}
	}

	open func parseLongList(_ key: String, json: JSON) -> [Int]? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map { $0.intValue }
		}
	}

	open func parseStringList(_ key: String, json: JSON) -> [String]? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map { $0.stringValue }
		}
	}

	open func parseJsonToString(_ json: JSON) -> String? {
		if json.type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json.rawString()
		}
	}

	open func parseDateList(_ key: String, json: JSON) -> [Date]? {
		if json[key].type == SwiftyJSON.Type.null {

			return nil
		} else {

			return json[key].arrayValue.map({ (json) -> Date in
				var result: Date? = nil
				if let value = json[key].string {

					result = DateHelper.parse(date: value)
				} else {

					result = nil
				}

				return result ?? Date()
			})
		}
	}

	open func parseString(json: JSON) -> String? {

		return json.rawString()
	}

	open func parseDouble(json: JSON) -> Double? {

		return json.double
	}

	open func parseFloat(json: JSON) -> Float? {

		return json.float
	}

	open func parseBool(json: JSON) -> Bool? {

		return json.bool
	}

	open func parseInt(json: JSON) -> Int? {

		return json.int
	}

	open func parseLong(json: JSON) -> Int? {

		return json.int
	}

	open func parseJSON(json: JSON) -> JSON? {

		return json
	}

	open func parseBinary(json: JSON) -> Data? {
		var result: Data? = nil
		if let jsonValue = json.rawString() {

			result = jsonValue.data(using: .utf8)
		}

		return result
	}

	open func parseChar(json: JSON) -> Character? {
		var result: Character? = nil
		if let jsonValue = json.rawString() {

			result = Character(jsonValue)
		}

		return result
	}

	open func parseUUID(json: JSON) -> UUID? {
		var result: UUID? = nil
		if let jsonValue = json.rawString() { result = UUID(uuidString: jsonValue) }

		return result
	}

	open func parseEnum<TEnum>(json: JSON) -> TEnum? {
		let value = json as! TEnum

		return value
	}

	open func parseDate(json: JSON) -> Date? {
		var result: Date? = nil
		if let value = json.rawString() {

			result = DateHelper.parse(date: value)
		} else {

			result = nil
		}

		return result
	}

	open func parseDate(json: JSON, dateFormat: String) -> Date? {
		var result: Date? = nil
		if let value = json.rawString() {

			result = DateHelper.parse(date: value, format: dateFormat)
		} else {

			result = nil
		}

		return result
	}

	open func parseArray(json: JSON) -> Array<JSON>? {

		return json.array
	}

	open func parseAnyArray(json: JSON) -> Array<Any>? {

		return json.arrayObject
	}

	open func parseIntList(json: JSON) -> Array<Int>? {

		return json.arrayValue.map { $0.intValue }
	}

	open func parseBoolList(json: JSON) -> Array<Bool>? {

		return json.arrayValue.map { $0.boolValue }
	}

	open func parseDoubleList(json: JSON) -> Array<Double>? {

		return json.arrayValue.map { $0.doubleValue }
	}

	open func parseLongList(json: JSON) -> [Int]? {

		return json.arrayValue.map { $0.intValue }
	}

	open func parseStringList(json: JSON) -> [String]? {

		return json.arrayValue.map { $0.stringValue }
	}

	open func parseDateList(json: JSON) -> [Date]? {

		return json.arrayValue.map({ (json) -> Date in
			var result: Date? = nil
			if let value = json.rawString() {

				result = DateHelper.parse(date: value)
			} else {

				result = nil
			}

			return result ?? Date()
		})
	}

	open func parseJsonToData(_ json: JSON) -> Data? {
		if json.type == SwiftyJSON.Type.null {

			return nil
		} else {

			var result: Data?
			do {
				result = try json.rawData()
			} catch {
				result = nil
			}

			return result
		}
	}

	open func parseJsonIntoDictionary(_ jsonString: String) -> NSDictionary? {
		return JSON.init(parseJSON: jsonString).dictionaryObject as NSDictionary?
	}

	open func parseJsonToArray(_ jsonString: String) -> Array<Any>? {

		return JSON.init(parseJSON: jsonString).arrayObject
	}

	open func format(_ value: String) -> String {
		return value
	}

	open func format(_ value: Bool) -> Bool {
		return value
	}

	open func format(_ value: Data) -> String {
		return String(data: value, encoding: String.Encoding.utf8)!
	}

	open func format(_ value: Character) -> String {
		return "\(value)"
	}

	open func formatEnum(_ value: CustomStringConvertible) -> String {
		return value.description
	}

	open func format(_ value: Double) -> Double {
		return value
	}

	open func format(_ value: Float) -> Float {
		return value
	}

	open func format(_ value: Int) -> Int {
		return value
	}

	open func format(_ date: Date) -> String {
		return DateHelper.format(date: date)
	}

/// Formats the given date with the specified format, using the default Locale
	open func format(_ date: Date, dateFormat: String) -> String {
		return DateHelper.format(date: date, format: dateFormat)
	}

	open func format(_ value: UUID) -> String {
		return value.uuidString
	}

	open func format(_ value: [Int]) -> [Int] {
		return value
	}

	open func format(_ value: JSON) -> String? {
		return prettyPrint(value)
	}

	open func format(_ value: [JSON]) -> String? {
		return self.prettyPrint(JSON(value))
	}

	open func prettyPrint(_ value: JSON) -> String? {
		return value.rawString(.utf8, options: .init(rawValue: 0))
	}

	open func prettyPrint(_ dataResponse: DataResponse<Any>) -> String? {

		return prettyPrint(JSON(dataResponse.result.value ?? JSON()))?.unescaped
	}

/// Checks if the provided JSON is valid
	open func isValid(_ value: JSON) -> Bool {
		return value != JSON.null && (value.dictionary != nil || value.array != nil)
	}

/// Checks if string is a valid JSON
	open func isValid(_ value: String) -> Bool {
		let json = JSON(value)

		return json != JSON.null && (json.dictionary != nil || json.array != nil)
	}
}
