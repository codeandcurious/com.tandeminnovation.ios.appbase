/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import MapKit
import SafariServices
import MessageUI

public struct ActionHelper {

    /// Opens Apple Maps on a give coordinate and shows a nome for said location
    ///
    /// - Parameters:
    ///   - locationName: Name to be shown
    ///   - phoneNumber: Phone number to be called from Apple Maps
    ///   - url: URL to be used by Apple Maps
    ///   - clLocationCoordinate2D: The coordinates object
    ///   - launchOptions: Apple Maps Options
    public static func openInAppleMaps(locationName: String?, phoneNumber: String? = nil, url: URL? = nil, clLocationCoordinate2D: CLLocationCoordinate2D, launchOptions: [String: Any]?) {

        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: clLocationCoordinate2D, addressDictionary: nil))
        if let locationName = locationName {

            mapItem.name = locationName
            mapItem.phoneNumber = phoneNumber
            mapItem.url = url
        }
        mapItem.openInMaps(launchOptions: launchOptions)
    }

    @available(*, deprecated, message: "This method will be deprecated with AppBase release for iOS 13")
    public static func shareUrl(url: URL, uiViewController: UIViewController, excludedActivityTypes: [UIActivity.ActivityType]?) {

        let sharedObjects: [AnyObject] = [url as AnyObject]
        let activityViewController = UIActivityViewController(activityItems: sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = uiViewController.view
        activityViewController.excludedActivityTypes = excludedActivityTypes
        uiViewController.present(activityViewController, animated: true, completion: nil)
    }

    public static func openUrl(url: String, options: [UIApplication.OpenExternalURLOptionsKey: Any] = [:], completionHandler completion: ((Bool) -> Void)? = nil) {
        if let url = URL(string: url) {

            self.openUrl(url: url, options: options, completionHandler: completion)
        } else {

            logDebug("Error parsing url")
        }
    }

    public static func openUrl(url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any] = [:], completionHandler completion: ((Bool) -> Void)? = nil) {

        UIApplication.shared.open(url, options: options, completionHandler: completion)
    }

    public static func openSafariViewControllerUrl(urlString: String, dismissButtonStyle: SFSafariViewController.DismissButtonStyle = .done, controlTintColor: UIColor = UIColor.black, barTintColor: UIColor = UIColor.black, modalTransitionStyle: UIModalTransitionStyle = .coverVertical, modalPresentationStyle: UIModalPresentationStyle = .automatic, _ animated: Bool = true, sender: UIViewController) {

        if let url = URL(string: urlString) {

            self.openSafariViewControllerUrl(url: url, dismissButtonStyle: dismissButtonStyle, controlTintColor: controlTintColor, barTintColor: barTintColor, modalTransitionStyle: modalTransitionStyle, modalPresentationStyle: modalPresentationStyle, animated, sender: sender)
        } else {

            logDebug("Error parsing url")
        }
    }

    public static func openSafariViewControllerUrl(url: URL, dismissButtonStyle: SFSafariViewController.DismissButtonStyle = .done, controlTintColor: UIColor = UIColor.black, barTintColor: UIColor = UIColor.black, modalTransitionStyle: UIModalTransitionStyle = .coverVertical, modalPresentationStyle: UIModalPresentationStyle = .automatic, _ animated: Bool = true, sender: UIViewController) {

        let safariConfiguration: SFSafariViewController.Configuration = SFSafariViewController.Configuration()
        safariConfiguration.barCollapsingEnabled = true
        safariConfiguration.entersReaderIfAvailable = false

        let safariViewController: SFSafariViewController = SFSafariViewController(url: url, configuration: safariConfiguration)
        safariViewController.dismissButtonStyle = dismissButtonStyle
        safariViewController.preferredControlTintColor = controlTintColor
        safariViewController.preferredBarTintColor = barTintColor

        safariViewController.modalTransitionStyle = modalTransitionStyle
        safariViewController.modalPresentationStyle = modalPresentationStyle
        sender.present(safariViewController, animated: animated)
    }
    
    public static func callPhoneNumber(_ number: String?) {
        
        guard let phoneNumber: String = number?.replacingOccurrences(of: "tel:", with: "") else { return }
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "tel"
        urlComponents.host = phoneNumber
        
        if let callUrl: URL = urlComponents.url, UIApplication.shared.canOpenURL(callUrl) {
            
            UIApplication.shared.open(callUrl)
        }
    }
    
    /// This method will check if there is GoogleMaps and/or Waze installed on the device, if it is an alert will be displayed with available options to get direction
    /// - Parameters:
    ///   - alertTitle: The title that should be displayed when multiple options are available
    ///   - alertMessage: The message that should be displayed when multiple options are available
    ///   - presenter: The view controller that will present the alert with options
    ///
    /// Also put these in your info.plist:
    ///    <key>LSApplicationQueriesSchemes</key>
    ///    <array>
    ///        <string>googlechromes</string>
    ///        <string>comgooglemaps</string>
    ///        <string>waze</string>
    ///    </array>
    public static func getAvailableMapsAppsAndGetDirections(alertTitle: String, alertMessage: String, latitude: Double, longitude: Double, tintColor: UIColor? = nil, presenter: UIViewController) {
        
        let appleURL = "http://maps.apple.com/?daddr=\(latitude),\(longitude)"
        let googleURL = "comgooglemaps://?daddr=\(latitude),\(longitude)&directionsmode=driving"
        let wazeURL = "waze://?ll=\(latitude),\(longitude)&navigate=false"
        
        let googleItem = ("Google Map", URL(string:googleURL)!)
        let wazeItem = ("Waze", URL(string:wazeURL)!)
        var installedNavigationApps = [("Apple Maps", URL(string:appleURL)!)]
        
        if UIApplication.shared.canOpenURL(googleItem.1) {
            installedNavigationApps.append(googleItem)
        }
        
        if UIApplication.shared.canOpenURL(wazeItem.1) {
            installedNavigationApps.append(wazeItem)
        }
        
        if installedNavigationApps.count == 1 {
            
            UIApplication.shared.open(installedNavigationApps.first!.1, options: [:], completionHandler: nil)
            return
        }
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .actionSheet)
        
        if let color = tintColor {
            alert.view.tintColor = color
        }
        
        for app in installedNavigationApps {
            let button = UIAlertAction(title: app.0, style: .default, handler: { _ in
                UIApplication.shared.open(app.1, options: [:], completionHandler: nil)
            })
            alert.addAction(button)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        
        presenter.present(alert, animated: true)
    }
    
    ///    <key>LSApplicationQueriesSchemes</key>
    ///    <array>
    ///        <string>readdle-spark</string>
    ///        <string>ymail</string>
    ///        <string>ms-outlook</string>
    ///        <string>googlegmail</string>
    ///    </array>
    public static func composeEmail(delegate: MFMailComposeViewControllerDelegate, presenterViewController: UIViewController?) -> MailComposerBuilder {
        
        return MailComposerBuilder(delegate: delegate, presenter: presenterViewController)
    }
}
