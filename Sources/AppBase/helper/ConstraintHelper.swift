/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit

open class ConstraintHelper: NSLayoutConstraint {

	var byDefaultValue: CGFloat = 0.0
	var byDefaultDistance: CGFloat = 8.0

	@available(*, deprecated, message: "This method will be deprecated with AppBase release for iOS 13")
	public convenience init(item: Any, attribute: NSLayoutConstraint.Attribute, relatedBy: NSLayoutConstraint.Relation, constant: CGFloat) {
		self.init(
			item: item,
			attribute: attribute,
			relatedBy: relatedBy,
			toItem: nil,
			attribute: .notAnAttribute,
			multiplier: 1.0,
			constant: constant
		)
	}

	@available(*, deprecated, message: "This method will be deprecated with AppBase release for iOS 13")
	public convenience init(item: Any, attribute: NSLayoutConstraint.Attribute, relatedBy: NSLayoutConstraint.Relation, toItem: Any?, attribute relatedAttribute: NSLayoutConstraint.Attribute, constant: CGFloat) {
		self.init(
			item: item,
			attribute: attribute,
			relatedBy: relatedBy,
			toItem: toItem,
			attribute: relatedAttribute,
			multiplier: 1.0,
			constant: constant
		)
	}

	@available(*, deprecated, renamed: "constraint(subView:toSuper:)")
	public static func add(subview: UIView, toSuper superView: UIView) {

		self.constraint(subView: subview, toSuper: superView)
	}

	/// Adds a view to another within the same rect/constraints
	///
	/// - Parameters:
	///   - subView: View to be added
	///   - view: View to receive the subview
	public static func constraint(subView: UIView, toSuper view: UIView) {

		subView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(subView)
		subView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
		subView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
		subView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
		subView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
	}
}
