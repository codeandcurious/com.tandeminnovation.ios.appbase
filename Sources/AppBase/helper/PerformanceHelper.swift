/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
/**
 Performance Helper allows the user to calculate performance times.
 
 ## Available Methods: ##
 - logTime
 - getElapsedTime
 - printElapsedTime
 
 */

open class PerformanceHelper {

    var startDate: Date
    var stopDate: Date

    public init() {

        startDate = Date()
        stopDate = Date.distantFuture
    }

    /**
     Register a user determined key to start or end performance measuring.
     
     ### Usage Example: ###
     ````
     PerformanceHelper.logTime(.start, description: "Entire Loop")
     for _ in 0...300000 {
     PerformanceHelper.logTime(.start, description: "Each Loop")
     //...
     PerformanceHelper.logTime(.end, description: "Each Loop")
     }
     PerformanceHelper.logTime(.end, description: "Entire Loop")
     ````
     
     - Parameter type: .start or .end of type PerformanceLog
     
     */
    public func logTime(_ type: PerformanceActionType) {

        let dateNow = Date()
        if type == .start {

            self.startDate = dateNow
        } else {

            self.stopDate = dateNow
        }
    }

    /**
     Prints the elapsed time of a **finished** performance. The user can select the **TimeUnit** which will be reported
     
     ### Usage Example: ###
     ````
     PerformanceHelper.printElapsedTime(forDescription: "Entire Loop" in: .minutes)
     ````
     
     - Parameter type: .start or .end of type PerformanceLog
     - Parameter description: the key used to resgister a performance
     */
    public func printElapsedTime(forDescription description: String, in timeUnit: TimeUnit) -> String {
        let startTimeStamp = getStartTimeStamp()
        let endTimeStamp = getStopTimeStamp()
        var timeElapsed = "\(getElapsedTime(in: timeUnit)) \(timeUnit.description)"

        if self.stopDate == Date.distantFuture {

            timeElapsed = "it wasn't over when this print was called".uppercased()
        }
        let log = ("Time Performance for: ' |\(description)' | StartTime: \(startTimeStamp) | EndTime: \(endTimeStamp) | TimeElapsed: \(timeElapsed)")
        NSLog(log)

        return log
    }

    /**
     Return the elapsed time of a **finished** performance. The user can select the **TimeUnit** which will be reported
     
     ### Usage Example: ###
     ````
     PerformanceHelper.getElapsedTime(forDescription: "Entire Loop" in: .minutes)
     ````
     
     - Parameter type: .start or .end of type PerformanceLog
     - Parameter description: the key used to resgister a performance
     - Returns: **Double** describing the time elapsed in the selected **TimeUnit**
     */
    public func getElapsedTime(in TimeUnit: TimeUnit) -> Int64 {
        let startTimeInterval: Double = startDate.timeIntervalSince1970
        let stopTimeInterval: Double = stopDate.timeIntervalSince1970
        var elapsedTime: Int64 = Int64((startTimeInterval - stopTimeInterval) * 1000)
        switch TimeUnit {

        case .milliseconds:
            elapsedTime = elapsedTime / 1

        case .seconds:
            elapsedTime = elapsedTime / 1000

        case .minutes:
            elapsedTime = (elapsedTime / 1000) / 60

        case .hours:
            elapsedTime = ((elapsedTime / 1000) / 60) / 60

        case .days:
            elapsedTime = (((elapsedTime / 1000) / 60) / 60) / 24
        }

        return abs(elapsedTime)
    }

    public func getStartTimeStamp() -> String {

        return DateHelper.format(date: self.startDate)
    }

    public func getStopTimeStamp() -> String {

        return DateHelper.format(date: self.stopDate)
    }


    /// Show alert with the last logged http request
    /// - Parameters:
    ///   - title: the title to present
    ///   - tintColor: the color of the action
    ///   - presenter: the UiViewController responsible to present
    ///   - completion: optional completion
    public static func showLogger(_ title: String? = "Log", _ tintColor: UIColor? = UIColor.black, _ presenter: UIViewController, _ completion: (() -> Void)? = nil) {

        let longTextMessage = PerformanceLogger.shared.httpLogger.joined(separator: "\n")
        let uiAlertController = UIAlertController(title: title, message: longTextMessage, preferredStyle: .actionSheet)
        uiAlertController.view.tintColor = tintColor
        let copyAction = UIAlertAction.init(title: "Copy&Clear", style: UIAlertAction.Style.default) { (uiAlertAction) in

            UIPasteboard.general.string = longTextMessage
            PerformanceLogger.shared.clearHttpLogger()
        }
        let closeAction = UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil)
        uiAlertController.addAction(copyAction)
        uiAlertController.addAction(closeAction)

        if let popoverController = uiAlertController.popoverPresentationController {

            popoverController.sourceView = presenter.view
            popoverController.sourceRect = CGRect(x: presenter.view.bounds.midX, y: presenter.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        presenter.present(uiAlertController, animated: true, completion: completion)
    }
}

/// Performance Log options
///
/// - start: Marks the Beginning of the log
/// - end: Marks the End of the log
public enum PerformanceActionType {
    ///It declares the start of a log
    case start
    ///It declares the end of a log
    case stop
}

/**
 Time Unit options.
 
 ````
 case milliseconds
 case seconds
 case minutes
 case hours
 case days
 ````
 
 - **NOTE:**
 Each case also has a description **String**
 
 */
public enum TimeUnit {
    case milliseconds
    case seconds
    case minutes
    case hours
    case days

    public var description: String {
        switch self {
        case .milliseconds:
            return "milliseconds"
        case .seconds:
            return "seconds"
        case .minutes:
            return "minutes"
        case .hours:
            return "hours"
        case .days:
            return "days"
        }
    }
}

public class PerformanceLogger {

    public static let shared = PerformanceLogger()

    let concurrentQueue = DispatchQueue(label: "performanceLoggerConcurrentQueue", attributes: .concurrent)

    ///Private Properties
    private var innerHttpLogger: [String] = [String]()
    private var innerUiLogger: [String] = [String]()
    private var innerBusinessLogger: [String] = [String]()

    ///Getters
    public var httpLogger: [String] { concurrentQueue.sync { return innerHttpLogger } }
    public var uiLogger: [String] { concurrentQueue.sync { return innerUiLogger } }
    public var businessLogger: [String] { concurrentQueue.sync { return innerBusinessLogger } }

    ///Setters
    public func clearHttpLogger() {

        concurrentQueue.async(flags: .barrier) {

            self.innerHttpLogger.removeAll()
        }
    }

    public func clearUiLogger() {

        concurrentQueue.async(flags: .barrier) {

            self.innerUiLogger.removeAll()
        }
    }

    public func clearBusinessLogger() {

        concurrentQueue.async(flags: .barrier) {

            self.innerBusinessLogger.removeAll()
        }
    }

    public func setHttpLogger(append items: [String] = []) {

        concurrentQueue.async(flags: .barrier) {

            self.innerHttpLogger.append(contentsOf: items)
        }
    }

    public func setUiLogger(append items: [String] = []) {

        concurrentQueue.async(flags: .barrier) {

            self.innerUiLogger.append(contentsOf: items)
        }
    }

    public func setBusinessLogger(append items: [String] = []) {

        concurrentQueue.async(flags: .barrier) {

            self.innerBusinessLogger.append(contentsOf: items)
        }
    }

    public func appendHttpLoggerItem(_ item: String) {

        concurrentQueue.async(flags: .barrier) {

            self.innerHttpLogger.append(item)
        }
    }

    public func appendUiLoggerItem(_ item: String) {

        concurrentQueue.async(flags: .barrier) {

            self.innerUiLogger.append(item)
        }
    }

    public func appendBusinessLoggerItem(_ item: String) {

        concurrentQueue.async(flags: .barrier) {

            self.innerHttpLogger.append(item)
        }
    }
}
