/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit

public struct ViewHelper {

    public static func loadNibView(withName: String, owner: Any?) -> UIView? {

        return Bundle.main.loadNibNamed(withName, owner: owner, options: nil)?[0] as? UIView
    }

    public static func loadStoryboardViewController(withStoryboardName: String, withIdentifier: String, bundle: Bundle? = nil) -> UIViewController? {

        return UIStoryboard(name: withStoryboardName, bundle: bundle).instantiateViewController(withIdentifier: withIdentifier)
    }

    /// Returns the topViewController
    /// WARNING: Should not be used for applications that support multiple scenes because it will only return the UIViewController on first keyWindow it finds
    /// - Returns: UIViewController?
    public static func topViewController() -> UIViewController? {
        var result: UIViewController?

        if var topController = UIWindow.keyWindow?.rootViewController {

            while let presentedViewController = topController.presentedViewController {

                topController = presentedViewController
            }

            result = topController
        }

        return result
    }

    /// Set the viewController passed as argument to rootViewController of the UIWindow
    /// WARNING:  Should not be used for applications that support multiple scenes because it will only set the UIViewController on first keyWindow it finds
    /// - Parameter uiViewController: The uiViewController which will be the rootViewController
    public static func setRootViewController(_ uiViewController: UIViewController) {

        UIWindow.keyWindow?.rootViewController = uiViewController
    }

    /// WARNING:  Should not be used for applications that support multiple scenes because it will only set the UIViewController on first keyWindow it finds
    public static var visibleViewController: UIViewController? {

        guard let rootViewController: UIViewController = UIWindow.keyWindow?.rootViewController else {
            return nil
        }

        return getVisibleViewController(rootViewController)
    }

    private static func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {

        if let presentedViewController: UIViewController = rootViewController.presentedViewController {

            return getVisibleViewController(presentedViewController)
        }
        if let navigationController: UINavigationController = rootViewController as? UINavigationController {

            return navigationController.visibleViewController
        }
        if let tabBarController: UITabBarController = rootViewController as? UITabBarController {

            return tabBarController.selectedViewController
        }

        return rootViewController
    }
}
