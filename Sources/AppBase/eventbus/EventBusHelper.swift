/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

open class EventBusHelper: NSObject {

	//MARK: Variables
	fileprivate var eventBus = EventBus.shared
	fileprivate var apiCallCount: Int = 0

	//MARK: Singleton & Constructors
	public static var shared = EventBusHelper()

	public override init() {
		super.init()
	}

	public func setup() {
	}

	//MARK: Register
	open func RegisterOnMainThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void) {
		if !eventBus.isRegistered(name, controller: controller) {
			eventBus.registerOnMainThread(name, controller: controller, block: { n in
				let userInfo: [String: T] = n.userInfo as! [String: T]
				DispatchQueue.main.async {
					handler(userInfo["result"]! as T)
				}
			})
		}
	}

	open func RegisterOnPostThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void) {
		if !eventBus.isRegistered(name, controller: controller) {
			eventBus.registerOnMainThread(name, controller: controller, block: { n in
				let userInfo: [String: T] = n.userInfo as! [String: T]
				handler(userInfo["result"]! as T)
			})
		}
	}

	open func RegisterOnAsyncThread<T : AnyObject>(_ name: String, controller: AnyObject, handler: @escaping (T) -> Void) {
		if !eventBus.isRegistered(name, controller: controller) {
			eventBus.registerOnAsyncThread(name, controller: controller, block: { n in
				let userInfo: [String: T] = n.userInfo as! [String: T]
				DispatchQueue.global().async {
					handler(userInfo["result"]! as T)
				}
			})
		}
	}



	//MARK: Publish
	open func postEvent(_ event: EventBase) {
		self.eventBus.post(event.eventName, result: ["result": event])
	}

	open func postAction(_ action: ActionBase) {
		eventBus.post(action.actionName, result: ["result": action])
	}

	open func postInitializeAction(sentBy: NSObject) {
		self.postAction(InitializeAction(sentBy: sentBy))
	}

	//MARK: Publish STICKY
	open func postStickyEvent(_ event: EventBase) {
		self.eventBus.postSticky(event.eventName, result: ["result": event])
	}

	open func cleanStickyEvent(_ event: EventBase) {
		self.eventBus.cleanStickyEvent(event.eventName)
	}

	open func getStickyEvent(_ eventName: String) -> EventBase? {
		let dic = self.eventBus.getStickyEvent(eventName)
		if dic != nil {
			return dic!["result"] as! EventBase?
		}
		else {
			return nil
		}
	}

	open func postStickyAction(_ action: ActionBase) {
		self.eventBus.postSticky(action.actionName, result: ["result": action])
	}

	open func cleanStickyAction(_ action: ActionBase) {
		self.eventBus.cleanStickyEvent(action.actionName)
	}

	open func getStickyAction(_ actionName: String) -> ActionBase? {
		let dic = self.eventBus.getStickyEvent(actionName)
		if dic != nil {
			return dic!["result"] as! ActionBase?
		}
		else {
			return nil
		}
	}

	//MARK: Start & Finish Initialize methods
	open func startInitialization() {
		self.eventBus.startInitialization()
	}

	open func finishInitialization(_ event: EventBase) {
		self.eventBus.finishInitialization()
		if self.eventBus.getInitializationCounter() <= 0 {
			postStickyEvent(event)
		}
	}

	//MARK: Remove
	open func removeListeners(_ object: AnyObject) {
		eventBus.removeListeners(object)
	}


	//MARK: APICall Events

	open func hasAPICall() -> Bool {
		return self.eventBus.getStickyEvent(OnAPICallEvent.TAG) != nil
	}

	open func onApiCall(_ action: ActionBase?) {
		if apiCallCount == 0 {
			self.eventBus.postSticky(OnAPICallEvent.TAG, result: ["result": OnAPICallEvent(action: action)])
		}
		apiCallCount += 1
	}

	open func onApiCallFinished(_ action: ActionBase?) {
		if apiCallCount == 1 {
			self.eventBus.cleanStickyEvent(OnAPICallEvent.TAG)
			self.eventBus.post(OnAPICallFinishedEvent.TAG, result: ["result": OnAPICallFinishedEvent(action: action)])
		}

		apiCallCount -= 1
	}

	// MARK Sender

	open func isSentByMe(_ sender: NSObject, event: EventBase) -> Bool {

		return event.action!.actionSentBy == sender.description
	}
}
