/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

private enum EventType: String {
	case EVENT_ON_MAIN = "main"
	case EVENT_ON_BACKGROUND = "background"
}

open class EventBus: NSObject {

	//MARK: Variables
	fileprivate var registeredEventDic: [String: EventType]
	fileprivate var registeredEventInController: [String: [AnyObject]]
	fileprivate var stickyEvents: [String: [AnyHashable: Any]]
	fileprivate var initializationCounter: Int
	fileprivate var notificationsNames: Set<String> = Set<String>()

	//MARK: Singleton & Constructors
	public static var shared = EventBus()

	public override init() {
		self.registeredEventDic = [String: EventType]()
		self.registeredEventInController = [String: [AnyObject]]()
		self.stickyEvents = [String: [AnyHashable: Any]]()
		self.initializationCounter = 0
		super.init()
	}

	//MARK: Start and Finish

	open func startInitialization() {
		self.initializationCounter = initializationCounter + 1
	}

	open func finishInitialization() {
		self.initializationCounter = initializationCounter - 1
	}

	open func getInitializationCounter() -> Int {
		return self.initializationCounter
	}

	open func isRegistered(_ name: String, controller: AnyObject) -> Bool {
		let key: String = controller.description + "." + name
		return self.notificationsNames.contains(key)
	}

	//MARK: Register
	open func registerOnMainThread(_ name: String, controller: AnyObject, block: @escaping (Notification) -> Void) {
		self.notificationsNames.insert(controller.description + "." + name)
		let notification = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: name), object: nil, queue: OperationQueue.main, using: block)
		self.registeredEventDic.updateValue(EventType.EVENT_ON_MAIN, forKey: name)

		if self.registeredEventInController[controller.description] != nil {

			var array = self.registeredEventInController[controller.description]!
			array.append(notification)
			self.registeredEventInController.updateValue(array, forKey: controller.description)

		} else {
			var array = [AnyObject]()
			array.append(notification)
			self.registeredEventInController.updateValue(array, forKey: controller.description)
		}
	}

	open func registerOnAsyncThread(_ name: String, controller: AnyObject, block: @escaping (Notification) -> Void) {

		self.notificationsNames.insert(controller.description + "." + name)
		_ = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: name), object: nil, queue: OperationQueue(), using: block)
		self.registeredEventDic.updateValue(EventType.EVENT_ON_BACKGROUND, forKey: name)
		var array: [AnyObject] = [AnyObject]()

		if self.registeredEventInController[controller.description] != nil {

			array = self.registeredEventInController[controller.description]!
		}

		array.append(name as AnyObject)
		self.registeredEventInController.updateValue(array, forKey: controller.description)
	}

	//MARK: Post
	open func post(_ name: String, result: [AnyHashable: Any]?) {

		let index = self.registeredEventDic.index(forKey: name)

		if index != nil {

			let type = self.registeredEventDic[name]

			if type == EventType.EVENT_ON_MAIN {

				//POST ON MAIN THREAD
				DispatchQueue.main.async(execute: {
					NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: result)
				})

			} else if type == EventType.EVENT_ON_BACKGROUND {
				//POST ON BACKGROUND THREAD
				NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: result)
			}
		}
	}

	//MARK: Post STICKY
	open func postSticky(_ name: String, result: [AnyHashable: Any]?) {
		let index = self.registeredEventDic.index(forKey: name)

		if index != nil {

			let type = self.registeredEventDic[name]

			if type == EventType.EVENT_ON_MAIN {

				//POST ON MAIN THREAD
				DispatchQueue.main.async(execute: {
					NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: result)
				})

			} else if type == EventType.EVENT_ON_BACKGROUND {
				//POST ON BACKGROUND THREAD
				NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: result)
			}
		}

		self.stickyEvents.updateValue(result!, forKey: name)
	}

	open func getStickyEvent(_ name: String) -> [AnyHashable: Any]? {
		return self.stickyEvents[name]
	}

	open func cleanStickyEvent(_ name: String) {
		self.stickyEvents.removeValue(forKey: name)
	}

	//MARK: Remove Listeners
	open func removeListeners(_ object: AnyObject) {
		let array = self.registeredEventInController[object.description]

		if array != nil {

			for observer in array! {
				NotificationCenter.default.removeObserver(observer)
			}
		}

		self.registeredEventInController.removeValue(forKey: object.description)

		for notificationName in self.notificationsNames {

			if notificationName.hasPrefix(object.description) {

				self.notificationsNames.remove(notificationName)
			}
		}
	}
}
