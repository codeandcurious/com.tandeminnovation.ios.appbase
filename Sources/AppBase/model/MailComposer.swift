//
//    MailComposerStruct.swift
//
//    MIT License
//
//    Copyright (c) 2018 Tandem Innovation
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import UIKit
import MessageUI

fileprivate final class MailComposer: NSObject {

    //MARK: - Properties

    fileprivate var delegate: MFMailComposeViewControllerDelegate?
    fileprivate var emails: [String] = []
    fileprivate var subject: String?
    fileprivate var body: String?
    fileprivate var isBodyInHtml: Bool = false
    fileprivate var alertTintColor: UIColor?
    fileprivate var presenterViewController: UIViewController?
    fileprivate var alertTitle: String?
    fileprivate var alertMessage: String?

    private var mailController: MFMailComposeViewController {

        let mail: MFMailComposeViewController = MFMailComposeViewController()
        mail.mailComposeDelegate = delegate == nil ? self : delegate
        mail.setToRecipients(emails)
        mail.setSubject(subject ?? String())
        mail.setMessageBody(body ?? String(), isHTML: isBodyInHtml)

        return mail
    }

    //MARK: - Init

    fileprivate override init() { }

    //MARK: - Public Methods

    fileprivate func composeEmail() {

        let composer = createEmailUrl(to: emails, subject: subject, body: body)

        if composer.actions.count <= 1 {

            if composer.url?.absoluteString.prefix(6) == "mailto" && MFMailComposeViewController.canSendMail() {

                presenterViewController?.present(mailController, animated: true)
            } else if let url: URL = composer.url {

                UIApplication.shared.open(url)
            }
        } else {

            let alertStyle: UIAlertController.Style = UIDevice.current.userInterfaceIdiom == .phone ? .actionSheet : .alert
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: alertStyle)

            alert.view.tintColor = alertTintColor
            composer.actions.forEach {
                alert.addAction($0)
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancelar", style: .cancel)
            alert.addAction(cancelAction)

            presenterViewController?.present(alert, animated: true)
        }
    }

    //MARK: - Private Methods

    private func createEmailUrl(to: [String], subject: String?, body: String?) -> (url: URL?, actions: [UIAlertAction]) {

        let subjectEncoded = (subject ?? String()).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? String()
        let bodyEncoded = (body ?? String()).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? String()
        let receipts: String = to.joined(separator: ",")
        var actions: [UIAlertAction] = []

        let appleMail = URL(string: "mailto:\(receipts)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let gmailUrl = URL(string: "googlegmail://co?to=\(receipts)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(receipts)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(receipts)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(receipts)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        var defaultUrl = appleMail

        if let _ = appleMail, MFMailComposeViewController.canSendMail() {

            let action: UIAlertAction = UIAlertAction(title: "Mail", style: .default) { _ in

                self.presenterViewController?.present(self.mailController, animated: true)
            }
            actions.append(action)
            defaultUrl = appleMail
        }

        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {

            let action: UIAlertAction = UIAlertAction(title: "Gmail", style: .default) { _ in
                UIApplication.shared.open(gmailUrl)
            }
            actions.append(action)
            defaultUrl = gmailUrl
        }
        if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {

            let action: UIAlertAction = UIAlertAction(title: "Outlook", style: .default) { _ in
                UIApplication.shared.open(outlookUrl)
            }
            actions.append(action)
            defaultUrl = outlookUrl
        }
        if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {

            let action: UIAlertAction = UIAlertAction(title: "Yahoo Mail", style: .default) { _ in
                UIApplication.shared.open(yahooMail)
            }
            actions.append(action)
            defaultUrl = yahooMail
        }
        if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {

            let action: UIAlertAction = UIAlertAction(title: "Spark", style: .default) { _ in
                UIApplication.shared.open(sparkUrl)
            }
            actions.append(action)
            defaultUrl = sparkUrl
        }
        return (defaultUrl, actions)
    }
}

//MARK: - MFMailComposeViewControllerDelegate

extension MailComposer: MFMailComposeViewControllerDelegate {

    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {

        controller.dismiss(animated: true)
    }
}

public class MailComposerBuilder {

    ///AppBase add html opt to native and omit body in others


    fileprivate var composer: MailComposer = MailComposer()

    public init(delegate: MFMailComposeViewControllerDelegate, presenter: UIViewController?) {

        composer.delegate = delegate
        composer.presenterViewController = presenter
    }

    @discardableResult
    public func withEmailDetails(to: [String], subject: String?, body: String?) -> MailComposerBuilder {

        composer.emails = to
        composer.subject = subject
        composer.body = body
        return self
    }

    @discardableResult
    public func withAlertDetails(tintColor: UIColor?, alertTitle: String?, alertMessage: String?) -> MailComposerBuilder {

        composer.alertTintColor = tintColor
        composer.alertTitle = alertTitle
        composer.alertMessage = alertMessage

        return self
    }

    @discardableResult
    public func build() -> MailComposerBuilder {

        composer.composeEmail()
        return self
    }
}
