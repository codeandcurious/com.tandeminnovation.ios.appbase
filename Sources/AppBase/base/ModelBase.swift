/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

open class ModelBase: NSCoding, Appliable, Runnable {

    public init() {

    }

    public init(json: JSON) {
        self.fromJson(json: json)
    }

    open func fromJson(json: JSON) {
        // intentionally left blank. To be overridden
    }

    open func toJson() -> JSON {
        // intentionally returning nil. To be overridden

        return JSON.null
    }

    open func encode(with coder: NSCoder) {
        // intentionally left blank. To be overridden
    }

    public required init?(coder aDecoder: NSCoder) {

    }

    open func toJsonString() -> String? {

        return self.toJson().rawString(.utf8, options: .init(rawValue: 0))
    }

    /// Simple function to avoid xcode infinite indexing and slow compile time https://stackoverflow.com/questions/65745161/xcode-slow-compile-time-on-equatable-protocol
    /// - Parameter boolValues: example: lhs.title == rhs.title,lhs.subtitle == rhs.subtitle, ...
    /// - Returns: true if it's equals, false if not
    public static func areEquals(_ boolValues: Bool...) -> Bool {

        return !boolValues.contains(false)
    }
}
