/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import Alamofire
import SwiftyJSON

open class ApiBase {

	var initialized: Bool
	var deviceId: String?
	var language: String? = DeviceHelper.deviceLanguage
	open var httpHelper = HttpHelper.shared

	public init(language: String) {
		self.deviceId = DeviceHelper.deviceIdentifier
		self.language = language

		self.initialized = true
	}

	public init() {
		self.deviceId = DeviceHelper.deviceIdentifier
		self.initialized = true
	}

	open func getApiBaseUrl() -> String? {
		// intentionally left blank. to be overriden by extending class
		return nil
	}

	open func getDeviceId() -> String? {

		return self.deviceId
	}

	public func getHttpHelper() -> HttpHelper {

		return httpHelper
	}

	public func setHttpHelper(httpHelper: HttpHelper) {

		self.httpHelper = httpHelper
	}
}
