/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

open class ActionBase: NSObject {

    //MARK: Variables
    public var actionName: String
    public var actionSentBy: String

    //MARK: Constructors
    public init(actionName: String, sentBy: Any) {
        self.actionName = actionName
        self.actionSentBy = String(describing: type(of: sentBy))
        super.init()
    }

    /// Go to all properties and values and build a string based on his contents. To instances of the same object with the same values will return the same string
    /// - Returns: returns a string describing all the properties and values
    public func propertiesUniqueIdentifier() -> String {

        let mirror: Mirror = Mirror(reflecting: self)
        let labels: [String] = mirror.children.compactMap { $0.label }
        let values: [Any] = mirror.children.compactMap { $0.value }

        return "\(String(describing: type(of: self)))\(labels.description)\(values.description)"
    }

    /// To instances of the same object with the same values will return the same uniqueIdentifier
    public var uniqueIdentifier: Int {

        self.propertiesUniqueIdentifier().hash
    }
}
