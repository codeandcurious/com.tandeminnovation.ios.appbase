//
//  Swift.swift
//  AppBase
//
//  Created by Fabio Barreiros on 17/09/2019.
//

import Foundation

public protocol Appliable { }

public extension Appliable {
	@discardableResult
	func apply(closure: (Self) -> Void) -> Self {

		closure(self)

		return self
	}
}

public protocol Runnable { }

public extension Runnable {
	@discardableResult
	func run<T>(closure: (Self) -> T) -> T {

		return closure(self)
	}
}

extension NSObject: Appliable { }
extension NSObject: Runnable { }
