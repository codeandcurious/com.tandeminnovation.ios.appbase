/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit

open class CommonViewControllerBase: UIViewController {

    private static let TAG: String = "CommonViewControllerBase"

    fileprivate let busHelper: EventBusHelper = EventBusHelper.shared
    public private (set) var dataRequestActions: Set<ActionBase> = Set<ActionBase>()
    public var hasEndedFetchingData: Bool { return dataRequestActions.isEmpty }
    public var tapToDismissKeyboard: Bool = false

    public var logHelper = LogHelper.shared

    open override func viewDidLoad() {
        super.viewDidLoad()
        self.registerEvents()
        self.setupApplicationLifeCycle()
        self.setup()
    }

    override open func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if tapToDismissKeyboard {

            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeKeyboardOnTap))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
        }
        self.registerEvents()
    }

    override open func viewDidDisappear(_ animated: Bool) {

        self.unregisterEvents()
        super.viewDidDisappear(animated)
    }

    open func registerEvents() {

        self.busHelper.RegisterOnMainThread(OnErrorEvent.TAG, controller: self, handler: { event in self.OnErrorEventReceiver(event: event) })
    }

    open func unregisterEvents() {

        self.busHelper.removeListeners(self)
    }

    open func addDataRequest(action: ActionBase?) {
        action.whenNotNil {

            dataRequestActions.insert($0)
        }
    }

    open func removeDataRequest(action: ActionBase?) {
        action.whenNotNil {

            if !dataRequestActions.isEmpty {

                dataRequestActions.remove($0)
            }
        }
    }

    open func clearDataRequest() {
        dataRequestActions.removeAll()
    }

    @objc open func closeKeyboardOnTap() {

        view.endEditing(true)
    }

    @objc open func onApplicationWillEnterBackground() {
    }

    @objc open func onApplicationDidEnterBackground() {
    }

    @objc open func onApplicationWillEnterForeground() {

    }

    @objc open func onApplicationDidEnterForeground() {

    }

    open func OnErrorEventReceiver(event: OnErrorEvent) {

    }

    open func setupApplicationLifeCycle() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(onApplicationWillEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(onApplicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(onApplicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(onApplicationDidEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    open func setup() {

    }
}
