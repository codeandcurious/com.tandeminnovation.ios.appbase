/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

public extension Optional {

    /// Receives a closure to be executed if the wrapped value isn't nil
    func whenNotNil(_ closure: (Wrapped) -> Void) {

        guard let unwrapped = self else { return }
        closure(unwrapped)
    }

    func whenNilOrNot(_ whenNil: () -> Void, _ whenNotNil: (Wrapped) -> Void) {

        if let unwrapped = self {

            whenNotNil(unwrapped)
        } else {

            whenNil()
        }
    }

    func whenNil(_ completion: () -> Void) {

        if self == nil {

            completion()
        }
    }

    /// Return the value of the Optional or the `default` parameter
    /// - param: The value to return if the optional is empty
    func unwrapped(_ default: Wrapped) -> Wrapped {

        return self ?? `default`
    }

    /// Returns the unwrapped value of the optional *or*
    /// the result of an expression `else`
    /// I.e. optional.or(else: print("Arrr"))
    func unwrapped(else: @autoclosure () -> Wrapped) -> Wrapped {

        return self ?? `else`()
    }

    /// Returns the unwrapped contents of the optional if it is not empty
    /// If it is empty, throws exception `throw`
    func unwrapped(throw exception: Error) throws -> Wrapped {
        guard let unwrapped = self else { throw exception }

        return unwrapped
    }
}

public extension Optional where Wrapped: Collection {

    var isNilOrEmpty: Bool {

        return self?.isEmpty ?? true
    }

    func whenNotNilOrEmpty(_ closure: (Wrapped) -> Void) {

        if !self.isNilOrEmpty {

            if let unwrapped = self {

                closure(unwrapped)
            }
        }
    }
}

public extension Optional where Wrapped == ModelBase {

    /// Return the value of the Optional or a new ModelBase object
    func unwrapped() -> Wrapped {

        return self ?? ModelBase()
    }
}

public extension Optional where Wrapped == Date {

    /// Return the value of the Optional or a new Date object
    func unwrapped() -> Wrapped {

        return self ?? Date()
    }
}
