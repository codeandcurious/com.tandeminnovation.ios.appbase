/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit

public extension UINavigationController {
    
    /// This method is used as a fail safe method as an alternative to popViewController(_:animated:), the declaration of the viewController is not needed. The name of the controller can be used directly in the parameters of the function
    /// - Example: popToViewController(type: TheViewController.self, animated: true)
    /// - Returns: The poped viewControllers
    @discardableResult func popToViewController<T:UIViewController>(type: T.Type? = nil, animated: Bool = true) -> [UIViewController]? {
        var result: [UIViewController]?
        let viewController = self.viewControllers.lazy.compactMap { $0 as? T }.first
        viewController.whenNotNil {

            result = self.popToViewController($0, animated: animated)
        }

        return result
    }
    
    /// Find the parent viewController of type
    /// - Example: findParentViewController(type: TheViewController.self)
    /// - Returns: The viewController
    func findParentViewController<T:UIViewController>(of type: T.Type? = nil) -> T? {
        var currentParent = self.parent
        while currentParent != nil {

            if let viewController = currentParent as? T {

                return viewController
            }
            currentParent = currentParent?.parent
        }

        return nil
    }
    
    /// Find the viewController of type
    /// - Example: findParentViewController(type: TheViewController.self)
    /// - Returns: The viewController
    func findViewController<T>(of type: T.Type) -> T? {

        return findViewControllers(of: type)?.first
    }
    
    /// Find the viewControllers of type
    /// - Example: findViewControllers(type: TheViewController.self)
    /// - Returns: The viewControllers
    func findViewControllers<T>(of type: T.Type) -> [T]? {

        return self.viewControllers.compactMap { $0 as? T }
    }
}
