/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit

public extension UIView {

    /// Creates a UIImage from the UIView
    /// - Returns: UIImage of the UIView
    func createUIImage() -> UIImage {

        let renderer = UIGraphicsImageRenderer(size: self.bounds.size)
        let image = renderer.image { context in

            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }

        return image
    }

    /// Return the earliest related ParentViewController of any given view
    var parentViewController: UIViewController? {

        var parentResponder: UIResponder? = self
        while parentResponder != nil {

            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {

                return viewController
            }
        }
        return nil
    }

    func setBackgroundGradient(colorArray: [CGColor]) {

        let frame: CGRect = self.frame //UIScreen.main.bounds
        let gradientFrame: CGRect = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)

        let view: UIView = UIView(frame: gradientFrame)
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = gradientFrame
        gradient.colors = colorArray


        let x: Double = 135 / 360.0
        let a: Float = pow(sinf(Float(2.0 * Double.pi * ((x + 0.75) / 2.0))), 2.0)
        let b: Float = pow(sinf(Float(2 * Double.pi * ((x + 0.0) / 2))), 2)
        let c: Float = pow(sinf(Float(2 * Double.pi * ((x + 0.25) / 2))), 2)
        let d: Float = pow(sinf(Float(2 * Double.pi * ((x + 0.5) / 2))), 2)

        gradient.endPoint = CGPoint(x: CGFloat(c), y: CGFloat(d))
        gradient.startPoint = CGPoint(x: CGFloat(a), y: CGFloat(b))

        view.layer.insertSublayer(gradient, at: 0)
        self.layer.insertSublayer(gradient, at: 0)

    }

    /// Set each corner rounded by given corners and radius
    /// - Parameters:
    ///   - corners: UIRectCorner array ex: [.topRight, .topLeft]
    ///   - radius: CGFloat
    func setRoundedCorners(corners: UIRectCorner, radius: CGFloat) {
        layer.cornerRadius = radius
        var caCornerMask: CACornerMask = CACornerMask()
        if corners.contains(.topLeft) {

            caCornerMask.insert(.layerMinXMinYCorner)
        }
        if corners.contains(.topRight) {

            caCornerMask.insert(.layerMaxXMinYCorner)
        }
        if corners.contains(.bottomLeft) {

            caCornerMask.insert(.layerMinXMaxYCorner)
        }
        if corners.contains(.bottomRight) {

            caCornerMask.insert(.layerMaxXMaxYCorner)
        }
        if corners.contains(.allCorners) {

            caCornerMask = CACornerMask(arrayLiteral: .layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner)
        }

        layer.maskedCorners = caCornerMask
        clipsToBounds = true
    }


    /// Sets the view with rounded corners, the corner radius is height / 2
    func setRoundedCorners() {

        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }

    /// Sets the view fully rounded
    func setRound() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set(newValue) {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {

                return UIColor(cgColor: color)
            }
            return nil
        }
        set(newValue) {
            layer.borderColor = newValue?.cgColor
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    func addSubview(view: UIView, viewFrame: CGRect?) {

        _ = self.subviews.map { $0.removeFromSuperview() }
        view.frame = self.bounds
        viewFrame.whenNotNil {

            view.frame = $0
        }
        self.addSubview(view)
    }

    enum Visibility: String {
        case visible = "visible"
        case invisible = "invisible"
        case gone = "gone"
    }

    var visibility: Visibility {
        get {
            let constraint = (self.constraints.filter { $0.firstAttribute == .height && $0.constant == 0 }.first)
            if let constraint = constraint, constraint.isActive {
                return .gone
            } else {
                return self.isHidden ? .invisible : .visible
            }
        }
        set {
            if self.visibility != newValue {
                self.setVisibility(newValue)
            }
        }
    }

    @IBInspectable
    var visibilityState: String {
        get {
            return self.visibility.rawValue
        }
        set {
            let _visibility = Visibility(rawValue: newValue)!
            self.visibility = _visibility
        }
    }

    private func setVisibility(_ visibility: Visibility) {
        let constraints = self.constraints.filter({ $0.firstAttribute == .height && $0.constant == 0 && $0.secondItem == nil && ($0.firstItem as? UIView) == self })
        let constraint = (constraints.first)

        switch visibility {
        case .visible:
            constraint?.isActive = false
            self.isHidden = false
            break
        case .invisible:
            constraint?.isActive = false
            self.isHidden = true
            break
        case .gone:
            self.isHidden = true
            if let constraint = constraint {
                constraint.isActive = true
            } else {
                let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 0)
                // constraint.priority = UILayoutPriority(rawValue: 999)
                self.addConstraint(constraint)
                constraint.isActive = true
            }
            self.setNeedsLayout()
            self.setNeedsUpdateConstraints()
        }
    }

}
