//
//  UILabel.swift
//  appbase
//
//  Created by Fabio Barreiros on 07/05/2018.
//  Copyright © 2018 Tandem Innovation. All rights reserved.
//

import UIKit
import Foundation

public extension UILabel {

    func text(value: String?, defaultValue: String) {
        if value.isNilOrEmpty {

            self.text = defaultValue
        } else {

            self.text = value
        }
    }

    /// This var it's usefull to know the fontSize after the text is setted, useCase: want to use the same fontSize for muiltiple uiLabels
    var actualFontSize: CGFloat {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        guard let attributedText = attributedText else { return font.pointSize }
        let text = NSMutableAttributedString(attributedString: attributedText)
        text.setAttributes([.font: font as Any], range: NSRange(location: 0, length: text.length))
        let context = NSStringDrawingContext()
        context.minimumScaleFactor = minimumScaleFactor
        text.boundingRect(with: frame.size, options: .usesLineFragmentOrigin, context: context)
        let adjustedFontSize: CGFloat = font.pointSize * context.actualScaleFactor

        return adjustedFontSize
    }
}
