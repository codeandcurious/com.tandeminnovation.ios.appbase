/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */


import UIKit
import Foundation

public extension UIButton {

    @IBInspectable var localize: String {
        set (key) {

            setTitle(LocalizationHelper.shared.getLocalizedString(key), for: UIControl.State())
        }
        get {
            return title(for: UIControl.State())!
        }
    }

    @IBInspectable var localizedForHighlighted: String {
        set (key) {
            setTitle(LocalizationHelper.shared.getLocalizedString(key), for: .highlighted)
        }
        get {
            return title(for: .highlighted)!
        }
    }

    func enable() {
        isEnabled = true
        alpha = 1
        isUserInteractionEnabled = true
    }

    func disable() {
        isEnabled = false
        alpha = 0.2
        isUserInteractionEnabled = false
    }

    func setTitle(_ title: String?) {

        self.setTitle(title, for: [])
    }

    func moveImageToRight() {

        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
}
