//
//    ArrayExtension.swift
//
//    MIT License
//
//    Copyright (c) 2018 Tandem Innovation
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation

public extension Array where Element: NSCopying {

    func copy() -> [Element] {

        return self.map {
            $0.copy() as! Element
        }
    }
}

public extension Array where Element == String {

    func containsIgnoringCase(_ element: Element) -> Bool {
        contains { $0.caseInsensitiveCompare(element) == .orderedSame }
    }
}

public extension Array where Element: Hashable {

    func toSet() -> Set<Element> {

        return Set(self)
    }
}

public extension Array where Element: Equatable {

    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {

            if result.contains(value) == false {

                result.append(value)
            }
        }

        return result
    }

    /// Return unique values by keypath, usage yourArray,uniques(by: \.property)
    /// - Parameter keyPath: keyPath description
    /// - Returns: description
    func uniques<T: Hashable>(by keyPath: KeyPath<Element, T>) -> [Element] {

        return reduce([]) { result, element in

            let alreadyExists = (result.contains(where: { $0[keyPath: keyPath] == element[keyPath: keyPath] }))

            return alreadyExists ? result : result + [element]
        }
    }
}

public extension Array {

    /// Safely lookup an index that might be out of bounds, returning nil if it does not exist
    /// - Parameter index: desired index
    /// - Returns: return or the element or nil
    func getSafely(index: Int) -> Element? {
        if 0 <= index && index < count {

            return self[index]
        } else {

            return nil
        }
    }

    var isNotEmpty: Bool {

        return !self.isEmpty
    }

    mutating func insertOrAppend(_ newElement: Self.Element, at i: Self.Index) -> Int {
        var insertedAt: Int = 0
        if self.isEmpty {

            self.append(newElement)
        } else {

            if self.count >= i {

                self.insert(newElement, at: i)
                insertedAt = -1
            } else {

                self.append(newElement)
                insertedAt = self.count - 1
            }
        }

        return insertedAt
    }
}

