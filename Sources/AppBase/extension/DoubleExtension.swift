/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

public extension Double {

    /// Rounds the double to decimal places value
    func roundDecimalPlaces(decimalPlaces: Int) -> Double {

        let divisor: Double = pow(10.0, Double(decimalPlaces))

        return (self * divisor).rounded() / divisor
    }

    func toString(decimalPlaces: Int = 9) -> String {
        let value: Int = decimalPlaces < 0 ? 0 : decimalPlaces
        var string: String = String(format: "%.\(value)f", self)

        while string.last == "0" || string.last == "." {

            if string.last == "." { string = String(string.dropLast()); break }
            string = String(string.dropLast())
        }

        return string
    }

    func toInt() -> Int {

        Int(self)
    }
}
