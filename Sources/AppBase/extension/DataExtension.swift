/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*
 Explanation for this emplementation found here:
 https://stackoverflow.com/a/38024025/9158311
*/

import Foundation

/// Initializes a given type from Data, and adds a property to return the value as Data.
protocol DataConvertible {

	init?(data: Data)

	var data: Data { get }
}

extension DataConvertible where Self: ExpressibleByIntegerLiteral {

	init?(data: Data) {

		var value: Self = 0
		guard data.count == MemoryLayout.size(ofValue: value) else { return nil }
		_ = withUnsafeMutableBytes(of: &value, { data.copyBytes(to: $0) })
		self = value
	}

	var data: Data {

		return withUnsafeBytes(of: self) { Data($0) }
	}
}

// The types below are convertible to/from Data trivially, as per the protocol implementation
extension Int: DataConvertible { }
extension Float: DataConvertible { }
extension Double: DataConvertible { }
extension Bool: DataConvertible {

	init?(data: Data) {

		var value: Bool = false
		guard data.count == MemoryLayout.size(ofValue: value) else { return nil }
		_ = withUnsafeMutableBytes(of: &value, { data.copyBytes(to: $0) })
		self = value
	}

	var data: Data {

		return withUnsafeBytes(of: self) { Data($0) }
	}

}
extension Date: DataConvertible {

	init?(data: Data) {

		var value: Date = Date()
		guard data.count == MemoryLayout.size(ofValue: value) else { return nil }
		_ = withUnsafeMutableBytes(of: &value, { data.copyBytes(to: $0) })
		self = value
	}

	var data: Data {

		return withUnsafeBytes(of: self) { Data($0) }
	}
}

// Conversion to/from String is non-trivial
extension String: DataConvertible {

	init?(data: Data) {

		self.init(data: data, encoding: .utf8)
	}

	var data: Data {
		// Note: a conversion to UTF-8 cannot fail.
		return self.data(using: .utf8)!
	}
}

