//
//    ArrayExtension.swift
//
//    MIT License
//
//    Copyright (c) 2018 Tandem Innovation
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation

public extension Bundle {

    private class CurrentBundleFinder { }

    /// Search for the Bundle with the provided name in the possible candidate locations (ex: App, Framework or DebugSimulator
    /// - Parameter bundleName: The bundle name (ex: MySpmModule)
    /// - Returns: returns the bundle or throw fatalError("unable to find bundle named")
    static func findBundle(bundleName: String) -> Bundle {

        let bundleFullName = "\(bundleName)_\(bundleName)"
        let candidates = [
            /* Bundle should be present here when the package is linked into an App. */
            Bundle.main.resourceURL,
            /* Bundle should be present here when the package is linked into a framework. */
            Bundle(for: CurrentBundleFinder.self).resourceURL,
            /* For command-line tools. */
            Bundle.main.bundleURL,
            /* Bundle should be present here when running previews from a different package (this is the path to "…/Debug-iphonesimulator/"). */
            Bundle(for: CurrentBundleFinder.self).resourceURL?.deletingLastPathComponent().deletingLastPathComponent(),
        ]
        for candidate in candidates {

            let bundlePath = candidate?.appendingPathComponent(bundleFullName + ".bundle")
            if let bundle = bundlePath.flatMap(Bundle.init(url:)) {

                return bundle
            }
        }
        fatalError("unable to find bundle named \(bundleName)")
    }
}
