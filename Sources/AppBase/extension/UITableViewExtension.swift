/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit

public extension UITableView {

    /// Register UITableViewCell
    ///
    /// - Parameter cellName: cellName should be the same for Identifier and XIB Name
    func registerForCell(_ cellName: String, bundle: Bundle? = nil) {

        let cell = UINib(nibName: cellName, bundle: bundle)
        self.register(cell, forCellReuseIdentifier: cellName)
    }

    func reuseCell(_ cellWrapperBase: CellWrapperBase, _ indexPath: IndexPath) -> UITableViewCell {

        return dequeueReusableCell(withIdentifier: cellWrapperBase.cellIdentifier, for: indexPath)
    }

    var centerPoint: CGPoint {

        get {
            let x: CGFloat = self.center.x + self.contentOffset.x
            let y: CGFloat = self.center.y + self.contentOffset.y

            return CGPoint(x: x, y: y)
        }
    }

    var centerCellIndexPath: IndexPath? {

        if let centerIndexPath = self.indexPathForRow(at: self.centerPoint) {

            return centerIndexPath
        }

        return nil
    }

    func reloadDataAndKeepOffset() {
        // stop scrolling
        setContentOffset(contentOffset, animated: false)

        // calculate the offset and reloadData
        let beforeContentSize = contentSize
        reloadData()
        layoutIfNeeded()
        let afterContentSize = contentSize

        // reset the contentOffset after data is updated
        let newOffset = CGPoint(
            x: contentOffset.x + (afterContentSize.width - beforeContentSize.width),
            y: contentOffset.y + (afterContentSize.height - beforeContentSize.height))
        setContentOffset(newOffset, animated: false)
    }
}
