/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

public extension Sequence {

    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U: [Iterator.Element]] {

        return Dictionary.init(grouping: self, by: key)
    }

    /// Simple method to sort sequence by parameter avoiding closure
    /// - Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
    /// - Returns: The sequence sorted by a > b
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in
            return a[keyPath: keyPath] > b[keyPath: keyPath]
        }
    }

    /// Sort descendent by the keypath
    /// - Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
    /// - Returns: The sequence sorted descendent
    func sortedDescendent<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in

            return a[keyPath: keyPath] > b[keyPath: keyPath]
        }
    }

    /// Sort ascendent by the keypath
    /// - Parameter keyPath: The argument must be passed as a keypath, eg: \.argName
    /// - Returns: The sequence sorted ascendent
    func sortedAscendent<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in

            return a[keyPath: keyPath] < b[keyPath: keyPath]
        }
    }
}

