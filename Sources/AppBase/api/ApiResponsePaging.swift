/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

open class ApiResponsePaging: NSObject {

	public static let TAG: String = String(describing: self)

	private let JSON_PAGE_NUMBER: String = "pageNumber"
	private let JSON_PAGE_SIZE: String = "pageSize"
	private let JSON_PAGE_COUNT: String = "pageCount"
	private let JSON_TOTAL_RECORD_COUNT: String = "languages"

	public var pageNumber: Int?
	public var pageSize: Int?
	public var pageCount: Int?
	public var totalRecordCount: Int?

	private var jsonHelper = JsonHelper.shared

	public override init() {
		super.init()
	}

	public init(pageNumber: Int?, pageSize: Int?, pageCount: Int?, totalRecordCount: Int?) {
		super.init()
		self.pageNumber = pageNumber
		self.pageSize = pageSize
		self.pageCount = pageCount
		self.totalRecordCount = totalRecordCount
	}

	public init(json: JSON) {
		super.init()
		self.fromJson(json: json)
	}

	open func fromJson(json: JSON) {
		if self.jsonHelper.hasKey(JSON_PAGE_NUMBER, json: json) {

			self.pageNumber = jsonHelper.parseLong(JSON_PAGE_NUMBER, json: json)
		}
		if self.jsonHelper.hasKey(JSON_PAGE_SIZE, json: json) {

			self.pageSize = jsonHelper.parseLong(JSON_PAGE_SIZE, json: json)
		}
		if self.jsonHelper.hasKey(JSON_PAGE_COUNT, json: json) {

			self.pageCount = jsonHelper.parseLong(JSON_PAGE_COUNT, json: json)
		}
		if self.jsonHelper.hasKey(JSON_TOTAL_RECORD_COUNT, json: json) {

			self.totalRecordCount = jsonHelper.parseLong(JSON_TOTAL_RECORD_COUNT, json: json)
		}
	}
}
