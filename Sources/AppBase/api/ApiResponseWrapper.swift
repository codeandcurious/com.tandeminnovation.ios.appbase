/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

open class ApiResponseWrapper<T>: ApiResponseWrapperBase<T> {

    public let TAG: String = "ApiResponseWrapper"

    private let JSON_DATA: String = "data"
    private let JSON_ERROR: String = "error"
    private let JSON_LOCALIZATION: String = "localization"
    private let JSON_PAGING: String = "paging"

    // auth token json keys
    private let JSON_TOKEN_TYPE = "token_type"
    private let JSON_ACCESS_TOKEN = "access_token"
    private let JSON_EXPIRES_IN = "expires_in"

    open var error: ApiResponseError?
    open var localization: ApiResponseLocalization?
    open var paging: ApiResponsePaging?
    open var etag: String?
    open var lastModified: String?
    open var urlRequest: URLRequest?

    private static func buildDefaultApiResponseWrapper(_ dataResponse: DataResponse<Any>) -> ApiResponseWrapper<T> {
        let apiResponseWrapper: ApiResponseWrapper<T> = ApiResponseWrapper<T>()
        dataResponse.response.whenNotNil {

            apiResponseWrapper.code = $0.statusCode
        }

        return apiResponseWrapper
    }

    public static func build(dataResponse: DataResponse<Any>, apiResponseParser: ((_ json: JSON) -> T?) = { _ in return nil }) -> ApiResponseWrapper<T> {

        guard let json = HttpHelper.shared.getDataIfSuccessful(response: dataResponse) else { return buildDefaultApiResponseWrapper(dataResponse) }
        let apiResponseWrapper = ApiResponseWrapper<T>.init(json: json, apiResponseParser: apiResponseParser)
        dataResponse.response.whenNotNil {

            apiResponseWrapper.code = $0.statusCode
            apiResponseWrapper.etag = $0.allHeaderFields["Etag"] as? String
            apiResponseWrapper.lastModified = $0.allHeaderFields["Last-Modified"] as? String
        }
        apiResponseWrapper.urlRequest = dataResponse.request

        return apiResponseWrapper
    }

    public static func build<T>(data: Data, responseCode: Int, apiResponseParser: ((_ json: JSON) -> T?)) -> ApiResponseWrapper<T> {

        guard let json = try? JSON(data: data) else {

            let apiResponseWrapper: ApiResponseWrapper<T> = ApiResponseWrapper<T>()
            apiResponseWrapper.code = responseCode
            return apiResponseWrapper
        }
        let apiResponseWrapper = ApiResponseWrapper<T>.init(json: json, apiResponseParser: apiResponseParser)
        apiResponseWrapper.code = responseCode

        return apiResponseWrapper
    }

    public static func build<T>(data: T?, responseCode: Int, etag: String) -> ApiResponseWrapper<T> {

        return ApiResponseWrapper<T>(data: data, code: responseCode, etag: etag)
    }

    public convenience init(error: ApiResponseError, data: T?, localization: ApiResponseLocalization?, paging: ApiResponsePaging?) {

        self.init(error: error, data: data, localization: localization, paging: paging, code: nil)
    }

    public init(error: ApiResponseError? = nil, data: T? = nil, localization: ApiResponseLocalization? = nil, paging: ApiResponsePaging? = nil, code: Int? = nil, etag: String? = nil) {

        super.init()
        self.error = error
        self.data = data
        self.localization = localization
        self.paging = paging
        self.code = code ?? HttpHelper.HTTP_CODE.CLIENT_TIMEOUT
        self.etag = etag
    }

    public convenience init(json: JSON, apiResponseParser: (_ json: JSON) -> T?) {

        self.init()
        self.fromJson(json: json)
        if let jsonData = self.jsonData {

            self.data = apiResponseParser(jsonData)
        }
    }

    open func fromJson(json: JSON) {

        if self.jsonHelper.hasKey(JSON_ERROR, json: json) {

            self.error = ApiResponseError.init(json: json[JSON_ERROR])
        }
        if self.jsonHelper.hasKey(JSON_LOCALIZATION, json: json) {

            self.localization = ApiResponseLocalization.init(json: json[JSON_LOCALIZATION])
        }
        if self.jsonHelper.hasKey(JSON_PAGING, json: json) {

            self.paging = ApiResponsePaging.init(json: json[JSON_PAGING])
        }
        if self.jsonHelper.hasKey(JSON_DATA, json: json) {

            self.jsonData = json[JSON_DATA]
        }
    }
}
