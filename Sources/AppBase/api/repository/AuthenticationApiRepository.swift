/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

open class AuthenticationApiRepository: ApiRepositoryBase {

    private lazy var authenticationApi = AuthenticationApi()

    open func getAccessToken(url: String, clientID: String, clientSecret: String, scope: String) throws -> ApiResponseWrapper<String> {

        return try self.authenticationApi.getAccessToken(url: url, clientID: clientID, clientSecret: clientSecret, scope: scope)
    }

    open func refreshAccessToken(url: String, clientID: String, clientSecret: String, scope: String, refreshToken: String) throws -> ApiResponseWrapper<OAuth2TokenModel> {

        return try self.authenticationApi.refreshAccessToken(url: url, clientID: clientID, clientSecret: clientSecret, scope: scope, refreshToken: refreshToken)
    }
}
