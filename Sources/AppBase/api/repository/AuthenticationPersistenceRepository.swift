/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

class AuthenticationPersistenceRepository: PersistenceRepositoryBase {

    private let preferencesHelper: PreferencesHelper = PreferencesHelper.shared
    static let shared: AuthenticationPersistenceRepository = AuthenticationPersistenceRepository()
    private let oauth2TokenKey: String = "OAuth2Token"


    public func save(oauth2Token: OAuth2TokenModel, password: String? = nil) -> Bool {

        guard let tokenJsonString: String = oauth2Token.toJsonString() else { return false }
        return self.preferencesHelper.set(value: tokenJsonString, forKey: self.oauth2TokenKey, password: password)
    }

    public func loadOAuth2Token(password: String = "") -> OAuth2TokenModel? {
        guard let savedTokenJsonString: String = self.preferencesHelper.getValue(forKey: self.oauth2TokenKey, defaultValue: nil, password: password) else {

            return nil
        }

        let jsonToken = JSON(parseJSON: savedTokenJsonString)
        let token = OAuth2TokenModel()
        token.fromJson(json: jsonToken)

        return token
    }

    public func clearOAuth2Token() {
        self.preferencesHelper.removeObject(forKey: self.oauth2TokenKey)
    }
}
