/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import Alamofire
import SwiftyJSON

open class AuthenticationApi: ApiBase {

    public static let TAG: String = "AuthenticationApi"

    private func buildToken(clientId: String, clientSecret: String) -> String {
        let token: String = "\(clientId):\(clientSecret)"
        let data: Data? = token.data(using: String.Encoding.utf8)

        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }

    open func getAccessToken(url: String, clientID: String, clientSecret: String, scope: String) throws -> ApiResponseWrapper<String> {
        var headers: [String: String] = [String: String]()
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Authorization"] = "basic " + self.buildToken(clientId: clientID, clientSecret: clientSecret)

        let content = "grant_type=client_credentials&scope=\(scope)"
        let dataResponse = try httpHelper.makeRequest(url: url, body: content, method: .post, headers: headers)
        let apiResponseWrapper = ApiResponseWrapper<String>.build(dataResponse: dataResponse) { (data) -> String? in

            return data["access_token"].string
        }

        return apiResponseWrapper
    }

    open func refreshAccessToken(url: String, clientID: String, clientSecret: String, scope: String, refreshToken: String) throws -> ApiResponseWrapper<OAuth2TokenModel> {
        var headers = [String: String]()
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Authorization"] = "basic " + self.buildToken(clientId: clientID, clientSecret: clientSecret)

        let content = "grant_type=refresh_token&refresh_token=\(refreshToken)"
        let dataResponse = try httpHelper.makeRequest(url: url, body: content, method: .post, headers: headers)

        // encapsulate dataResponse into data key
        var jsonString = String(data: dataResponse.data!, encoding: String.Encoding.utf8)!
        jsonString = String(String(jsonString.dropFirst()).dropLast())
        let newJsonString = "{\"data\":{\(jsonString)}}"
        let newData = newJsonString.data(using: String.Encoding.utf8)!

        let apiResponseWrapper = ApiResponseWrapper<OAuth2TokenModel>.build(data: newData, responseCode: dataResponse.response?.statusCode ?? HttpHelper.HTTP_CODE.NETWORK_NOT_FOUND) { (data: JSON) -> OAuth2TokenModel? in

            if JsonHelper.shared.isValid(data) {

                return OAuth2TokenModel(json: data)
            } else {

                return nil
            }
        }

        return apiResponseWrapper
    }

    open func getToken(url: String, success: (_ result: String, _ statusCode: Int?) -> (), fail: (_ result: String, _ statusCode: Int?) -> ()?, clientID: String, clientSecret: String, scope: String) {
        let token64: String = buildToken(clientId: clientID, clientSecret: clientSecret)
        let headers: [String: String] = [
            "content-type": "application/x-www-form-urlencoded",
            "authorization": "basic " + token64,
        ]

        let parameters: Parameters = [
            "grant_type": "client_credentials",
            "scope": scope
        ]

        do {
            let dataResponse: DataResponse = try httpHelper.makeRequest(url: url, parameter: parameters, method: .post, headers: headers)
            if CompilerHelper.shared.isDebugging {

                let _: String? = JsonHelper.shared.prettyPrint(dataResponse)
                _ = "DO NOTHING"
            }
            var accessToken: String? = nil
            dataResponse.data.whenNotNilOrEmpty { data in

                let json: JSON = JSON(data)
                accessToken = json["access_token"].string
            }
            success(accessToken ?? String(), dataResponse.response?.statusCode)
        } catch(let error) {

            fail(error.localizedDescription, HttpHelper.HTTP_CODE.UNAVAILABLE)
        }
    }
}
