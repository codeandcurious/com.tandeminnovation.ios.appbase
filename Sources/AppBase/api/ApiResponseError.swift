/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

open class ApiResponseError: NSObject {

	public static let TAG: String = String(describing: self)

	private let JSON_USER_MESSAGE: String = "userMessage"
	private let JSON_ERROR_MESSAGE: String = "errorMessage"
	private let JSON_ERROR_CODE: String = "errorCode"

	public var userMessage: String?
    public var errorMessage: String?
    public var errorCode: String?

	private var jsonHelper = JsonHelper.shared

	public override init() {
		super.init()
	}

	public init(userMessage: String?, errorMessage: String?, errorCode: String?) {
		super.init()
		self.userMessage = userMessage
		self.errorMessage = errorMessage
		self.errorCode = errorCode
	}

	public init(json: JSON) {
		super.init()
		self.fromJson(json: json)
	}

	open func fromJson(json: JSON) {
		if self.jsonHelper.hasKey(JSON_USER_MESSAGE, json: json) {

			self.userMessage = jsonHelper.parseString(JSON_USER_MESSAGE, json: json)
		}
		if self.jsonHelper.hasKey(JSON_ERROR_MESSAGE, json: json) {

			self.errorMessage = jsonHelper.parseString(JSON_ERROR_MESSAGE, json: json)
		}
		if self.jsonHelper.hasKey(JSON_ERROR_CODE, json: json) {

			self.errorCode = jsonHelper.parseString(JSON_ERROR_CODE, json: json)
		}
	}
}
