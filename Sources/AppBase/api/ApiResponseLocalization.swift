/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import SwiftyJSON

open class ApiResponseLocalization: NSObject {

	public static let TAG: String = String(describing: self)

	private let JSON_DEFAULT_LANGUAGE: String = "defaultLanguage"
	private let JSON_LANGUAGES: String = "languages"

	public var defaultLanguage: String?
	public var languages: Array<String>?

	private var jsonHelper = JsonHelper.shared

	public override init() {
		super.init()
	}

	public init(defaultLanguage: String?, languages: Array<String>?) {
		super.init()
		self.defaultLanguage = defaultLanguage
		self.languages = languages
	}

	public init(json: JSON) {
		super.init()
		self.fromJson(json: json)
	}

	open func fromJson(json: JSON) {
		if self.jsonHelper.hasKey(JSON_DEFAULT_LANGUAGE, json: json) {

			self.defaultLanguage = jsonHelper.parseString(JSON_DEFAULT_LANGUAGE, json: json)
		}
		if self.jsonHelper.hasKey(JSON_LANGUAGES, json: json) {

			self.languages = jsonHelper.parseStringList(JSON_LANGUAGES, json: json)
		}
	}
}
