/*
 MIT License
 
 Copyright (c) 2018 Tandem Innovation
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation

open class ApiAuthenticationManager: ManagerBase {

    public static let shared = ApiAuthenticationManager()
    private let authenticationApiRepository = AuthenticationApiRepository()
    private let authenticationPersistenceRepository = AuthenticationPersistenceRepository.shared
    private let authenticationCacheRepository = AuthenticationCacheRepository.shared

    private var tokenUrl: String = String()
    private var clientSecret: String = String()
    private var clientId: String = String()
    private var scope: String = String()

    public override init() {
        super.init()
    }

    open func setup(tokenUrl: String, clientSecret: String, clientId: String, scope: String) {
        super.setup()

        self.tokenUrl = tokenUrl
        self.clientSecret = clientSecret
        self.clientId = clientId
        self.scope = scope
    }

    open func getAccessToken() throws -> ApiResponseWrapper<String> {

        return try self.authenticationApiRepository.getAccessToken(url: tokenUrl, clientID: clientId, clientSecret: clientSecret, scope: scope)
    }

    open func refreshAccessToken(password: String = "") throws -> ApiResponseWrapper<OAuth2TokenModel> {
        guard let refreshToken = self.loadOAuth2Token(password: password)?.refreshToken else {
            throw Exception.TOKEN_NOT_FOUND
        }

        return try self.authenticationApiRepository.refreshAccessToken(url: tokenUrl, clientID: clientId, clientSecret: clientSecret, scope: scope, refreshToken: refreshToken)
    }

    open func saveOAuth2Token(tokenType: String, refreshToken: String, expiresIn: Int, idToken: String, accessToken: String, password: String? = nil) -> Bool {
        let newOAuth2Token: OAuth2TokenModel = OAuth2TokenModel()
        newOAuth2Token.tokenType = tokenType
        newOAuth2Token.refreshToken = refreshToken
        newOAuth2Token.expiresIn = expiresIn
        newOAuth2Token.idToken = idToken
        newOAuth2Token.accessToken = accessToken
        self.authenticationCacheRepository.save(oauth2Token: newOAuth2Token)
        return self.authenticationPersistenceRepository.save(oauth2Token: newOAuth2Token, password: password)
    }

    open func clearOAuth2Token() {
        self.authenticationCacheRepository.clearOAuth2Token()
        self.authenticationPersistenceRepository.clearOAuth2Token()
    }

    /// Retrieves token from cache or persistence
    open func loadOAuth2Token(password: String = "") -> OAuth2TokenModel? {
        if let savedToken = self.authenticationCacheRepository.loadOAuth2Token() {

            return savedToken
        } else if let persistedToken = self.authenticationPersistenceRepository.loadOAuth2Token(password: password) {

            self.authenticationCacheRepository.save(oauth2Token: persistedToken)
            return persistedToken
        }

        return nil
    }
}
