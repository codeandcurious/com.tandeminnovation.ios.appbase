// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppBase",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "AppBase", targets: ["AppBase"])
    ],
    dependencies: [
        .package(name:"SwiftyJSON", url: "https://github.com/SwiftyJSON/SwiftyJSON.git", .exact("5.0.1")),
        .package(name:"Alamofire", url: "https://github.com/Alamofire/Alamofire.git", .exact("4.9.1")),
        .package(name:"RNCryptor", url: "https://github.com/RNCryptor/RNCryptor.git", .exact("5.1.0")),
        .package(name:"IBLocalizable", url: "https://github.com/firetrap/IBLocalizable.git", .exact("1.3.6"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "AppBase",
            dependencies: [
                .product(name: "SwiftyJSON", package: "SwiftyJSON"),
                .product(name: "Alamofire", package: "Alamofire"),
                .product(name: "RNCryptor", package: "RNCryptor"),
                .product(name: "IBLocalizable", package: "IBLocalizable"),
            ])
    ]
)
