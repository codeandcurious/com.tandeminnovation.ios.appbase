![tandem-innovation](tandem-innovation.png)

## AppBase

AppBase is a set of core libraries developed in Swift that allows for faster apps development, by employing our base classes along with our helpers and extensions to the Swift Standard Library.


## Installation

#### [CocoaPods](http://cocoapods.org) (recommended)

````ruby
use_frameworks!

pod 'AppBase', :git => 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.appbase.git', :branch => 'master', :tag => 'LATEST_TAG'
````

#### [Carthage](https://github.com/Carthage/Carthage)

````bash
git "https://firetrap@bitbucket.org/codeandcurious/com.tandeminnovation.ios.appbase.git" == LATEST_TAG
````

#### [Swift Package Manager](https://swift.org/package-manager/)

```swift
dependencies: [
    .package(url: "https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.appbase.git", from: "LATEST_TAG")
]
```
## License

AppBase is available under the MIT license. See the * [LICENSE](LICENSE) file for more info.

 
