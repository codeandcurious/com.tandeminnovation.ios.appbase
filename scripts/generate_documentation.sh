rm -f ../README.md #removes the old file
cp TEMPLATE_README.md .. #copy the template
mv sourcedocs .. #move sourcedocs because it needs to be executed in project root
cd ..
mv TEMPLATE_README.md README.md #rename
./sourcedocs generate --link-beginning "Documentation/Reference/" #generate docs
mv sourcedocs scripts #move again sourcedocs to scripts folder
cat Documentation/Reference/README.md >> README.md #concat the generated readme to main readme


