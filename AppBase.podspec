Pod::Spec.new do |s|
  s.name             = 'AppBase'
  s.version          = '1.9.9'
  s.summary          = 'Tandem innovation AppBase'
  s.description      = <<-DESC
AppBase is a set of core libraries developed in Swift that allows for faster apps development, by employing our base classes along with our helpers and extensions to the Swift Standard Library.
DESC

  s.homepage         = 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.appbase/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'firetrap' => 'fabio.barreiros@nextreality.pt' }
  s.source           = { :git => 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.appbase.git', :tag => s.version.to_s }
  s.swift_version = '5'
  s.ios.deployment_target = '13.0'
  s.source_files = 'Sources/**/*{swift,h,m}'

  s.dependency 'Alamofire', '4.9.1'
  s.dependency 'SwiftyJSON', '5.0.1'
  s.dependency 'IBLocalizable', '1.3.0'
  s.dependency 'RNCryptor', '5.1.0'

end
